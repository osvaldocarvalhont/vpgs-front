import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  {
    path: '',
    loadChildren: () => import('./externo/externo.module').then(m => m.ExternoModule)
  },
  {
    path: 'sso-portal/app',
    loadChildren: () => import('./externo/externo.module').then(m => m.ExternoModule)
  },
  {
      path: 'interno',
      loadChildren: () => import('./interno/interno.module').then(m => m.InternoModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
