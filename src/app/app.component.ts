import { Component, OnDestroy, HostListener } from '@angular/core';
import {​​​​​​​ Cookie }​​​​​​​ from 'ng2-cookies';
import { AuthService } from 'src/app/externo/login/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {

  constructor(private authService: AuthService){}

  title = 'vpgs-front';

  @HostListener('window:beforeunload')
  ngOnDestroy()
  {
    this.authService.resetAllCookies();
  }
}
