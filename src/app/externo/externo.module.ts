import { ScrollPanelModule } from 'primeng/scrollpanel';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExternoRoutingModule } from './externo-routing.module';
import { ExternoComponent } from './externo.component';


@NgModule({
  declarations: [ExternoComponent],
  imports: [
    CommonModule,
    ExternoRoutingModule,
    ScrollPanelModule
  ]
})
export class ExternoModule { }
