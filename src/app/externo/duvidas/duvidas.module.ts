import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';


import { DuvidasRoutingModule } from './duvidas-routing.module';
import { DuvidasComponent } from './duvidas.component';


@NgModule({
  declarations: [DuvidasComponent],
  imports: [
    CommonModule,
    DuvidasRoutingModule,
    SharedModule,
    ScrollPanelModule,
    FontAwesomeModule
  ]
})
export class DuvidasModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faChevronLeft);
  }
}
