import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FaqCategory } from 'src/app/shared/models/faq-category.model';
import { FilterPerguntasFrequentes } from 'src/app/shared/models/filter-perguntas-frequentes.model';
import { FaqCategoryService } from 'src/app/interno/vpgs-adm/perguntas/services/faq-category.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-duvidas',
  templateUrl: './duvidas.component.html',
  styleUrls: ['./duvidas.component.css']
})
export class DuvidasComponent implements OnInit {

  public perguntasFrequentes: FaqCategory[] = [];
  public filter: FilterPerguntasFrequentes = new FilterPerguntasFrequentes();

  constructor(private location: Location,
              private faqCategoryService: FaqCategoryService) { }

  ngOnInit(): void {
    this.filter.isLogged = '1';
    this.getAllFaqCategoriesNotLogged();
  }

  public load(valueEmited: string) {
    this.filter.textSearch = valueEmited != null ? valueEmited.toUpperCase() : null;
    this.getAllFaqCategoriesNotLogged();
  }

  public getAllFaqCategoriesNotLogged(): void {
    this.faqCategoryService.findAll(this.filter).pipe(take(1)).subscribe(res => {
          this.perguntasFrequentes =  res;
    });
  }

  public back(): void {
    this.location.back();
  }

}
