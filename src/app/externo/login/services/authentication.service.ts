import { User } from '../../../../../src/app/shared/models/user.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { EsquecerSenha } from 'src/app/shared/models/esquecer-senha.model';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    private readonly ENDPOINT_LOGIN = 'login';
    private readonly ENDPOINT_SOLICITAR_TROCA_SENHA = 'senha/solicitar';
    private readonly ENDPOINT_CONFIRMAR_TROCA_SENHA = 'senha/confirmar';
    private readonly ENDPOINT_CADSTRAR = 'cadastrar-users-login';
    private readonly ENDPOINT_CADSTRAR_CONFIRMAR = 'cadastrar-users-login/confirmar';
    private readonly ENDPOINT_EXISTE_MATRICULA = 'cadastrar-users-login/has-matricula';

    constructor(private http: HttpClient) {
    //    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    //    this.currentUser = this.currentUserSubject.asObservable();
    }

    //public get currentUserValue(): User {
    //   return this.currentUserSubject.value;
    //}

    login(usernameP: string, passwordP: string, permissions: string[], code: string, vale: boolean) {
        return this.http.post<any>(`${environment.BASE_URL.concat(this.ENDPOINT_LOGIN)}`, { username : usernameP, password : passwordP, permissions : permissions, code: code, operadorVale : vale })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                //this.currentUserSubject.next(user);
                //console.log(user)
                return user;
            }));
    }

    exist(matricula: string) {
        return this.http.get<Boolean>(`${environment.BASE_URL.concat(this.ENDPOINT_EXISTE_MATRICULA).concat('/').concat(matricula)}`)
            .pipe(map(hasUser => {
                //console.log(hasUser)
                return hasUser;
            }));
    }

    solicitarNovaSenha(email: string) {
        //return this.http.post<any>(`${environment.BASE_URL.concat(this.ENDPOINT_SOLICITAR_TROCA_SENHA)}`, { email : email, callback : `${environment.REDIRECT_URI}` })
        return this.http.post<any>(`${environment.BASE_URL.concat(this.ENDPOINT_SOLICITAR_TROCA_SENHA)}`, { email : email })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                //localStorage.setItem('currentUser', JSON.stringify(user));
                //this.currentUserSubject.next(user);
                //console.log(user)
                return user;
            }));
    }

    confirmarNovaSenha(dto: EsquecerSenha) {
        return this.http.post<any>(`${environment.BASE_URL.concat(this.ENDPOINT_CONFIRMAR_TROCA_SENHA)}`, dto)
    }

    cadastrarUser(dto: User) {
        let path = ''// ('?callback=').concat(`${environment.REDIRECT_URI}`);
        return this.http.post<User>(`${environment.BASE_URL.concat(this.ENDPOINT_CADSTRAR).concat(path)}`, dto)
    }

    confimarCadastro(hash: string) {
        console.log(hash);
        return this.http.get<User>(`${environment.BASE_URL.concat(this.ENDPOINT_CADSTRAR_CONFIRMAR).concat('/').concat(hash)}`)
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        //this.currentUserSubject.next(null);
    }
}
