/*import {​​​​​​​ environment }​​​​​​​ from '../../../../environments/environment';
import {​​​​​​​ HttpClient, HttpHeaders, HttpParams }​​​​​​​ from '@angular/common/http';
import {​​​​​​​Injectable}​​​​​​​ from '@angular/core';
import {​​​​​​​ Cookie }​​​​​​​ from 'ng2-cookies';
import {​​​​​​​ Observable }​​​​​​​ from 'rxjs';
import {​​​​​​​ catchError }​​​​​​​ from 'rxjs/operators';
import { map } from 'rxjs/operators';
import {​​​​​​​ TokenAim }​​​​​​​ from '../../../shared/models/token-aim.model';


@Injectable({​​​​​​​
  providedIn: 'root'
}​​​​​​​)
export class AuthService {​​​​​​​


  private param_response_type = 'code';
  private param_state = '';
  private param_scope = 'vpgs';
  //private param_scope = 'vtooling';
  private param_grant_type = 'authorization_code';

  constructor(private _http: HttpClient) {​​​​​​​ }​​​​​​​


  redirectToAim(): void {​​​​​​​

    this.resetAllCookies();

    let urlRedirect =  environment.URL_AIM_AUTH + '?response_type=' + this.param_response_type + '&state='  + this.param_state + '&client_id=' +
    environment.CLIENT_ID  + '&scope=' + this.param_scope + '&redirect_uri=' + environment.REDIRECT_URI + '&acr_values=' + environment.ACR_VALUES;

    window.location.href = urlRedirect;
  }​​​​​​​


  resetAllCookies(): void {​​​​​​​

    Cookie.deleteAll('/', 'localhost');
    Cookie.deleteAll('/', 'vpgs.valeglobal.net');
    Cookie.delete('access_token');
    Cookie.delete('refresh_token');
    Cookie.delete('access_token_exp');
    Cookie.delete('access-code');

    console.log("Apagando cookies");
  }​​​​​​​

  getToken(code: string) {​​​​​​​

    return this._http.post<TokenAim>(environment.URL_TOKEN_ORIGINAL + this.montarUrl(code), this.buildOptionsToken(null));
    //console.info(environment.URL_TOKEN);
    //console.info(environment.URL_TOKEN_ORIGINAL);
    //let blank = new TokenAim();
    //return this._http.post<TokenAim>(environment.URL_TOKEN, blank, this.buildOptionsToken(environment.URL_TOKEN_ORIGINAL + this.montarUrl(code)));

  }​​​​​​​


  getUserInfo(token: TokenAim): Observable<any> {​​​​​​​

    return this._http.get(environment.URL_USER_INFO_ORIGNAL, this.buildOptions(token.access_token));
    //return this._http.get(environment.URL_USER_INFO, this.buildOptions(token.access_token));
  }​​​​​​​

  public montarUrl(code: string): string {​​​​​​​

    let parameters = '?grant_type=' + this.param_grant_type
    + '&code=' + code
    + '&redirect_uri=' + environment.REDIRECT_URI
    + '&client_id=' + environment.CLIENT_ID
    + '&client_secret=' + environment.CLIENT_SECRET;

    return parameters;
  }​​​​​​​

  public buildOptions(access_token: string): any {​​​​​​​

    let headers = new HttpHeaders({​​​​​​​}​​​​​​​);

    headers = headers
      //.append('Accept', 'application/json')
      //.append('Content-Type', 'text/plain')
      //.append('url', environment.URL_USER_INFO_ORIGNAL)
      .append('authorization', ('Bearer ' + access_token.trim()))
      //.append('Access-Control-Request-Headers', 'authorization');
      //.append('Access-Control-Allow-Origin', '*');


    var options = {​​​​​​​ headers: headers }​​​​​​​;

    console.info(headers);

    return options;
  }​​​​​​​

  public buildOptionsToken(access_token: string): any {

    if (access_token == null)​​​{
      return null;
    }​​​​

    let headers = new HttpHeaders({​​​​​​​}​​​​​​​);

    headers = headers.append('Accept', '')
      .append('Content-Type', 'application/x-www-form-urlencoded')
      .append('url', access_token);
      //.append('Access-Control-Allow-Origin','*')
      //.append('Host', 'ids-dev.valeglobal.net')
      //.append('Origin','http://localhost:4200')
      //.append('Access-Control-Allow-Method', 'POST');

    var options = {​​​​​​​ headers: headers }​​​​​​​;

    console.info(headers);

    return options;
  }​​​​​​​


  saveToken(token:  TokenAim) {​​​​​​​
    var expireDate = new Date().getTime() + (1000 * token.expires_in);
    Cookie.set("access_token", token.access_token.trim(), expireDate);
    Cookie.set("refresh_token", token.refresh_token.trim(), expireDate);
    Cookie.set("access_token_exp", token.expires_in.toString(), expireDate);
    console.log('Obtained Access token');
  }​​​​​​​


  getTokenAim(): TokenAim {​​​​​​​

    let token = new TokenAim();

    token.access_token = Cookie.get('access_token');
    token.refresh_token = Cookie.get('refresh_token');
    token.expires_in = new Number(Cookie.get('access_token')).valueOf();

    return token;
  }​​​​​​​


  getResource(resourceUrl) : Observable<any> {​​​​​​​
    var headers = new HttpHeaders({​​​​​​​
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer '+ Cookie.get('access_token')}​​​​​​​);
    return this._http.get(resourceUrl, {​​​​​​​ headers: headers }​​​​​​​).pipe(
      catchError((error:any) => Observable.throw(error.json().error || 'Server error'))
    );
  }​​​​​​​


  checkCredentials(): boolean {​​​​​​​
    return Cookie.check('access_token');
  }​​​​​​​


  logout() {​​​​​​​
    this.resetAllCookies();
    window.location.href = environment.REDIRECT_LOGOUT_URI;
  }​​​​​​​
}
*/​​​​​​​
