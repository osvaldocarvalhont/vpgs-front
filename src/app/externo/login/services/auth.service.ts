import { AuthMode } from './../../../shared/enums/auth-mode.enum';
import { User } from './../../../shared/models/user.model';
import {​​​​​​​ environment }​​​​​​​ from '../../../../environments/environment';
import {​​​​​​​ HttpClient, HttpHeaders }​​​​​​​ from '@angular/common/http';
import {​​​​​​​Injectable}​​​​​​​ from '@angular/core';
import {​​​​​​​ Cookie }​​​​​​​ from 'ng2-cookies';
import {​​​​​​​ Observable }​​​​​​​ from 'rxjs';
import {​​​​​​​ catchError }​​​​​​​ from 'rxjs/operators';


@Injectable({​​​​​​​
  providedIn: 'root'
}​​​​​​​)
export class AuthService {​​​​​​​

  constructor(private _http: HttpClient) {​​​​​​​ }​​​​​​​


  redirectToAim(): void {​​​​​​​

    this.resetAllCookies();

    this._http.get<any>(environment.BASE_URL + "iam/redirect-url",{}).subscribe(url => window.location.href = url.url);
  }

 ​​​  resetAllCookies(): void {​​​​​​​

    Cookie.deleteAll('/', 'localhost');
    Cookie.deleteAll('/', 'vpgs.valeglobal.net');
    Cookie.delete('access_token');
    Cookie.delete('refresh_token');
    Cookie.delete('access_token_exp');
    Cookie.delete('access-code');

    localStorage.clear();
    console.log("Apagando cookies");
  }​​​​​​​​​​​​​​​​​​​​
​​
  getResource(resourceUrl) : Observable<any> {​​​​​​​
    var headers = new HttpHeaders({​​​​​​​
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer '+ Cookie.get('access_token')}​​​​​​​);
    return this._http.get(resourceUrl, {​​​​​​​ headers: headers }​​​​​​​).pipe(
      catchError((error:any) => Observable.throw(error.json().error || 'Server error'))
    );
  }​​​​​​​
  checkCredentials(): boolean {​​​​​​​
    return Cookie.check('access_token');
  }​​​​​​​


  logout() {

    var json: string   = localStorage.getItem('currentUser');
    var user: User  = JSON.parse(json);
    ​​​​​​​
    this.resetAllCookies();
    if (user.authMode === AuthMode.SAML) {
      this._http.get<any>(environment.BASE_URL + "iam/logout-iam",{}).subscribe(url => window.location.href = url.url);
    } else {
      this._http.get<any>(environment.BASE_URL + "iam/logout-vpgs",{}).subscribe(url => window.location.href = url.url);
    }
  ​​​​​​​}
}​​​​​​​
