import { User } from 'src/app/shared/models/user.model';
import { EsquecerSenha } from 'src/app/shared/models/esquecer-senha.model';
import { Login } from './../../shared/models/login.model';
import { FormBuilder  } from '@angular/forms';
import { ConfirmationService, MessageService, SelectItem } from 'primeng/api';
import { Component, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { CountryService } from 'src/app/interno/vpgs-adm/usuarios/services/country.service';
import { KeyValue } from 'src/app/shared/models/key-value.model';
import { AuthenticationService } from '../../externo/login/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from './services/auth.service';
import { Cookie } from 'ng2-cookies';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthMode } from 'src/app/shared/enums/auth-mode.enum';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ConfirmationService, MessageService, DialogService]
})
export class LoginComponent implements OnInit {

  public showEsqueciSenha = false;
  public showInformarSenha = false;
  public showCadastro = false;
  public optionsPais: SelectItem[];
  public showDuvidasLogin = true;

  public emailNovaSenha: string;
  public hashEmail: string;
  public novaSenha: string;
  public novaContraSenha: string;
  public getValueSolicitarNovaSenha: string;
  public getValueInformarNovaSenha: string;
  public getValueSolicitarCadastro: string;
  public getValueEmailObrigatorio: string;
  public getValueSenhaObrigatorio: string;
  public login: Login = new Login();
  public user: User = new User();
  public acessoForn: boolean = false;
  public loading: boolean = false;
  public submitted: boolean = false;
  public error = '';

  public msgSucesso: string;
  public msgErro: string;
  public showSucess: boolean;
  public showError: boolean;

  constructor(private fb: FormBuilder,
              private countryService: CountryService,
              private authenticationService: AuthenticationService,
              private router: Router,
              private route: ActivatedRoute,
              private messageService: MessageService,
              private translocoService: TranslocoService,
              private authService: AuthService) {
              }

  ngOnInit(): void {

    let url: string = this.router.url;
    console.log( "-> " + url);
    if (url.endsWith('logout')) {
      this.msgControll(false, 'Usuário deslogado com sucesso!!!');
      this.authService.logout();
      this.authService.resetAllCookies();
      return;
    }

    this.msgSucesso = '';
    this.msgErro = '';

    let subscription = this.authenticationService.exist('teste').subscribe({
      next: (ok) => {
        this.msgControll(false, 'Aplicação Online...');
      },
      error: (er) => {
        //this.msgControll(true, 'Aplicação for do Ar. Tente mais tarde...');
        //alert(this.msgErro);
        //console.error(er);
        //this.authService.resetAllCookies();
        this.redirect();
      },
      complete:() => {
        this.redirect();
      }
    });

    this.getCountries();
    this.getLabelTanslate();
  }

  private isServerOnline() : boolean {

    let pass = false;
    let obs = this.authenticationService.exist('testeOnline').subscribe({
      next: (ok) => {
        pass = true;
        console.info('aaaa')
      },
      error: (er) => {
        this.msgControll(true, 'Aplicação for do Ar. Tente mais tarde...');
        //console.error(er);
        this.authService.resetAllCookies();
      }
    });


    console.info('ue')

    return pass;
  }

  public loginFornecedor(): void  {

    //if (!this.isServerOnline()){
    //  console.info('nao')
    //  return;
    //}

    this.submitted = true;
    if((this.login.email === null || this.login.email === undefined) &&
       (this.login.senha === null || this.login.senha === undefined)) {
        const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        //console.log(returnUrl);
        this.router.navigate([returnUrl]);
    } else {
      this.loading = true;
      const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      //console.log(returnUrl);
      //this.router.navigate([returnUrl]);

      this.authenticationService.login(this.login.email, this.login.senha, null, null, false)
        .pipe(first())
        .subscribe({
            next: () => {
                // get return url from route parameters or default to '/'
                console.log(this.route.snapshot.queryParams['returnUrl'] || '/interno');
                this.showError = false;
                this.loading = false;
                //localStorage.setItem('serviceCenter', true + '' );

            },
            error: error => {
              this.errorlogin(error);
              this.loading = false;
            }, complete : () => {
              const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/interno';
              this.router.navigate([returnUrl]);
            }
        });
    }
  }

  public loginAim(code: string): void  {

    this.submitted = true;
    this.loading = true;

    this.authenticationService.login('', 'byPassPassword', null, code, true)
          .pipe(first())
          .subscribe({
              next: () => {
                // get return url from route parameters or default to '/'
                //console.log(this.route.snapshot.queryParams['returnUrl'] || '/interno');
                const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/interno';
                this.showError = false;
                this.loading = false;
                //localStorage.setItem('serviceCenter', true + '' );
                this.router.navigate([returnUrl]);
              },
              error: error => {
                this.errorlogin(error);
                this.loading = false;
              }
          });
  }

  private errorlogin(error: any) : void {
    //console.log(error)

    if (error.error) {
      this.msgControll(true, error.error.message);
    } else if (error instanceof HttpErrorResponse) {
      this.msgControll(true, 'Servidor OFFLINE!');
    } else {
      this.msgControll(true, 'Erro inesperado no login!');
    }
    this.showError = true;
  }

  //Abrir tela esqueci senha
  public openDialogEsqueciSenha(): void {
    this.showEsqueciSenha = true;
  }

  //Abrir tela nova senha
  private openNovaSenha(hash: string) : void {
    this.showInformarSenha = true;
    this.hashEmail = hash;
    this.novaSenha = null;
    this.novaContraSenha = null;
  }

  //Abrir tela cadastro user
  public openDialogCadastro(): void {
    this.showCadastro = true;
  }

  //confirmar botão tela esqueci senha
  public solicitarNovaSenha(): void {

    if (!this.loading && this.emailNovaSenha !== null) {
      this.loading = true;

      this.showInformarSenha = false;
      this.authenticationService.solicitarNovaSenha(this.emailNovaSenha).pipe(first())
      .subscribe({
          next: () => {
              // get return url from route parameters or default to '/'
              this.error = "";
              this.showEsqueciSenha = false;
              this.msgControll(false, "Solicitação de nova senha enviada para: " + this.emailNovaSenha);
              this.emailNovaSenha = null;
          },
          error: error => {
            this.error = 'ERRO: ' + error.error.message;
            if(this.error) {
              this.error = 'ERRO: ' + error.message;
            }
            //console.log(error);
            this.showError = true;
            this.msgControll(true, this.error);
            this.showInformarSenha = true;
          },
          complete: () => {
            this.loading = false;
          }
      });
    }
  }

  //confirmar botão tela nova senha
  public confirmarNovaSenha(): void {
    debugger;
    if (this.novaSenha !== null && this.novaContraSenha !== null && this.novaSenha === this.novaContraSenha) {
      //console.log(this.emailNovaSenha);

      this.loading = true;

      var dto:EsquecerSenha = new EsquecerSenha();
      dto.senha = this.novaSenha;
      dto.contraSenha = this.novaContraSenha;
      dto.hash = this.hashEmail;
      this.authenticationService.confirmarNovaSenha(dto).pipe(first())
      .subscribe({
          next: () => {
              this.error = "";
              this.showError = false;
              this.showInformarSenha = false;
              this.msgControll(false, 'Senha do usuário atualizada com sucesso!');
          },
          error: error => {
            this.error = 'ERRO: ' + error.error.message;
            if(this.error) {
              this.error = 'ERRO: ' + error.message;
            }
            //console.log(error);
            this.showError = true;
            this.loading = false;
            this.msgControll(true, this.error);
          }
      });
    } else {
      this.msgControll(true, 'Senha diferente da Contra senha informada!');
    }
  }

  //confirmar botão cadastro user
  public cadastrar(): void {
    this.cadastrarUser(AuthMode.INTERNAL)
  }

  public cadastrarUser(authMode: AuthMode): boolean {

    this.loading = true;
    this.user.authMode = authMode;
    this.authenticationService.cadastrarUser(this.user).pipe(first())
      .subscribe({
          next: () => {
              // get return url from route parameters or default to '/'
              this.error = "";
              this.showError = false;
              this.showCadastro = false;
              this.msgControll(false, 'Usuário Cadastrado!');
              this.loading = true;
          },
          error: error => {
            this.error = 'ERRO: ' + error.error.message;
            if(this.error === undefined) {
              this.error = 'ERRO: ' + error.message;
            }
            this.showError = true;
            this.loading = false;
            this.msgControll(true, this.error);
          }
      });

      return this.showError;
  }

  public changeLang(language: string): void {
    this.translocoService.setActiveLang(language);
    if(language === 'en' || language === 'es') {
      this.showDuvidasLogin = false;
    } else {
      this.showDuvidasLogin = true;
    }
  }

  public acessoVale(): void {
    //if (this.isServerOnline()) {
      this.acessoForn = false;
      this.authService.redirectToAim();
    //}
  }

  public acessoFornecedor(): void {
    //console.info('Forn1');
    //if (this.isServerOnline()) {
      console.info('Forn2');
      this.acessoForn = true;
      this.user = new User();
    //}
  }

  private redirect(): void {
    let code: string = this.route.snapshot.queryParamMap.get('code');
    let hash: string = this.route.snapshot.queryParamMap.get('forgotten-email');
    let cadastro: string = this.route.snapshot.queryParamMap.get('confirmar-cadastro');
    //console.info('TESTE');
    console.info(code);
    console.info(hash);
    console.info(cadastro);

    if (cadastro != undefined && hash != cadastro) {
      this.msgControll(false, 'Confirmando solititação de ativação por email...');
      this.authenticationService.confimarCadastro(cadastro).subscribe({
        next: (ok) => {
          //console.info('Usuário ativado...');
          this.msgControll(false, 'Usuário ativado...');
          this.acessoFornecedor();
          this.user.email = ok.email;
        },
        error: (er) => {
          this.msgControll(true, this.msgErro);
          console.error(er);
        },
        complete:() => {
        }
      });

    } else if (hash != undefined && hash != null) {
      this.openNovaSenha(hash);
    } else if (code) {
      this.loading = true;
      this.msgControll(false, 'Validando credenciais do usuário...');
      Cookie.set('access-code', code);
      this.loginAim(code);
    }
  }

  public getCountries(): void {
    this.countryService.findAllCountries().subscribe(res => {
      this.optionsPais = KeyValue.convertToSelectItem(res);
    });
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('solicitarNovaSenha').subscribe(value => {
      this.getValueSolicitarNovaSenha = value;
    });
    this.translocoService.selectTranslate('solicitarCadastro').subscribe(value => {
      this.getValueSolicitarCadastro = value;
    });
    this.translocoService.selectTranslate('login-email-obrigatorio').subscribe(value => {
      this.getValueEmailObrigatorio = value;
    });
    this.translocoService.selectTranslate('login-senha-obrigatorio').subscribe(value => {
      this.getValueSenhaObrigatorio = value;
    });

    this.translocoService.selectTranslate('solicitarNovaSenha').subscribe(value => {
      this.getValueInformarNovaSenha = value;
    });
  }

  private msgControll(isError: boolean, msg: string): void {
    if (!isError) {
      this.msgSucesso = msg;
      this.showModal('success', 'Mensagem', msg);
    } else {
      this.msgErro = msg;
      this.showModal('error', 'Erro', msg);
    }
    this.showSucess = !isError;
    this.showError = isError;
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

}
