/* import { User } from 'src/app/shared/models/user.model';
import { EsquecerSenha } from 'src/app/shared/models/esquecer-senha.model';
import { Login } from '../../shared/models/login.model';
import { TokenAim } from 'src/app/shared/models/token-aim.model';
import { UserInfo } from 'src/app/shared/models/user-info.model';
import { FormBuilder  } from '@angular/forms';
import { MessageService, SelectItem } from 'primeng/api';
import { Component, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { CountryService } from 'src/app/interno/vpgs-adm/usuarios/services/country.service';
import { KeyValue } from 'src/app/shared/models/key-value.model';
import { AuthenticationService } from './services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from './services/auth.service';
import { Cookie } from 'ng2-cookies';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthMode } from 'src/app/shared/enums/auth-mode.enum';
import { UserStatus } from 'src/app/shared/enums/user-status.enum';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {

  public showEsqueciSenha = false;
  public showInformarSenha = false;
  public showCadastro = false;
  public optionsPais: SelectItem[];
  public showDuvidasLogin = true;

  public emailNovaSenha: string;
  public hashEmail: string;
  public novaSenha: string;
  public novaContraSenha: string;
  public getValueSolicitarNovaSenha: string;
  public getValueInformarNovaSenha: string;
  public getValueSolicitarCadastro: string;
  public getValueEmailObrigatorio: string;
  public getValueSenhaObrigatorio: string;
  public login: Login = new Login();
  public user: User = new User();
  public acessoForn: boolean = false;
  public loading: boolean = false;
  public submitted: boolean = false;
  public error = '';

  public msgSucesso: string;
  public msgErro: string;
  public showSucess: boolean;
  public showError: boolean;

  constructor(private fb: FormBuilder,
              private countryService: CountryService,
              private authenticationService: AuthenticationService,
              private router: Router,
              private route: ActivatedRoute,
              private messageService: MessageService,
              private translocoService: TranslocoService,
              private authService: AuthService) {
                //if (this.authenticationService.currentUserValue) {
                //  this.router.navigate(['interno']);
                //}
              }

  ngOnInit(): void {
    this.getCountries();
    this.getLabelTanslate();

    let url: string = this.router.url;

    if (url.endsWith('logout')) {
      this.authService.resetAllCookies();
      this.msgControll(false, 'Entre na página novamente...');
      return;
    }

    this.msgSucesso = '';
    this.msgErro = '';

    let subscription = this.authenticationService.exist('teste').subscribe({
      next: (ok) => {
        this.msgControll(false, 'Aplicação Online...');
      },
      error: (er) => {
        this.msgControll(true, 'Aplicação for do Ar. Tente mais tarde...');
        alert(this.msgErro);
        console.error(er);
        this.authService.resetAllCookies();
      },
      complete:() => {
        this.redirect();
      }
    });
  }

  public getCountries(): void {
    this.countryService.findAllCountries().subscribe(res => {
      this.optionsPais = KeyValue.convertToSelectItem(res);
    });
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('solicitarNovaSenha').subscribe(value => {
      this.getValueSolicitarNovaSenha = value;
    });
    this.translocoService.selectTranslate('solicitarCadastro').subscribe(value => {
      this.getValueSolicitarCadastro = value;
    });
    this.translocoService.selectTranslate('login-email-obrigatorio').subscribe(value => {
      this.getValueEmailObrigatorio = value;
    });
    this.translocoService.selectTranslate('login-senha-obrigatorio').subscribe(value => {
      this.getValueSenhaObrigatorio = value;
    });

    this.translocoService.selectTranslate('solicitarNovaSenha').subscribe(value => {
      this.getValueInformarNovaSenha = value;
    });
  }

  public loginFornecedor(): void  {
    this.submitted = true;
    if((this.login.email === null || this.login.email === undefined) &&
       (this.login.senha === null || this.login.senha === undefined)) {
        const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        console.log(returnUrl);
        this.router.navigate([returnUrl]);
    } else {
      this.loading = true;
      const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      console.log(returnUrl);
                    this.router.navigate([returnUrl]);

      this.authenticationService.login(this.login.email, this.login.senha, null, true)
        .pipe(first())
        .subscribe({
            next: () => {
                // get return url from route parameters or default to '/'
                console.log(this.route.snapshot.queryParams['returnUrl'] || '/interno');
                const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/interno';
                this.showError = false;
                this.loading = false;
                localStorage.setItem('serviceCenter', true + '' );
                this.router.navigate([returnUrl]);
            },
            error: error => {
              console.log(error)
                this.error = error.error.message;
                this.loading = false;
                this.showError = true;
            }
        });
    }
  }

  public loginAim(user: UserInfo, token: TokenAim): void  {

    var tree = user.groupMembership;
    let access: boolean = false;
    let unificada: boolean = false;
    let dashboard: boolean = false;
    let midas: boolean = false;
    let serviceCenter: boolean = false;
    //Verifica se a propriedade é um Array de String ou uma String
    if (!Array.isArray(tree)) {
      tree = [tree];
    }
    tree.forEach(node => {
      var treeItems: string[] = node.split(',');
      treeItems.forEach(item => {
        let i = item.trim().toLocaleUpperCase();
        if (i.startsWith('OU=')) {
            access = i.endsWith('VPGS');
            unificada     = unificada     || i.endsWith('VPGS');
            dashboard     = dashboard     || i.endsWith('VPGS');
            midas         = midas         || i.endsWith('VPGS');
            serviceCenter = serviceCenter || i.endsWith('VPGS');
        }
      });
      localStorage.setItem('unificada',     unificada + '' );
      localStorage.setItem('dashboard',     dashboard + '' );
      localStorage.setItem('midas',         midas + '' );
      localStorage.setItem('serviceCenter', serviceCenter + '' );
    });

    this.submitted = true;

    this.loading = true;

    this.authenticationService.exist(user.cn)
          .pipe(first())
          .subscribe({
              next: hasUser => {
                this.showError = false;
                if (!hasUser) {
                  this.user = new User();
                  this.user.companyCode = user.cn;
                  this.user.firstName = user.UserFullName.split(" ",2)[0];
                  this.user.lastName = user.UserFullName.split(" ",2)[1];
                  this.user.fullName = user.UserFullName;
                  this.user.status = UserStatus.ACTIVE;
                  this.user.country = user.locationCountry;
                  this.user.taxCode = null;
                  this.user.email = null;
                  if (this.cadastrarUser(AuthMode.SAML)) {
                    return;
                  }
                }
              },
              error: error => {
                this.user = new User();
                this.errorlogin(error);
                return;
              }
            });

    this.authenticationService.login(user.cn, 'byPassPassword', tree, true)
          .pipe(first())
          .subscribe({
              next: () => {
                // get return url from route parameters or default to '/'
                console.log(this.route.snapshot.queryParams['returnUrl'] || '/interno');
                const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/interno';
                this.showError = false;
                this.loading = false;
                this.router.navigate([returnUrl]);
              },
              error: error => {
                this.errorlogin(error);
                this.loading = false;
              }
          });
  }

  private errorlogin(error: any) : void {
    console.log(error)

    if (error instanceof HttpErrorResponse) {
      this.msgControll(true, 'Servidor OFFLINE!');
    } else {
      this.msgControll(true, error.error.message);
    }
    this.showError = true;
  }

  //Abrir tela esqueci senha
  public openDialogEsqueciSenha(): void {
    this.showEsqueciSenha = true;
  }

  //Abrir tela nova senha
  private openNovaSenha(hash: string) : void {
    this.showInformarSenha = true;
    this.hashEmail = hash;
    this.novaSenha = null;
    this.novaContraSenha = null;
  }

  //Abrir tela cadastro user
  public openDialogCadastro(): void {
    this.showCadastro = true;
  }

  //confirmar botão tela esqueci senha
  public solicitarNovaSenha(): void {
    if (this.emailNovaSenha !== null) {


      this.loading = true;

      this.authenticationService.solicitarNovaSenha(this.emailNovaSenha).pipe(first())
      .subscribe({
          next: () => {
              // get return url from route parameters or default to '/'
              this.error = "";
              this.emailNovaSenha = null;
              this.showEsqueciSenha = false;
              this.msgControll(false, "Solicitação de nova senha enviada para: " + this.emailNovaSenha);
          },
          error: error => {
            console.log(error)
            this.errorlogin(error);
            this.loading = false;
          }
      });
    }
  }

  //confirmar botão tela nova senha
  public confirmarNovaSenha(): void {
    debugger;
    if (this.novaSenha !== null && this.novaContraSenha !== null && this.novaSenha === this.novaContraSenha) {
      //console.log(this.emailNovaSenha);

      this.loading = true;

      var dto:EsquecerSenha = new EsquecerSenha();
      dto.senha = this.novaSenha;
      dto.contraSenha = this.novaContraSenha;
      dto.hash = this.hashEmail;
      this.authenticationService.confirmarNovaSenha(dto).pipe(first())
      .subscribe({
          next: () => {
              // get return url from route parameters or default to '/'
              this.error = "";
              this.showError = false;
              this.showInformarSenha = false;
              this.msgControll(false, 'Senha do usuário atualizada com sucesso!');
          },
          error: error => {
            console.log(error)
            this.errorlogin(error);
            this.loading = false;
          }
      });
    } else {
      this.msgControll(true, 'Senha diferente da Contra senha informada!');
    }
  }

  //confirmar botão cadastro user
  public cadastrar(): void {
    this.cadastrarUser(AuthMode.INTERNAL)
  }

  public cadastrarUser(authMode: AuthMode): boolean {

    this.loading = true;
    this.user.authMode = authMode;
    this.authenticationService.cadastrarUser(this.user).pipe(first())
      .subscribe({
          next: () => {
              // get return url from route parameters or default to '/'
              this.error = "";
              this.showError = false;
              this.showCadastro = false;
              this.msgControll(false, 'Usuário Cadastrado!');
          },
          error: error => {
            console.log(error)
            this.errorlogin(error);
            this.loading = false;
          }
      });

      return this.showError;
  }

  public changeLang(language: string): void {
    this.translocoService.setActiveLang(language);
    if(language === 'en' || language === 'es') {
      this.showDuvidasLogin = false;
    } else {
      this.showDuvidasLogin = true;
    }
  }

  public acessoVale(): void {
    this.acessoForn = false;
    this.authService.redirectToAim();
  }

  public acessoFornecedor(): void {
    this.acessoForn = true;
    this.user = new User();
    //consulta o bd vpgs
  }

  private redirect(): void {
    let code: string = this.route.snapshot.queryParamMap.get('code');
    let hash: string = this.route.snapshot.queryParamMap.get('forgotten-email');
    let cadastro: string = this.route.snapshot.queryParamMap.get('confirmar-cadastro');

    if(code) {
      console.info(code);
      //return;
    }

    if (cadastro != undefined && hash != cadastro) {
      this.msgControll(false, 'Confirmando solititação de ativação por email...');
      this.authenticationService.confimarCadastro(hash).subscribe({
        next: (ok) => {
          console.info('Usuário ativado...');
          this.acessoFornecedor();
          this.user.email = ok.email;
          this.msgControll(false, 'Usuário ativado...');
        },
        error: (er) => {
          this.msgControll(true, this.msgErro);
          console.error(er);
        },
        complete:() => {
        }
      });

    } else if (hash != undefined && hash != null) {
      this.openNovaSenha(hash);
    } else if (code == null || code == Cookie.get('access-code')) {
      console.log('Redirect');
      //this.authService.redirectToAim();
    } else {
      this.loading = true;
      this.msgControll(false, 'Soliticando credenciais do usuário...');
      Cookie.set('access-code', code);
      this.doGetToken(code);
    }
  }

  private doGetToken(code: string): void {
    let token: TokenAim = null;
    this.authService.getToken(code).subscribe({
      next: (ok) => {
        console.info(ok);
        this.authService.saveToken(ok);
        this.msgControll(false, 'Token de Acesso do Usuário obtido. Buscando dados do Usuário...');
        token = ok;
      },
      error: (er) => {
        this.msgControll(true, 'Erro em obter o Token de Acesso do Usuário.');
        alert(this.msgErro);
        console.error(er);
        this.authService.resetAllCookies();
      },
      complete:() => {
        this.doUserInfo(token);
      }
    });
  }

  private doUserInfo(token: TokenAim): void {

    let user: UserInfo = null;

    this.authService.getUserInfo(token).subscribe({
      next: (ok) => {
        this.msgControll(false, 'Dados do Usuário obtido. Solicitando acesso a aplicação...');
        user = ok;
        console.log(ok);
      },
      error: (er) => {
        this.msgControll(true, 'Erro em obter o Dados do Usuário.');
        console.error(er);
        this.authService.resetAllCookies();
      },
      complete: () => {
        this.loginAim(user, token);
      }
    });
  }

  private msgControll(isError: boolean, msg: string): void {
    if (!isError) {
      this.msgSucesso = msg;
    } else {
      this.msgErro = msg;
    }
    this.showSucess = !isError;
    this.showError = isError;
  }

}
 */
