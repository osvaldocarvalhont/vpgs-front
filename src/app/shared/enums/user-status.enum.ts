export enum UserStatus {
  ACTIVE = 'ACTIVE',
  WAITING_CONFIRMATION = 'WAITING_CONFIRMATION',
  REMOVED = 'REMOVED',
  INACTIVE = 'INACTIVE'
}

export const UserStatusPt = new Map<string, string>([
  ['ACTIVE', 'ATIVO'],
  ['WAITING_CONFIRMATION', 'AGUARDANDO CONFIRMAÇÃO'],
  ['REMOVED', 'REMOVIDO'],
  ['INACTIVE', 'INATIVO']
]);
