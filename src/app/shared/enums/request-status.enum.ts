export enum RequestStatus {
  OPEN = 'Aberto',
  CLOSED = 'Fechado',
  INACTIVATED = 'Inativado',
  ACCEPTED = 'Aceito',
  REJECTED = 'Rejeitado',
  WAITING_CONFIRMATION = 'Aguardando Confirmação'
}
