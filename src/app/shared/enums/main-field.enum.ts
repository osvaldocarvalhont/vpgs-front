import { KeyValue } from './../models/key-value.model';
export class MainField {

  public static MAIN_FIELD = [
    new KeyValue('COMPANYCODE', 'Matrícula'),
    new KeyValue('AUTHMODE', 'Tipo Acesso'),
    new KeyValue('COUNTRY', 'País'),
    new KeyValue('TAXCODE', 'CNPJ/CPF'),
    new KeyValue('EMAIL', 'Email')
  ];

  public static get COMPANYCODE(): KeyValue {
    return new KeyValue('COMPANYCODE', 'Matrícula');
  }

  public static get AUTHMODE(): KeyValue {
    return new KeyValue('AUTHMODE', 'Tipo Acesso');
  }

  public static get COUNTRY(): KeyValue {
    return new KeyValue('COUNTRY', 'País');
  }

  public static get TAXCODE(): KeyValue {
    return new KeyValue('TAXCODE', 'CNPJ/CPF');
  }

  public static get EMAIL(): KeyValue {
    return new KeyValue('EMAIL', 'Email');
  }

}
