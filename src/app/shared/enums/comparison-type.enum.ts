import { KeyValue } from './../models/key-value.model';
export class ComparisonType {

  public static COMPARISON_TYPE = [
    new KeyValue('EQUALS', 'Igual'),
    new KeyValue('CONTAINS', 'Contém'),
    new KeyValue('NOT_CONTAINS', 'Não Contém'),
    new KeyValue('DIFFERENT', 'Diferente')
  ];

  public static get EQUALS(): KeyValue {
    return new KeyValue('EQUALS', 'Igual');
  }

  public static get CONTAINS(): KeyValue {
    return new KeyValue('CONTAINS', 'Contém');
  }

  public static get NOT_CONTAINS(): KeyValue {
    return new KeyValue('NOT_CONTAINS', 'Não Contém');
  }

  public static get DIFFERENT(): KeyValue {
    return new KeyValue('DIFFERENT', 'Diferente');
  }

}
