import { AuthMode } from './../../enums/auth-mode.enum';
import { ComparisonType } from '../../enums/comparison-type.enum';
import { FiltroFiltrosPadrao } from '../..//models/filtro-filtros-padrao.model';
import { Component, OnInit } from '@angular/core';
import { MainField } from '../../enums/main-field.enum';

@Component({
  selector: 'app-base-core',
  templateUrl: './base-core.component.html',
  styleUrls: ['./base-core.component.css']
})
export class BaseCoreComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public setComparisonType(comparisonType: string): string {
    let newValue: string = '';
    switch (comparisonType) {
      case 'EQUALS':
        newValue = ComparisonType.EQUALS.value;
        break;
      case 'CONTAINS':
        newValue = ComparisonType.CONTAINS.value;
        break;
      case 'NOT_CONTAINS':
        newValue = ComparisonType.NOT_CONTAINS.value;
        break;
      case 'DIFFERENT':
        newValue = ComparisonType.DIFFERENT.value;
        break;
    }
    return newValue;
  }

  public setMainField(mainField: string): string {
    let newValue: string = '';
    switch (mainField) {
      case 'COMPANYCODE':
        newValue = MainField.COMPANYCODE.value;
        break;
      case 'AUTHMODE':
        newValue = MainField.AUTHMODE.value;
        break;
      case 'COUNTRY':
        newValue = MainField.COUNTRY.value;
        break;
      case 'TAXCODE':
        newValue = MainField.TAXCODE.value;
        break;
      case 'EMAIL':
        newValue = MainField.EMAIL.value;
        break;
    }
    return newValue;
  }

  public setAuthMode(authMode: string): string {
    let newValue: string = '';
    switch (authMode) {
      case 'SAML':
        newValue = AuthMode.SAML;
        break;
      case 'INTERNAL':
        newValue = AuthMode.INTERNAL;
        break;
    }
    return newValue;
  }

  public setCountry(country: string): string {
    let newValue: string = '';
    switch (country) {
      case 'CA':
        newValue = 'Canadá';
        break;
      case 'AE':
        newValue =  'Emirados Árabes Unidos';
        break;
      case 'AR':
        newValue =  'Argentina';
        break;
      case 'AT':
        newValue =  'Áustria';
        break;
      case 'AU':
        newValue =  'Austrália';
        break;
      case 'CH':
        newValue =  'Suiça';
        break;
      case 'CL':
        newValue =  'Chile';
        break;
      case 'CN':
        newValue =  'China';
        break;
      case 'GB':
        newValue =  'Reino Unido';
        break;
      case 'ID':
        newValue =  'Indonésia';
        break;
      case 'IN':
        newValue =  'Índia';
        break;
      case 'JP':
        newValue =  'Japão';
        break;
      case 'KR':
        newValue =  'Coreia do Sul';
        break;
      case 'MW':
        newValue =  'Malawi';
        break;
      case 'MY':
        newValue =  'Malásia';
        break;
      case 'MZ':
        newValue =  'Moçambique';
        break;
      case 'NC':
        newValue =  'Nova Caledônia';
        break;
      case 'OM':
        newValue =  'Omã';
        break;
      case 'PE':
        newValue =  'Peru';
        break;
      case 'PH':
        newValue =  'Filipinas';
        break;
      case 'PY':
        newValue =  'Paraguai';
        break;
      case 'SG':
        newValue =  'Cingapura';
        break;
      case 'TW':
        newValue =  'Taiwan';
        break;
      case 'US':
        newValue =  'Estados Unidos';
        break;
      case 'ZM':
        newValue =  'Zâmbia';
        break;
      case 'BR':
        newValue =  'Brasil';
        break;
      case 'NL':
        newValue =  'Netherlands';
        break;
    }
    return newValue;
  }

  public setLocale(locale: string): string {
    let localeLabel = '';
    switch (locale) {
      case 'pt':
        localeLabel = 'Português';
        break;
      case 'en':
        localeLabel = 'Inglês';
        break;
      case 'es':
        localeLabel = 'Espanhol';
        break;
    }
    return localeLabel;
  }

}
