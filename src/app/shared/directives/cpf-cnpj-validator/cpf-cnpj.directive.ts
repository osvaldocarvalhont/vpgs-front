import { Directive } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl, ValidationErrors } from '@angular/forms';
import { CpfCnpjValidator } from './cpf-cnpj.validator';


@Directive({
    selector: '[appCpfCnpjValidate][ngModel]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: CpfCnpjDirective,
        multi: true
    }]
})
export class CpfCnpjDirective extends CpfCnpjValidator implements Validator { }
