import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http'
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from "@angular/router";
//import { AuthenticationService } from '../../externo/login/services/authentication.service';



@Injectable({
  providedIn: 'root'
})
export class HttpClientInterceptor implements HttpInterceptor {
    constructor(private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add auth header with jwt if user is logged in and request is to the api url

        //console.log(request.url.toString());
        if (request.url.startsWith('https://ids-qa.valeglobal.net') || request.url.startsWith('https://ids-dev.valeglobal.net') || request.url.startsWith('https://ids-prd.valeglobal.net')){
          return next.handle(request);
        }

        var token = localStorage.getItem("token");

        if (token) {
          //console.info('Token: ' + token)
          request = request.clone({
            setHeaders: {
                Authorization: token
            }
          });
        } else {
          console.info('Sem token')
        }

        return next.handle(request).pipe(
          tap(event => {
            //console.log(event);
            if (event instanceof HttpResponse) {
              console.log(event);
                if (event.headers) {
                  let newToken = event.headers.get('Authorization');
                  //console.log(newToken);
                  if (newToken) {
                    localStorage.setItem("token", newToken);
                    //console.log(newToken);
                    //console.log(event);
                    //console.log("Interceptor - HttpResponse = " + event.status); // http response status code

                    //if (request.url.endsWith('login') && event.status === 200) {
                    //  this.router.navigate(['/interno']);
                    //}
                  }
                } else {
                  console.log("sem token");
                }
            }
          }, error => {
            // http response status code
            if (error instanceof HttpErrorResponse) {
              console.log("----response----");
              console.log(error);
              console.error("status code:");
              console.error(error.status);
              console.error(error.message);
              console.log(error.error);
              console.log("--- end of response---");
              console.log("----error object----");
              console.log(request);

              if (error.status === 401) {
                //this.showModal('error', 'Erro', error.error.message);
              } else if (error.status === 403) {
                //this.showModal('error', 'Erro', error.error.message);
                this.router.navigate(['']);
              } else if (error.status === 0) {
                //this.showModal('error', 'Erro', 'Servidor OFFLINE.');
                if (request.url.endsWith('status')) {
                  this.router.navigate(['/externo/login/logout']);
                }
              }
            /* TRATAR ERRO 500 NA TELA CHAMADORA
              else if (error.status === 500) {
                alert(error.error.message);
              }*/
            }
          })
        );
    }
}
