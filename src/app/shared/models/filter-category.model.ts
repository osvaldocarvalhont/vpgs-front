import { FilterGeneral } from './filter-general.model';

export class FilterCategory extends FilterGeneral{
  locale: number;
	isPersistent: string;
	isLogged: string;
  name: string;
}
