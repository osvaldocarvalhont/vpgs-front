import { FiltroFiltros } from './filtro-filtros.model';
import { FiltroPermissoes } from './filtro-permissoes.model';
export class FiltroPermissoesPadrao {
  checkHabilitado: boolean;
  nome: string;
  permissoes: FiltroPermissoes[] = [];
  filtros: FiltroFiltros[] = [];
}
