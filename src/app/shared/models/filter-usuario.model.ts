export class FilterUsuario {
  email?: string;
  cpfCnpj?: string;
  matricula?: string;
  nome?: string;
  situacao?: string;
  page?: any;
  size?: any;
  orderBy?: string;
  direction?: string;
}
