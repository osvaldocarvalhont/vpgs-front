import { Faq } from 'src/app/shared/models/faq.model';
import { UserPermission } from "./user-permission.model";

export class FaqCategory {
  id: number;
  name: string;
  position: number;
  locale: string;
  localeId: number;
  isPersistent: string;
  isLogged: string;
  permission?: UserPermission[] = [];
  faqs?: Faq[] = [];
}
