import { SelectItem } from 'primeng/api';

export class KeyValue {

  id: any;
  value: any;

  constructor(id?: any, value?: any) {
    this.id = id;
    this.value = value;
}

  // tslint:disable-next-line: member-ordering
  public static convertToSelectItem(kv: KeyValue[]): SelectItem[] {

    let selectItem: SelectItem[] = [];
    for (let item of kv) {
      selectItem.push({label: item.value, value: item.id});
    }
    return selectItem;
  }

}
