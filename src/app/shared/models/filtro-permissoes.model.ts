import { KeyValue } from './key-value.model';
export class FiltroPermissoes {
  id?: number;
  aplicacao: string;
  grupo: string;
  permissao: string;
  aplicacaoKey: any;
  grupoKey: string;
  permissaoKey: string;
  aplicacaoP: KeyValue;
  grupoP: KeyValue;
  permissaoP: KeyValue;
  isRemoved?: boolean;
}
