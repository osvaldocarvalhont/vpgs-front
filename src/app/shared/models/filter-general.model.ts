export class FilterGeneral {
  page?: any;
  size?: any;
  orderBy?: string;
  direction?: string;
}
