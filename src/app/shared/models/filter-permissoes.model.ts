export class FilterPermissoes {
  email?: string;
  serviceProvider?: string;
  group?: string;
  permission?: string;
  page?: any;
  size?: any;
  orderBy?: string;
  direction?: string;
}
