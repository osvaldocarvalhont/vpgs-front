import { FilterGeneral } from './filter-general.model';

export class FilterFaqCategoryQuestion extends FilterGeneral{
  category: number;
  question: string;
}
