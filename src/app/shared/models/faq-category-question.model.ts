export class FaqCategoryQuestion {
  id: number;
  categoryName: string;
  question: string;
}
