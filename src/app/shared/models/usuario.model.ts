export class Usuario {
  id: number;
  email: string;
  cpfCnpj: string;
  matricula?: string;
  nome: string;
  sobrenome?: string;
  pais: string;
  tipoAcesso: string;
  situacao: string;
}
