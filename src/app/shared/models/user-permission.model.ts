export class UserPermission {
  id?: number;
  serviceProviderId: number;
  serviceProvider: string;
  groupId: string;
  group: string;
  permissionId: string;
  permission: string;
  isRemoved: boolean;
  emailUser?: string;
}
