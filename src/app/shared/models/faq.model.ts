export class Faq {
  id?: number;
  category?: number;
  categoryName?: string;
  question?: string;
  answer?: string;
  position?: number;
}
