export class Permissoes {
  id: number;
  usuario: string;
  aplicacao: string;
  grupo: string;
  permissao: string;
}
