export class Mensagem {
  id?: number;
  fromId?: string;
  assunto?: string;
  mensagen?: string;
  data?: string;
  arquivada?: boolean;
  importante?: boolean;
  lida?: boolean;
  status?: string;

}
