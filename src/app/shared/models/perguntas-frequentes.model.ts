import { Faq } from '../../../../src/app/shared/models/faq.model';

export class PerguntasFrequentes {
  faqCategory: string;
  faq: Faq[];
}
