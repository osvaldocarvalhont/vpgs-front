export class Request {
  id: number;
  createdOn: Date;
  descriptiOn: string;
  rejectionNote: string;
  status: string;
  type: string;
  userId: number;
  email: string;
  firstName: string;
  lastName: string;
  serviceProvider: any;
  serviceProviderId: number;
}
