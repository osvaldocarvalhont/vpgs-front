export class FiltroFiltrosPadrao {
  id?: number;
  mainField?: string;
  comparisonType?: string;
  mainValue?: string;
  mainFieldValue?: string;
  comparisonTypeValue?: string;
  mainValueValue?: string;
  isRemoved?: boolean;
  standardProfile?: number;
}
