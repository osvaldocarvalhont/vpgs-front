export class Categoria {
  id: number;
  idioma: string;
  nome: string;
  isPersisten: boolean;
  isLogged: boolean;
}
