import { FilterPermissoes } from './filter-permissoes.model';
import { FiltroFiltrosPadrao } from "./filtro-filtros-padrao.model";
import { UserPermission } from "./user-permission.model";

export class PermissoesPadrao {
  id: number;
  isEnabled: boolean;
  name: string;
  filterPermission?: FiltroFiltrosPadrao[] =[];
  permission?: UserPermission[] = [];

}
