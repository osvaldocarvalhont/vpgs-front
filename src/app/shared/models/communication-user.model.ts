export class CommunicationUser {
  id: number;
  title: string;
  message: string;
  creationDate: Date;
  emailed: boolean;
  archived: boolean;
  read: boolean;
  important: boolean;
  readdate: Date;
  archiveddate: Date;
}
