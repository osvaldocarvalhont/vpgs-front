import { UserPermission } from './user-permission.model';
export class User {
  id: number;
  email: string;
  taxCode: string;
  companyCode: string;
  firstName: string;
  lastName: string;
  fullName: string;
  country: string;
  countryId: number;
  authMode: any;
  status: string;
  userPermission?: UserPermission[] = [];
  password?: string;
  newPassword?: string;
  confNewPassword?: string;
  login: string;
}
