export class TokenAim {
  expires_in: number;
  access_token: string;
  token_type: string;
  refresh_token: string;
  scope: string;
  validade: Date = new Date();
}