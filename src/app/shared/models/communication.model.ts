export class Communication {
  id: number;
  from: string;
  fromId: number;
  title: string;
  message: string;
  creationDate: Date;
  lastUpdate: Date;
  emailed: boolean;
  status: string;
  communicationUsers: string[] = [];
  communicationUsersId: number[] = [];
}
