import { TokenAim } from "./token-aim.model";

export class UserInfo {
    sub: string;
    claims: string[];
    cn: string;
    UserFullName: string;
    locationCountry: string;
    groupMembership: string[];
    //token: TokenAim;
}