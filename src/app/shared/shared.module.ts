import { CpfCnpjDirective } from './directives/cpf-cnpj-validator/cpf-cnpj.directive';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './components/panel/panel/panel.component';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faTimes, faSearch, faFileExcel, faCheckCircle, faPlusSquare, faTrashAlt, faEdit, faEye, faCopy } from '@fortawesome/free-solid-svg-icons';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PanelModule } from 'primeng/panel';
import { EditorModule } from 'primeng/editor';
import { MensagemCoreComponent } from './components/mensagem-core/mensagem-core/mensagem-core.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { SeletorPermissaoComponent } from './components/seletor-permissao/seletor-permissao/seletor-permissao.component';
import { from } from 'rxjs';
import { ToastModule } from 'primeng/toast';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { SeletorFiltroPermissaoComponent } from './components/seletor-filtro-permissao/seletor-filtro-permissao/seletor-filtro-permissao.component';
import { BaseCoreComponent } from './base-core/base-core/base-core.component';
import { TranslocoModule } from '@ngneat/transloco';



@NgModule({
  declarations: [PanelComponent, MensagemCoreComponent, SeletorPermissaoComponent, CpfCnpjDirective, SeletorFiltroPermissaoComponent, BaseCoreComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    ScrollPanelModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    PanelModule,
    InputTextModule,
    EditorModule,
    TableModule,
    DialogModule,
    DropdownModule,
    CalendarModule,
    ToastModule,
    ProgressSpinnerModule,
    TranslocoModule
  ],
  exports: [PanelComponent, MensagemCoreComponent, SeletorPermissaoComponent, CpfCnpjDirective, SeletorFiltroPermissaoComponent]
})
export class SharedModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTimes, faSearch, faFileExcel, faCheckCircle, faPlusSquare, faTrashAlt, faEdit, faEye, faCopy);
  }
}
