import { PerguntasFrequentes } from './../../../models/perguntas-frequentes.model';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Input } from '@angular/core';
import { FaqCategory } from 'src/app/shared/models/faq-category.model';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {

  @Output()
  public searchEventEmitter: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  public perguntasFrequentes: FaqCategory[] = [];

  @Input()
  public textSearch: string = '';

  @Input()
  public collapsed: boolean = true;

  constructor() { }

  ngOnInit(): void {
    console.log("componente: ",this.textSearch);
  }

  public search(): void {
    this.searchEventEmitter.emit(this.textSearch);
  }

  public clear(): void {
    this.textSearch = null;
    this.searchEventEmitter.emit(this.textSearch);
  }

}
