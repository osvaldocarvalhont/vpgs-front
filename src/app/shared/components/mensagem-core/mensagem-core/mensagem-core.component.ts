import { MensagemService } from './../../../../interno/vpgs-adm/mensagem-adm/service/mensagem.service';
import { DataTableMensagem } from './../../../models/data-table-mensagem.model';
import { Communication } from './../../../models/communication.model';
import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { LazyLoadEvent } from 'primeng/api';
import { FilterMensagem } from 'src/app/shared/models/filter-mensagem.model';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-mensagem-core',
  templateUrl: './mensagem-core.component.html',
  styleUrls: ['./mensagem-core.component.css']
})
export class MensagemCoreComponent implements OnInit {

  @Output()
  public loadMensagensEventEmitter: EventEmitter<LazyLoadEvent> = new EventEmitter<LazyLoadEvent>();

  @Input()
  public colunas: any[] = [];

  @Input()
  public itens: Communication[] = [];

  @Input()
  public datasource: Communication[] = [];

  @Input()
  public filter: FilterMensagem;

  @Input()
  public selectedMensagem: Communication[] = [];

  @Input()
  public totalRecords: number;

  @Input()
  public visibleBtnAdd: boolean;

  @Input()
  public visibleBtnCopy: boolean;

  @Input()
  public loading: boolean;

  @ViewChild('dt') table: Table;

  public pt: any;

  public dateFilters: any;
  public labelAdicionar: string;
  public labelExportar: string;
  public labelVisualizar: string;
  public labelCopiar: string;
  public labelMensagemNaoEncontrada: string;
  public labeInfoRegistrosTabela: string;

  constructor(private router: Router,
              private translocoService: TranslocoService,
              private mensagemService: MensagemService) { }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.loading = false;
    this.configCalendarPt();
    const dateInput = this;
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('adicionar').subscribe(value => {
      this.labelAdicionar = value;
    });
    this.translocoService.selectTranslate('exportar').subscribe(value => {
      this.labelExportar = value;
    });
    this.translocoService.selectTranslate('visualizar').subscribe(value => {
      this.labelVisualizar = value;
    });
    this.translocoService.selectTranslate('copiar').subscribe(value => {
      this.labelCopiar = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-mensagens-não-encontrada').subscribe(value => {
      this.labelMensagemNaoEncontrada = value;
    });
    this.translocoService.selectTranslate('info-total-registros-tabela').subscribe(value => {
      this.labeInfoRegistrosTabela = value;
    });
  }

  public clearDate(): void {
    this.table.filter('', 'creationDate', 'equals');
  }

  private configCalendarPt(): void {
    this.pt = {
      firstDayOfWeek: 1,
      dayNames: ['domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'],
      monthNamesShort: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };
  }

  public addMensagem(): void {
    this.router.navigate(['/interno/administracao/mensagens/add']);
  }

  public copyMensagem(): void {
    this.router.navigate(['/interno/administracao/mensagens/copy', this.selectedMensagem[0].id]);
  }

  public viewMensagem(): void{
    this.router.navigate(['/interno/administracao/mensagens/view', this.selectedMensagem[0].id]);
  }

  public exportExcel(): void {
    this.loading = true;
    this.getMensagensExcel(this.filter);
  }

  public getMensagensExcel(filter: FilterMensagem): void {
    this.mensagemService.getXlsxAllMensagemExport(this.filter).subscribe(res => {
      this.saveAsExcelFile(res, 'Mensagens.xlsx');
      this.loading = false;
    }, error => {
      console.error(error);
    });
  }

  public saveAsExcelFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        let EXCEL_EXTENSION = '.xlsx';

        let data: Blob = new Blob([buffer], {
            type: 'application/octet-stream'
        });
        FileSaver.saveAs(data, fileName);
    });
  }

  public getItens(): DataTableMensagem[] {
    const listMensagens: DataTableMensagem[] = [];
    this.mensagemService.findAllMensagemExport().subscribe(res => {
      this.itens =  res;
      for (let item of this.itens) {
        listMensagens.push({de: item.from, titulo: item.title, data: new Date(item.creationDate).toLocaleDateString()});
      }
    });
    return listMensagens;
  }

  public loadData(event: LazyLoadEvent): void {
    this.loadMensagensEventEmitter.emit(event);
  }


}
