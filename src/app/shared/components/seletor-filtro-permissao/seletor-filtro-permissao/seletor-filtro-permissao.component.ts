import { BaseCoreComponent } from './../../../base-core/base-core/base-core.component';
import { ComparisonType } from './../../../enums/comparison-type.enum';
import { MainField } from './../../../enums/main-field.enum';
import { FiltroFiltrosPadrao } from './../../../models/filtro-filtros-padrao.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MessageService, SelectItem } from 'primeng/api';
import { KeyValue } from 'src/app/shared/models/key-value.model';
import { CountryService } from 'src/app/interno/vpgs-adm/usuarios/services/country.service';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-seletor-filtro-permissao',
  templateUrl: './seletor-filtro-permissao.component.html',
  styleUrls: ['./seletor-filtro-permissao.component.css']
})
export class SeletorFiltroPermissaoComponent implements OnInit {

  @Input() @Output()
  public filtros: FiltroFiltrosPadrao[] = [];

  @Output()
  public filterEventEmitter: EventEmitter<FiltroFiltrosPadrao[]> = new EventEmitter<FiltroFiltrosPadrao[]>();

  @Input()
  public totalRecords: number;

  @Input()
  public selectedFiltrosSelecionado: FiltroFiltrosPadrao[];

  @Input()
  public filtro: FiltroFiltrosPadrao = new FiltroFiltrosPadrao();

  public filtroDialog: boolean;
  public optionsMainField: SelectItem[];
  public optionsComparisonType: SelectItem[];
  public selectedMainField: SelectItem;
  public selectedComparisonType: SelectItem;
  public selectedAuthMode: SelectItem;
  public selectedCountry: SelectItem;
  public selectedMainValue: any;
  // public filtroSelecionado: FiltroFiltrosPadrao;
  public showInput: boolean = true;
  public showDropCountry: boolean = false;
  public showDropAuthMode: boolean = false;
  public optionsCountry: SelectItem[];
  public optionsAuthMode: SelectItem[];
  public labelAdicionar: string;
  public labelRemover: string;
  public labelCampo: string;
  public labelComparacao: string;
  public labelValor: string;
  public labelCancelar: string;
  public labelSalvar: string;
  public labelFiltroSelecionado: string;
  public labelErro: string;
  public labelAdicionarFiltro: string;
  public labeInfoRegistrosTabela: string;

  constructor(private messageService: MessageService, private countryService: CountryService, private translocoService: TranslocoService) {
  }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.buildOptionsDiolog();
    this.totalRecords = this.filtros.length;
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('adicionar').subscribe(value => {
      this.labelAdicionar = value;
    });
    this.translocoService.selectTranslate('remover').subscribe(value => {
      this.labelRemover = value;
    });
    this.translocoService.selectTranslate('seletor-filtro-campo').subscribe(value => {
      this.labelCampo = value;
    });
    this.translocoService.selectTranslate('seletor-filtro-comparacao').subscribe(value => {
      this.labelComparacao = value;
    });
    this.translocoService.selectTranslate('seletor-filtro-valor').subscribe(value => {
      this.labelValor = value;
    });
    this.translocoService.selectTranslate('cancelar').subscribe(value => {
      this.labelCancelar = value;
    });
    this.translocoService.selectTranslate('salvar').subscribe(value => {
      this.labelSalvar = value;
    });
    this.translocoService.selectTranslate('seletor-filtro-ja-selecionado').subscribe(value => {
      this.labelFiltroSelecionado = value;
    });
    this.translocoService.selectTranslate('erro').subscribe(value => {
      this.labelErro = value;
    });
    this.translocoService.selectTranslate('seletor-filtro-adicionar-filtro').subscribe(value => {
      this.labelAdicionarFiltro = value;
    });
    this.translocoService.selectTranslate('info-total-registros-tabela').subscribe(value => {
      this.labeInfoRegistrosTabela = value;
    });
  }

  private buildOptionsDiolog(): void {
    this.buildOptionsMainField();
    this.buildOptionsComparisonType();
    this.buildOptionsCountries();
    this.buildOptionsAuthMode();
  }

  private buildOptionsComparisonType() {
    this.optionsComparisonType =
      ComparisonType.COMPARISON_TYPE.map((tipo) => {
        return { label: tipo.value, value: tipo.id };
      });
  }

  private buildOptionsMainField() {
    this.optionsMainField =
      MainField.MAIN_FIELD.map((tipo) => {
        return { label: tipo.value, value: tipo.id };
      });
  }

  public buildOptionsAuthMode(): void {
    this.optionsAuthMode = [
      { label: 'Selecione', value: null },
      { label: 'Fornecedor', value: 'INTERNAL' },
      { label: 'Vale', value: 'SAML' }
    ];
  }

  public buildOptionsCountries(): void {
    this.countryService.findAllCodeCountries().subscribe(res => {
      this.optionsCountry = KeyValue.convertToSelectItem(res);
    });
  }

  public getValueMainField(): void {
    this.filtro.mainField = this.selectedMainField.value;
    this.filtro.mainFieldValue = this.selectedMainField.label;
    this.showInput = (this.selectedMainField.value === 'AUTHMODE' || this.selectedMainField.value === 'COUNTRY') ? false : true;
    this.showDropAuthMode = this.selectedMainField.value === 'AUTHMODE' ? true : false;
    this.showDropCountry = this.selectedMainField.value === 'COUNTRY' ? true : false;
  }

  public getValueComparisonType(): void {
    this.filtro.comparisonType = this.selectedComparisonType.value;
    this.filtro.comparisonTypeValue = this.selectedComparisonType.label;
  }

  public getValueAuthMode(): void {
    this.filtro.mainValueValue = this.selectedAuthMode.label;
    this.filtro.mainValue = this.selectedAuthMode.value;
  }

  public getValueMainValue(): void {
    this.filtro.mainValue = this.selectedMainValue;
    this.filtro.mainValueValue = this.selectedMainValue;
  }

  public getValueCountry(): void {
    this.filtro.mainValueValue = this.selectedCountry.label;
    this.filtro.mainValue = this.selectedCountry.value;
  }

  public showFiltroDiolog(): void {
    this.filtroDialog = true;
    this.selectedMainField = null;
    this.selectedComparisonType = null;
    this.filtro.mainValue = null;
  }

  public hideDialog(): void {
    this.filtroDialog = false;
  }

  public removeFiltro(): void{
    for (let filtro of this.selectedFiltrosSelecionado) {
      const index: number = this.filtros.indexOf(filtro);
      if (index !== -1) {
          this.filtros.splice(index, 1);
      }
    }
  }

  public adicionarFiltro(): void {
    if(this.selectedMainValue != null){

      this.filtro.mainValueValue = this.selectedMainValue;
      this.filtro.mainValue = this.selectedMainValue;
    }

    console.log("Filtro: ");
    console.log(this.filtro);

    let count = 0;
    for (let f of this.filtros) {
      if (f.mainField === this.filtro.mainField &&
        f.comparisonType === this.filtro.comparisonType &&
          f.mainValue === this.filtro.mainValue) {
            count ++;
          }
    }

    if (count === 0) {
      this.filtro.isRemoved = true;
      this.filtros.push(Object.assign({}, this.filtro));
      console.log("Filtros: ");
      console.log(this.filtros);
      this.filtro = new FiltroFiltrosPadrao();
      this.hideDialog();
      this.uptadeFilterList();
    } else {
      this.showModal('error', this.labelErro, this.labelFiltroSelecionado);
      this.filtroDialog = false;
    }
    this.selectedMainValue = null;
  }

  public uptadeFilterList(): void {
    this.filterEventEmitter.emit(this.filtros);
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }


}
