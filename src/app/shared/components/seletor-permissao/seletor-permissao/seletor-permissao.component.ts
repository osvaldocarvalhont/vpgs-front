import { SeletorPermissaoService } from './../services/seletor-permissao.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FiltroPermissoes } from 'src/app/shared/models/filtro-permissoes.model';
import { KeyValue } from 'src/app/shared/models/key-value.model';
import { MessageService } from 'primeng/api';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-seletor-permissao',
  templateUrl: './seletor-permissao.component.html',
  styleUrls: ['./seletor-permissao.component.css'],
  providers: [SeletorPermissaoService]
})
export class SeletorPermissaoComponent implements OnInit {

  @Input() @Output()
  public permissoes: FiltroPermissoes[] = [];

  @Output()
  public permissoesEventEmitter: EventEmitter<FiltroPermissoes[]> = new EventEmitter<FiltroPermissoes[]>();

  @Input()
  public selectedPermissoesSelecionada: FiltroPermissoes[];

  @Input()
  public permissao: FiltroPermissoes = new FiltroPermissoes();

  @Input()
  public totalRecords: number;

  public permissaoDialog: boolean;
  public optionsAplicacao: KeyValue[];
  public optionsGrupo: KeyValue[];
  public optionsPermissao: KeyValue[];
  public selectedAplicacao: KeyValue;
  public selectedGrupo: KeyValue;
  public selectedPermissao: KeyValue;
  public labelAdicionar: string;
  public labelRemover: string;
  public labelAplicacao: string;
  public labelGrupo: string;
  public labelPermissao: string;
  public labelCancelar: string;
  public labelSalvar: string;
  public labelErro: string;
  public labelPermissaoSelecionado: string;
  public labelAdicionarPermissao: string;
  public labeInfoRegistrosTabela: string;


  constructor(private seletorPermissaoService: SeletorPermissaoService,
              private translocoService: TranslocoService,
              private messageService: MessageService) { }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.buildOptionsDropAplicacao();
    this.totalRecords = this.permissoes.length;
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('adicionar').subscribe(value => {
      this.labelAdicionar = value;
    });
    this.translocoService.selectTranslate('remover').subscribe(value => {
      this.labelRemover = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-permissao-aplicacao').subscribe(value => {
      this.labelAplicacao = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-permissao-grupo').subscribe(value => {
      this.labelGrupo = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-permissao-permissao').subscribe(value => {
      this.labelPermissao = value;
    });
    this.translocoService.selectTranslate('cancelar').subscribe(value => {
      this.labelCancelar = value;
    });
    this.translocoService.selectTranslate('salvar').subscribe(value => {
      this.labelSalvar = value;
    });
    this.translocoService.selectTranslate('seletor-permissao-ja-selecionado').subscribe(value => {
      this.labelPermissaoSelecionado = value;
    });
    this.translocoService.selectTranslate('erro').subscribe(value => {
      this.labelErro = value;
    });
    this.translocoService.selectTranslate('seletor-permissao-adicionar').subscribe(value => {
      this.labelAdicionarPermissao = value;
    });
    this.translocoService.selectTranslate('info-total-registros-tabela').subscribe(value => {
      this.labeInfoRegistrosTabela = value;
    });
  }

  private buildOptionsDropAplicacao(): void {
    this.seletorPermissaoService.findAllAplications().subscribe(res => {
      this.optionsAplicacao = res;
    });
  }

  public getValueAplicacao(event: any): void {
    this.permissao.aplicacao = this.selectedAplicacao.value;
    this.permissao.aplicacaoKey = this.selectedAplicacao.id;
    this.permissao.aplicacaoP = this.selectedAplicacao;
    this.buildOptionsDropGrupo(this.selectedAplicacao.id);
  }

  public getValueGrupo(): void {
    this.permissao.grupo = this.selectedGrupo.value;
    this.permissao.grupoKey = this.selectedGrupo.id;
    this.permissao.grupoP = this.selectedGrupo;
    this.buildOptionsDropPermissao(this.selectedGrupo.id);
  }

  public getValuePermissao(): void {
    this.permissao.permissao = this.selectedPermissao.value;
    this.permissao.permissaoKey = this.selectedPermissao.id;
    this.permissao.permissaoP = this.selectedPermissao;

  }

  public buildOptionsDropGrupo(aplicacao: any): void {
    this.seletorPermissaoService.findAllGroups(aplicacao).subscribe(res => {
      this.optionsGrupo = res;
    });
  }

  public buildOptionsDropPermissao(grupo: any): void {
    this.seletorPermissaoService.findAllPermission(grupo).subscribe(res => {
      this.optionsPermissao = res;
    });
  }

  public showPermissaoDiolog(): void{
    this.permissaoDialog = true;
  }

  public adicionarPermissao(): void {
    let count = 0;
    for (let p of this.permissoes) {
      if (p.permissaoKey === this.permissao.permissaoKey &&
          p.grupoKey === this.permissao.grupoKey &&
          p.aplicacaoKey === this.permissao.aplicacaoKey) {
            count ++;
          }
    }
    // tslint:disable-next-line: no-conditional-assignment
    if (count === 0) {
      this.permissao.isRemoved = true;
      this.permissoes.push(Object.assign({}, this.permissao));
      this.permissao = new FiltroPermissoes();
      this.hideDialogPermissao();
      this.uptadePermissionList();
    } else {
      this.showModal('error', this.labelErro, this.labelPermissaoSelecionado);
      this.permissaoDialog = false;
    }
  }

  public removePermissao(): void{
    for (const permissao of this.selectedPermissoesSelecionada) {
      permissao.isRemoved = true;
      const index: number = this.permissoes.indexOf(permissao);
      if (index !== -1) {
          this.permissoes.splice(index, 1);
      }
    }
    this.uptadePermissionList();
  }

  public hideDialogPermissao(): void {
    this.permissaoDialog = false;
  }

  public uptadePermissionList(): void {
    this.permissoesEventEmitter.emit(this.permissoes);
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

}
