import { environment } from './../../../../../environments/environment';
import { KeyValue } from './../../../models/key-value.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SeletorPermissaoService {

  private readonly ENDPOINT_SERVICE_PROVIDER = 'service-providers';
  private readonly ENDPOINT_GROUP = 'groups';
  private readonly ENDPOINT_PERMISSION = 'permissions';

  constructor(private http: HttpClient) { }

  public findAllAplications(): Observable<KeyValue[]> {
    return this.http.get<KeyValue[]>(`${environment.BASE_URL.concat(this.ENDPOINT_SERVICE_PROVIDER)}`);
  }

  public findAllGroups(id: any): Observable<KeyValue[]> {
    return this.http.get<KeyValue[]>(`${environment.BASE_URL.concat(this.ENDPOINT_GROUP)}/${id}`);
  }

  public findAllPermission(id: any): Observable<KeyValue[]> {
    return this.http.get<KeyValue[]>(`${environment.BASE_URL.concat(this.ENDPOINT_PERMISSION)}/${id}`);
  }

}
