import { InternoComponent } from './interno.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: InternoComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./vpgs-app/vpgs-app.module').then(m => m.VpgsAppModule)
      },
      {
        path: 'administracao',
        loadChildren: () => import('./vpgs-adm/vpgs-adm.module').then(m => m.VpgsAdmModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InternoRoutingModule { }
