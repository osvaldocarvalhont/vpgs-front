import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faFileExcel, faCheckCircle, faPlusSquare, faTrashAlt, faEdit, faFilter, faTimes, faUserCheck, faUserAltSlash} from '@fortawesome/free-solid-svg-icons';
import { PanelModule } from 'primeng/panel';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';

import { UsuariosListRoutingModule } from './usuarios-list-routing.module';
import { UsuariosListComponent } from './usuarios-list.component';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputTextModule } from 'primeng/inputtext';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { TranslocoModule } from '@ngneat/transloco';


@NgModule({
  declarations: [UsuariosListComponent],
  imports: [
    CommonModule,
    UsuariosListRoutingModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    FontAwesomeModule,
    ConfirmDialogModule,
    ToastModule,
    PanelModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    ProgressSpinnerModule,
    TranslocoModule
  ]
})
export class UsuariosListModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faFileExcel, faCheckCircle, faPlusSquare, faTrashAlt, faEdit, faFilter, faTimes, faUserCheck, faUserAltSlash);
  }
}
