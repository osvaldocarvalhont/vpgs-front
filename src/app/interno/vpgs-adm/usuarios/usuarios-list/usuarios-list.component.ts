import { AuthMode } from './../../../../shared/enums/auth-mode.enum';
import { UsuarioService } from './../services/usuario.service';
import { UserStatus, UserStatusPt } from './../../../../shared/enums/user-status.enum';
import { FilterUsuario } from './../../../../shared/models/filter-usuario.model';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService, SelectItem } from 'primeng/api';
import { Router } from '@angular/router';
import { LazyLoadEvent } from 'primeng/api';
import { User } from 'src/app/shared/models/user.model';
import { Location } from '@angular/common';
import { TranslocoService } from '@ngneat/transloco';

interface UserStatusModel {
  label: string;
  value: string;
}

@Component({
  selector: 'app-usuarios-list',
  templateUrl: './usuarios-list.component.html',
  styleUrls: ['./usuarios-list.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class UsuariosListComponent implements OnInit {

  public colunas: any[];
  public itens: User[];
  public selectedUsuario: User[];

  public filter: FilterUsuario = new FilterUsuario();
  public optionsUserStatus: UserStatusModel[];
  public loading: boolean;
  public totalRecords: number;
  public userList: number[] = [];
  public users: User[] = [];
  public valueStatusFilter: UserStatusModel;
  public firstName = 'firstName';

  public labelFiltro: string;
  public labelFiltrar: string;
  public labelLimpar: string;
  public labelAdicionar: string;
  public labelRemover: string;
  public labelEditar: string;
  public labelAtivar: string;
  public labelInativar: string;
  public labelExportar: string;
  public labelEmail: string;
  public labelCpfCnpj: string;
  public labelMatricula: string;
  public labelNome: string;
  public labelPais: string;
  public labelTipoAcesso: string;
  public labelSituacao: string;
  public labeInfoRegistrosTabela: string;


  constructor(private confirmationService: ConfirmationService,
              private messageService: MessageService,
              private translocoService: TranslocoService,
              private router: Router,
              private location: Location,
              private cdref: ChangeDetectorRef,
              private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.buildColumns();
    this.buildOptionsUserStatus();
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('filtro').subscribe(value => {
      this.labelFiltro = value;
    });
    this.translocoService.selectTranslate('filtrar').subscribe(value => {
      this.labelFiltrar = value;
    });
    this.translocoService.selectTranslate('limpar').subscribe(value => {
      this.labelLimpar = value;
    });
    this.translocoService.selectTranslate('editar').subscribe(value => {
      this.labelEditar = value;
    });
    this.translocoService.selectTranslate('ativar').subscribe(value => {
      this.labelAtivar = value;
    });
    this.translocoService.selectTranslate('inativar').subscribe(value => {
      this.labelInativar = value;
    });
    this.translocoService.selectTranslate('remover').subscribe(value => {
      this.labelRemover = value;
    });
    this.translocoService.selectTranslate('exportar').subscribe(value => {
      this.labelExportar = value;
    });
    this.translocoService.selectTranslate('email').subscribe(value => {
      this.labelEmail = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-usuario-cpf').subscribe(value => {
      this.labelCpfCnpj = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-usuario-matricula').subscribe(value => {
      this.labelMatricula = value;
    });
    this.translocoService.selectTranslate('nome').subscribe(value => {
      this.labelNome = value;
    });
    this.translocoService.selectTranslate('adicionar').subscribe(value => {
      this.labelAdicionar = value;
    });
    this.translocoService.selectTranslate('pais').subscribe(value => {
      this.labelPais = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-usuario-tipoAcesso').subscribe(value => {
      this.labelTipoAcesso = value;
    });
    this.translocoService.selectTranslate('vpgs-app-acesso-solicitacoes-situacao').subscribe(value => {
      this.labelSituacao = value;
    });
    this.translocoService.selectTranslate('info-total-registros-tabela').subscribe(value => {
      this.labeInfoRegistrosTabela = value;
    });
  }

  private buildOptionsUserStatus(): void {
    this.optionsUserStatus = [
      { label: 'ATIVO', value: UserStatus.ACTIVE },
      { label: 'AGUARDANDO CONFIRMAÇÃO', value: UserStatus.WAITING_CONFIRMATION },
      { label: 'INATIVO', value: UserStatus.INACTIVE },
      { label: 'REMOVIDO', value: UserStatus.REMOVED }
    ];
  }

  private buildColumns(): void {
    this.colunas = [
      { field: 'email', header: this.labelEmail },
      { field: 'taxCode', header: this.labelCpfCnpj},
      { field: 'companyCode', header: this.labelMatricula},
      { field: 'fullName', header: this.labelNome },
      { field: 'country', header: this.labelPais },
      { field: 'authMode', header: this.labelTipoAcesso },
      { field: 'status', header: this.labelSituacao }

    ];
  }

  public addUser(): void{
    this.router.navigate(['/interno/administracao/usuarios/usuariosForm']);
  }

  public editUser(): void{
    this.router.navigate(['/interno/administracao/usuarios/editUser', this.selectedUsuario[0].id]);
  }

  public activeUser(): void {
    this.buildUserListSelected();
    alert(this.userList)
    this.usuarioService.active(this.userList).subscribe(data => {
      //this.load();
      alert('SUCESSO\n\nUsuario(s) alterado(s)!');
      //this.router.navigate(['/interno/administracao/usuarios']);
      this.filtrar();
    }, error => {
      this.showModal("error", "Erro" , "Erro ao tentar ativar");
    });
  }

  public inactiveUser(): void {
    this.buildUserListSelected();
    this.usuarioService.inactive(this.userList).subscribe(data => {
      //this.load();
      alert('SUCESSO\n\nUsuario(s) não alterado(s)!');
      //this.router.navigate(['/interno/administracao/usuarios']);
      this.filtrar();
    }, error => {
      this.showModal("error", "Erro" , "Erro ao tentar ativar");
    });
  }

  public removeUser(): void{
    this.buildUserListSelected();
    this.showConfirmationDiolog();
  }

  private buildUserListSelected(): void {
    if (this.selectedUsuario != null) {
      for (const usuario of this.selectedUsuario) {
        this.userList.push(usuario.id);
      }
    }
  }

  private showConfirmationDiolog(): void {
    const msgConfirmation = (this.selectedUsuario.length === 1) ? 'Foi selecionado 1 Usuário. Deseja removê-lo? ' :
    ' Foram selecionados ' + this.selectedUsuario.length + ' Usuários. Deseja removê-los?';
    const msgModal = (this.selectedUsuario.length === 1) ? 'Usuário removido com sucesso!' : ' Usuários removidos com sucesso!';

    this.confirmationService.confirm({
      message: msgConfirmation,
      acceptLabel: 'Sim',
      rejectLabel: 'Não',
      accept: () => {
        this.usuarioService.remove(this.userList).subscribe(data =>
          {
            this.showModal('success', 'Sucesso', msgModal);
            //this.load();
            //this.router.navigate(['/interno/administracao/usuarios']);
            this.filtrar();

          },
        );
      }

    });
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

  public exportExcel(): void {
    this.loading = true;
    this.setFilter();
    this.getUsuariosExcel(this.filter);
  }

  public getUsuariosExcel(filter: FilterUsuario): void {
    this.usuarioService.getXlsxAllUsuariosFilterPage(this.filter).subscribe(res => {
      this.saveAsExcelFile(res, 'Usuários.xlsx');
      this.loading = false;
    }, error => {
      console.error(error);
    });
  }

  public saveAsExcelFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        let EXCEL_EXTENSION = '.xlsx';

        let data: Blob = new Blob([buffer], {
            type: 'application/octet-stream'
        });
        FileSaver.saveAs(data, fileName);
    });
  }

  public filtrar(): void {
    this.loading = true;
    this.setFilter();
    this.getAllUsuario(this.filter);
  }

  private setFilter() {
    this.filter.email = this.filter.email != null ? this.filter.email.toUpperCase() : null;
    this.filter.matricula = this.filter.matricula != null ? this.filter.matricula.toUpperCase() : null;
    this.filter.nome = this.filter.nome != null ? this.filter.nome.toUpperCase() : null;
    this.filter.situacao = this.valueStatusFilter === undefined ? UserStatus.ACTIVE : this.valueStatusFilter.value;
  }

  public clear(): void {
    this.loading = true;
    this.filter.email = null;
    this.filter.cpfCnpj = null;
    this.filter.matricula = null;
    this.filter.nome = null;
    this.buildOptionsUserStatus();
    this.getAllUsuario(this.filter);
  }

  public loadData(event: LazyLoadEvent): void {
    this.loading = true;
    // tslint:disable-next-line: no-string-literal
    this.filter.page = (event['first'] / 10).toString();
    this.filter.orderBy = event['sortField'] === 'fullName' ? this.firstName : event['sortField'];
    this.filter.direction = event['sortOrder'] === 1 ? 'ASC' : 'DESC';
    this.getAllUsuario(this.filter);
  }

  public getAllUsuario(filter: FilterUsuario): void {
    this.usuarioService.findAllUsuarioFilterPage(this.filter).subscribe(res => {
      // tslint:disable-next-line: no-string-literal
      this.users =  res['content'];
      // tslint:disable-next-line: forin
      for ( let index in res['content']) {
        this.users[index].authMode = this.setAuthMode(res['content'][index].authMode, this.users[index]);
        this.users[index].status = this.setStatus(res['content'][index].status, this.users[index]);
      }
      this.itens = this.users;

      // tslint:disable-next-line: no-string-literal
      this.totalRecords = res['totalElements'];
      this.loading = false;
    }, error => {
      alert(error)
    });
  }

  public setAuthMode(authMode: string, user: User): string {
    switch (authMode) {
      case 'INTERNAL':
        user.authMode = AuthMode.INTERNAL;
        break;
      case 'SAML':
        user.authMode = AuthMode.SAML;
        break;
    }
    return user.authMode;
  }

  public setStatus(status: string, user: User): string {
    switch (status) {
      case 'ACTIVE':
        user.status = UserStatusPt.get(UserStatus.ACTIVE);
        break;
      case 'WAITING_CONFIRMATION':
        user.status = UserStatusPt.get(UserStatus.WAITING_CONFIRMATION);
        break;
      case 'REMOVED':
        user.status = UserStatusPt.get(UserStatus.REMOVED);
        break;
      case 'INACTIVE':
        user.status = UserStatusPt.get(UserStatus.INACTIVE);
        break;
    }
    return user.status;
  }

  public load(): void {
    //location.reload();
  }


}
