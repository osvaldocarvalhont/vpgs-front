import { UsuariosComponent } from './usuarios.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: UsuariosComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./usuarios-list/usuarios-list.module').then(m => m.UsuariosListModule)
      },
      {
        path: 'usuariosList',
        loadChildren: () => import('./usuarios-list/usuarios-list.module').then(m => m.UsuariosListModule)
      },
      {
        path: 'usuariosForm',
        loadChildren: () => import('./usuarios-form/usuarios-form.module').then(m => m.UsuariosFormModule)
      },
      {
        path: 'editUser/:id',
        loadChildren: () => import('./usuarios-form/usuarios-form.module').then(m => m.UsuariosFormModule)
      },
      {
        path: 'editRequest/:id/:idRequest',
        loadChildren: () => import('./usuarios-form/usuarios-form.module').then(m => m.UsuariosFormModule)
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
