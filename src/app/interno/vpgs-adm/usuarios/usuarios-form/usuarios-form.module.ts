import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';

import { UsuariosFormRoutingModule } from './usuarios-form-routing.module';
import { UsuariosFormComponent } from './usuarios-form.component';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faPlus, faTrashAlt, faTimes, faCheck, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TranslocoModule } from '@ngneat/transloco';

@NgModule({
  declarations: [UsuariosFormComponent],
  imports: [
    CommonModule,
    UsuariosFormRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    DropdownModule,
    TableModule,
    DialogModule,
    FontAwesomeModule,
    ToastModule,
    SharedModule,
    InputTextareaModule,
    TranslocoModule
  ]
})
export class UsuariosFormModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faPlus, faTrashAlt, faTimes, faCheck, faChevronLeft);
  }
}
