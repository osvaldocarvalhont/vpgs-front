import { AuthTokenService } from './../../../../shared/authToken/auth-token.service';
import { Observable } from 'rxjs';
import { Request } from './../../../../shared/models/request.model';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { KeyValue } from './../../../../shared/models/key-value.model';
import { CountryService } from '../services/country.service';
import { User } from './../../../../shared/models/user.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageService, SelectItem } from 'primeng/api';
import { FiltroPermissoes } from 'src/app/shared/models/filtro-permissoes.model';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import { UserPermission } from 'src/app/shared/models/user-permission.model';
import { SolicitacaoService } from '../../solicitacoes/services/solicitacao.service';
import { Location } from '@angular/common';
import { take } from 'rxjs/operators';
import { CpfCnpjValidator } from 'src/app/shared/directives/cpf-cnpj-validator/cpf-cnpj.validator';
import { TranslocoService } from '@ngneat/transloco';
import { AuthMode } from 'src/app/shared/enums/auth-mode.enum';

@Component({
  selector: 'app-usuarios-form',
  templateUrl: './usuarios-form.component.html',
  styleUrls: ['./usuarios-form.component.css'],
  providers: [MessageService]
})
export class UsuariosFormComponent implements OnInit {

  public loading: boolean;
  public edit = false;
  public editRequest = false;
  public user: User = new User();
  public optionsPais: SelectItem[];
  public optionsTipoAcesso: SelectItem[];
  public showDropCpf = false;
  public id: number;
  public idRequest: number;
  public permissionList: FiltroPermissoes[] = [];
  public requestAccept: Request = new Request();
  public totalRecords: number;
  public labelVoltar: string;
  public labelSalvar: string;
  public isAdm: boolean;

  @ViewChild('form') form: NgForm;

  public userForm: FormGroup;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private usuarioService: UsuarioService,
              private countryService: CountryService,
              private solicitacaoService: SolicitacaoService,
              private translocoService: TranslocoService,
              private location: Location,
              private formBuilder: FormBuilder,
              private messageService: MessageService,
              private tokenAuth : AuthTokenService) { }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.buildForm();
    this.id = this.route.snapshot.params.id;
    this.idRequest = this.route.snapshot.params.idRequest;
    if (this.id) {
      this.edit = true;
      this.getUsuarioById(this.id);
    }
    if (this.idRequest) {
      this.editRequest = true;
      this.getRequestById(this.idRequest);
    }
    this.buildOptions();
  }

  public getLabelTanslate(): void {
    this.translocoService.selectTranslate('voltar').subscribe(value => {
      this.labelVoltar = value;
    });
    this.translocoService.selectTranslate('salvar').subscribe(value => {
      this.labelSalvar = value;
    });
  }

  private buildForm(): void {
    this.userForm = this.formBuilder.group({
      taxCode: ['', [Validators.required, CpfCnpjValidator]],
    });
  }

  private buildOptions(): void {
    this.getCountries();
    this.getAuthMode();

    var token = this.tokenAuth.decodePayloadJWT();

    //console.info(this.tokenAuth.decodePayloadJWT());

    //var json: string   = localStorage.getItem('currentUser');

    //var user: User  = JSON.parse(json);
    this.isAdm      = token.isAdm;
  }

  public getAuthMode(): void {
    this.optionsTipoAcesso = [
      { label: 'Selecione', value: null },
      { label: 'Fornecedor', value: 'INTERNAL' },
      { label: 'Vale', value: 'SAML' }
    ];
  }

  public getCountries(): void {
    this.countryService.findAllCountries().subscribe(res => {
      this.optionsPais = KeyValue.convertToSelectItem(res);
    });
  }

  public back(): void {
    this.router.navigate(['interno/administracao/usuarios']);
  }

  public save(): void {
    if(this.userForm.controls.taxCode.value) {
      this.user.taxCode = this.userForm.controls.taxCode.value;
    }
    if(!this.user.countryId) {
      this.user.countryId = 2;
    }

    if ((this.user.firstName === null || this.user.firstName === undefined) &&
          (this.user.lastName === null || this.user.lastName === undefined) &&
          (this.user.countryId === null || this.user.countryId === 1 || this.user.countryId === undefined) &&
          (this.user.authMode === null || this.user.authMode === undefined) ||
          (this.user.countryId === 2 && this.user.taxCode === undefined)) {
        this.showModal('error', 'Erro', 'Campos obrigatórios não informados!');
      } else if (this.loading !== true) {
        this.getUserPermission();
        // console.log(this.user);
        this.loading = true;
        if (!this.edit) {
          this.usuarioService.addUser(this.user).subscribe(
            data => {
              this.showModal('success', 'Sucesso', 'Usuário salvo com sucesso!');
              //this.load();
              this.router.navigate(['/interno/administracao/usuarios'])
              this.loading = false;
            },
            error => {
              const msg = error.error.message === null ? 'Erro ao salvar o usuário!' : error.error.message;
              this.showModal('error', 'Erro', msg);
              this.loading = false;
            });
        } else if (this.edit || this.editRequest) {
          this.usuarioService.update(this.user).pipe(take(1)).subscribe(
            data => {
              this.showModal('success', 'Sucesso', 'Usuário atualizado com sucesso!');
              //this.load();
              this.router.navigate(['/interno/administracao/usuarios'])
              this.loading = false;
            },
            error => {
              this.showModal('error', 'Erro', 'Erro ao atualizar o usuário!');
              this.loading = false;
            });
        }
      } else {
        this.messageService.add({severity: 'error', summary: 'Aguarde...', detail: 'Ainda processando...'});
      }
  }

  private getUserPermission(): void {
    if (this.permissionList.length > 0) {
      this.user.userPermission = [];
      for (const item of this.permissionList) {
        const permissao: UserPermission = new UserPermission();
        permissao.id = item.id ? item.id : null;
        permissao.serviceProviderId = item.aplicacaoKey;
        permissao.serviceProvider = item.aplicacao;
        permissao.groupId = item.grupoKey;
        permissao.group = item.grupo;
        permissao.permissionId = item.permissaoKey;
        permissao.permission = item.permissao;
        permissao.isRemoved = item.isRemoved;

        // permissao.id = item.id ? item.id : null;
        // permissao.serviceProviderId = item.aplicacaoP.id;
        // permissao.serviceProvider = item.aplicacaoP.value;
        // permissao.groupId = item.grupoP.id;
        // permissao.group = item.grupoP.value;
        // permissao.permissionId = item.permissaoP.id;
        // permissao.permission = item.permissaoP.value;
        // permissao.isRemoved = false;

        this.user.userPermission.push(Object.assign({}, permissao));
      }
    }
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    alert(summaryValue + '\n' + detailValue);
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

  public getUsuarioById(id: number): void {
    this.usuarioService.findUsuarioById(id).subscribe(res => {
      this.user = res;
      this.populateTaxCode();
      let listPerm: FiltroPermissoes = new FiltroPermissoes();
      for (let p of this.user.userPermission) {
        listPerm.id = p.id;
        listPerm.aplicacaoKey = p.serviceProviderId;
        listPerm.aplicacao = p.serviceProvider;
        listPerm.grupoKey = p.groupId;
        listPerm.grupo = p.group;
        listPerm.permissaoKey = p.permissionId;
        listPerm.permissao = p.permission;
        listPerm.isRemoved = p.isRemoved ? p.isRemoved : false;
        this.permissionList.push(Object.assign({}, listPerm));
      }
      this.totalRecords = this.permissionList.length;
    }
    );
    console.log(this.user);
  }

  private populateTaxCode() {
    this.userForm.patchValue({
      taxCode: this.user.taxCode
    });
  }

  public getRequestById(id: number): void {
    this.solicitacaoService.findRequestById(id).subscribe(data =>
      {
        this.requestAccept.descriptiOn = data.descriptiOn;
      });
  }

  public loadPermission(valueEmited: FiltroPermissoes[]): void {
    this.permissionList = valueEmited;
  }

  public getValuePais(changedValue): void {
    this.showDropCpf = changedValue === 2 ? true : false;
  }

  public load(): void {
    //location.reload();
  }

}
