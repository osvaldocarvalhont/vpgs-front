import { environment } from './../../../../../environments/environment';
import { KeyValue } from './../../../../shared/models/key-value.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private readonly ENDPOINT_COUNTRY = 'countries';
  private readonly ENDPOINT_COUNTRY_CODE = '/code';

  constructor(private http: HttpClient) { }

  public findAllCountries(): Observable<KeyValue[]> {
    //console.log(`${environment.BASE_URL.concat(this.ENDPOINT_COUNTRY)}`);
    //alert(`${environment.BASE_URL.concat(this.ENDPOINT_COUNTRY)}`);
    return this.http.get<KeyValue[]>(`${environment.BASE_URL.concat(this.ENDPOINT_COUNTRY)}`);
  }

  public findAllCodeCountries(): Observable<KeyValue[]> {
    //console.log(`${environment.BASE_URL.concat(this.ENDPOINT_COUNTRY)}`);
    //alert(`${environment.BASE_URL.concat(this.ENDPOINT_COUNTRY)}`);
    return this.http.get<KeyValue[]>(`${environment.BASE_URL.concat(this.ENDPOINT_COUNTRY).concat(this.ENDPOINT_COUNTRY_CODE)}`);
  }

}
