import { FilterUsuario } from './../../../../shared/models/filter-usuario.model';
import { KeyValue } from './../../../../shared/models/key-value.model';
import { environment } from './../../../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../../../shared/models/user.model';
import { TrocarSenha } from 'src/app/shared/models/trocar-senha.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private readonly ENDPOINT_USER = 'users';
  private readonly ENDPOINT_EMAIL_USUARIOS = '/email-user';
  private readonly ENDPOINT_USER_FILTER_PAGINADA = '/filter-search';
  private readonly ENDPOINT_USER_EXPORT = '/export';
  private readonly ENDPOINT_USER_REMOVE = '/remove';
  private readonly ENDPOINT_USER_ACTIVE = '/active';
  private readonly ENDPOINT_USER_INACTIVE = '/inactive';
  private readonly ENDPOINT_USER_UPDATE = '/update-name';
  private readonly ENDPOINT_CONFIRMAR_TROCA_SENHA = 'senha/trocar';

  constructor(private http: HttpClient) { }

  // public findAllUsuarios(): Observable<User[]> {
  //   return this.http.get<User[]>(`${environment.BASE_URL.concat(this.ENDPOINT_USER)}`);
  // }

  public findAllEmailUsuarios(): Observable<KeyValue[]> {
    return this.http.get<KeyValue[]>(`${environment.BASE_URL.concat(this.ENDPOINT_USER).concat(this.ENDPOINT_EMAIL_USUARIOS)}`);
  }

  public findAllEmailUsuariosByPermission(aplicacao: any, grupo: any, permissao: any): Observable<KeyValue[]> {
    return this.http.get<KeyValue[]>(`${environment.BASE_URL.concat(this.ENDPOINT_USER)}/${aplicacao}/${grupo}/${permissao}`);
  }

  public findAllUsuarioFilterPage(filter: FilterUsuario): Observable<User[]> {
    return this.http.get<User[]>(`${environment.BASE_URL.concat(this.ENDPOINT_USER)
      .concat(this.ENDPOINT_USER_FILTER_PAGINADA)}`, { params: this.buildParam(filter) });
  }

  public getXlsxAllUsuariosFilterPage(filter: FilterUsuario): any {
    return this.http.get<any>(`${environment.BASE_URL.concat(this.ENDPOINT_USER)
      .concat(this.ENDPOINT_USER_EXPORT)}`, this.buildOptions(filter));
  }

  public findUsuarioById(id: number): Observable<User> {
    return this.http.get<User>(`${environment.BASE_URL.concat(this.ENDPOINT_USER)}/${id}`);
  }

  public addUser(user: User): Observable<User> {
    return this.http.post<User>(`${environment.BASE_URL.concat(this.ENDPOINT_USER)}`, user);
  }

  public update(user: User): Observable<User> {
    return this.http.put<User>(`${environment.BASE_URL.concat(this.ENDPOINT_USER)}`, user);
  }

  public updateName(user: User): Observable<User> {
    return this.http.put<User>(`${environment.BASE_URL.concat(this.ENDPOINT_USER).concat(this.ENDPOINT_USER_UPDATE)}`, user).pipe(map(user => {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem('currentUser', JSON.stringify(user));
      //this.currentUserSubject.next(user);
      console.log(user)
      return user;
  }));
  }

  public remove(list: number[]): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_USER).concat(this.ENDPOINT_USER_REMOVE)}`, list);
  }

  public active(list: number[]): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_USER).concat(this.ENDPOINT_USER_ACTIVE)}`, list);
  }

  public inactive(list: number[]): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_USER).concat(this.ENDPOINT_USER_INACTIVE)}`, list);
  }

  confirmarNovaSenha(dto: TrocarSenha) {
    return this.http.post<User>(`${environment.BASE_URL.concat(this.ENDPOINT_CONFIRMAR_TROCA_SENHA)}`, dto)
    .pipe(map(user => {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem('currentUser', JSON.stringify(user));
      //this.currentUserSubject.next(user);
      //console.log(user)
      return user;
  }));
}

  public buildParam(filter: FilterUsuario): HttpParams {
    let parameters = new HttpParams();
    // tslint:disable-next-line: forin
    for (const key in filter) {
      const value = filter[key];
      if (value !== null && value !== undefined) {
        parameters = parameters.append(key, value.toString());
      }
    }
    return parameters;
  }

  public buildOptions(filter: FilterUsuario): any {
    let headers = new HttpHeaders({'content-type' : 'application/json',
    'Access-Control-Allow-Origin' : '*'});

    var options = {
      params: this.buildParam(filter),
      headers: headers,
      responseType: 'blob'};

    return options;
  }

}
