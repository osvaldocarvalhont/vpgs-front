import { MensagemFormComponent } from './mensagem-form/mensagem-form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MensagemAdmComponent } from './mensagem-adm.component';

const routes: Routes = [
  {
    path: '',
    component: MensagemAdmComponent
  },
  {
    path: 'add',
    component: MensagemFormComponent
  },
  {
    path: 'copy/:id',
    component: MensagemFormComponent,
    data: {copy: true}
  },
  {
    path: 'view/:id',
    component: MensagemFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MensagemAdmRoutingModule { }
