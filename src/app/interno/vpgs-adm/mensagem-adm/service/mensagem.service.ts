import { FilterMensagemUser } from './../../../../shared/models/filter-mensagem-user.model';
import { CommunicationUser } from './../../../../shared/models/communication-user.model';
import { FilterMensagem } from './../../../../shared/models/filter-mensagem.model';
import { environment } from './../../../../../environments/environment';
import { Communication } from './../../../../shared/models/communication.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MensagemService {

  private readonly ENDPOINT_MENSAGEM = 'communications';
  private readonly ENDPOINT_MENSAGEM_EXPORT = '/export';
  private readonly ENDPOINT_MENSAGEM_FILTER_PAGINADA = '/filter-search';
  private readonly ENDPOINT_MENSAGEM_USUARIO_FILTER_PAGINADA = '/filter-user';

  constructor(private http: HttpClient) { }

  public findAllMensagem(): Observable<Communication[]> {
    return this.http.get<Communication[]>(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM)}`);
  }

  public findAllMensagemExport(): Observable<Communication[]> {
    return this.http.get<Communication[]>(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM).concat(this.ENDPOINT_MENSAGEM_EXPORT)}`);
  }

  public getXlsxAllMensagemExport(filter: FilterMensagem): any {
    return this.http.get<any>(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM)
      .concat(this.ENDPOINT_MENSAGEM_EXPORT)}`, this.buildOptions(filter));
  }

  public findAllMensagemFilterPage(filter: FilterMensagem): Observable<Communication[]> {
    return this.http.get<Communication[]>(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM).concat(this.ENDPOINT_MENSAGEM_FILTER_PAGINADA)}`, { params: this.buildParam(filter) });
  }

  public findMensagemById(id: number): Observable<Communication> {
    return this.http.get<Communication>(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM)}/${id}`);
  }

  public addMensagem(communication: Communication): Observable<Communication> {
    return this.http.post<Communication>(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM)}`, communication);
  }

  public findAllCommunicationByUserFilter(filter: FilterMensagemUser): Observable<CommunicationUser[]> {
    return this.http.get<CommunicationUser[]>(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM).concat(this.ENDPOINT_MENSAGEM_USUARIO_FILTER_PAGINADA)}`, { params: this.buildParam(filter) });
  }

  public buildParam(filter: FilterMensagem): HttpParams {
    let parameters = new HttpParams();
    // tslint:disable-next-line: forin
    for (const key in filter) {
      const value = filter[key];
      if (value !== null && value !== undefined) {
        parameters = parameters.append(key, value.toString());
      }
    }
    return parameters;
  }

  public buildOptions(filter: FilterMensagem): any {
    let headers = new HttpHeaders({'content-type' : 'application/json',
    'Access-Control-Allow-Origin' : '*'});

    var options = {
      params: this.buildParam(filter),
      headers: headers,
      responseType: 'blob'};

    return options;
  }

}
