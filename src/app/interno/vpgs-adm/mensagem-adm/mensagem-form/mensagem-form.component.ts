import { KeyValue } from './../../../../shared/models/key-value.model';
import { UsuarioService } from './../../usuarios/services/usuario.service';
import { Communication } from './../../../../shared/models/communication.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { MensagemService } from '../service/mensagem.service';
import { FiltroPermissoes } from 'src/app/shared/models/filtro-permissoes.model';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-mensagem-form',
  templateUrl: './mensagem-form.component.html',
  styleUrls: ['./mensagem-form.component.css'],
  providers: [MessageService, UsuarioService]
})
export class MensagemFormComponent implements OnInit {

  public filterDialog = false;
  public selectedUsers: SelectItem[] = [];
  public emailUserList: SelectItem[];
  public edit = false;
  public copy = false;

  public colunas: any[];

  public mensagem: Communication = new Communication();
  public id: number;

  public permissionList: FiltroPermissoes[] = [];
  public emailUserListByPermission: SelectItem[];
  public labelTitulo: string;
  public labelMensagem: string;
  public labelNotificarEmail: string;
  public labelUsuarios: string;
  public labelFiltrarUsuarios: string;
  public labelFiltrarUsuario: string;
  public labelVoltar: string;
  public labelSalvar: string;
  public labelFiltrar: string;

  // tslint:disable-next-line: max-line-length
  constructor(private route: ActivatedRoute, private router: Router,
              private translocoService: TranslocoService,
              private mensagemService: MensagemService,
              private messageService: MessageService,
              private usuarioService: UsuarioService) {}

  ngOnInit(): void {
    // tslint:disable-next-line: no-string-literal
    this.getLabelTanslate();
    this.route.data.subscribe (res => {
      this.copy = res['copy'];
    });
    this.id = this.route.snapshot.params['id'];
    if (this.id) {
      this.edit = this.copy ? false : true;
      this.getMensagemById(this.id);
    }
    this.colunas = [
      { field: 'email', header: 'Email' }
    ];
    this.getEmailUsers();
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('vpgs-adm-mensagens-titulo').subscribe(value => {
      this.labelTitulo = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-mensagens-mensagem').subscribe(value => {
      this.labelMensagem = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-mensagens-notificar-por-email').subscribe(value => {
      this.labelNotificarEmail = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-contas-usuarios').subscribe(value => {
      this.labelUsuarios = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-mensagens-filtrar-usuarios').subscribe(value => {
      this.labelFiltrarUsuarios = value;
    });
    this.translocoService.selectTranslate('voltar').subscribe(value => {
      this.labelVoltar = value;
    });
    this.translocoService.selectTranslate('salvar').subscribe(value => {
      this.labelSalvar = value;
    });
    this.translocoService.selectTranslate('filtrar').subscribe(value => {
      this.labelFiltrar = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-mensagens-filtrar-usuario').subscribe(value => {
      this.labelFiltrarUsuario = value;
    });
  }

  public getEmailUsers(): void {
    this.usuarioService.findAllEmailUsuarios().subscribe(res => {
      this.emailUserList = KeyValue.convertToSelectItem(res);
    });
  }

  public getMensagemById(id: number): void {
    this.mensagemService.findMensagemById(id).subscribe(res => {
      this.mensagem = res;
    });
  }

  public back(): void {
    this.router.navigate(['interno/administracao/mensagens']);
  }

  public save(): void {
    //this.mensagem.fromId = 32217; // considerando que já terei o id do usuario logado
    this.mensagemService.addMensagem(this.mensagem).subscribe(
      data => {
        this.showModal('success', 'Sucesso', 'Mensagem enviada com sucesso!');
        alert('Sucesso\n\nMensagem enviada com sucesso!');
        this.router.navigate(['interno/administracao/mensagens']);
      },
      error => {
        this.showModal('error', 'Erro', 'Erro ao enviar a mensagem!');
        //console.log(error);
      });
    this.back();
  }

  public filtrarUsuario(): void {
    this.removeCommunicationUser();
    if (this.selectedUsers.length > 0) {
      this.addCommunicationUser(this.selectedUsers);
    }
    if (this.permissionList.length > 0) {
      for (const item of this.permissionList) {
        this.usuarioService.findAllEmailUsuariosByPermission(item.aplicacaoP.id, item.grupoP.id, item.permissaoP.id).subscribe(res =>
          {
            this.emailUserListByPermission = KeyValue.convertToSelectItem(res);
          },
          error => {
            this.showModal('info', 'Informação',
            // tslint:disable-next-line: max-line-length
            'Não existem usuários para a permissão:' + '[' + item.aplicacaoP.value + ', ' + item.grupoP.value + ', ' + item.permissaoP.value + ']');
          }
        );
        if (this.emailUserListByPermission.length > 0) {
          // tslint:disable-next-line: no-shadowed-variable
          this.addCommunicationUser(this.emailUserListByPermission);
        }
      }
    }
    this.filterDialog = false;
  }

  private addCommunicationUser(listUser: SelectItem[]): void {
    for (const item of listUser) {
      this.mensagem.communicationUsersId.push(item.value);
      this.mensagem.communicationUsers.push(item.label);
    }
  }

  private removeCommunicationUser(): void {
    this.mensagem.communicationUsersId = [];
    this.mensagem.communicationUsers = [];
  }

  public showFilterUser(): void {
    this.filterDialog = true;
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

  public loadPermission(valueEmited: FiltroPermissoes[]): void {
    this.permissionList = valueEmited;
  }

}
