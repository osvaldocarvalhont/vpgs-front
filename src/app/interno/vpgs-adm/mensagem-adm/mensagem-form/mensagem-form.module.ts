import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faCheck, faChevronLeft, faFilter, faUserCheck} from '@fortawesome/free-solid-svg-icons';

import { MensagemFormRoutingModule } from './mensagem-form-routing.module';
import { MensagemFormComponent } from './mensagem-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';

import { EditorModule } from 'primeng/editor';
import { CheckboxModule } from 'primeng/checkbox';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { TranslocoModule } from '@ngneat/transloco';

@NgModule({
  declarations: [MensagemFormComponent],
  imports: [
    CommonModule,
    MensagemFormRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    FontAwesomeModule,
    EditorModule,
    CheckboxModule,
    TableModule,
    DialogModule,
    MultiSelectModule,
    SharedModule,
    ToastModule,
    TranslocoModule
  ]
})
export class MensagemFormModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faCheck, faChevronLeft, faFilter, faUserCheck);
  }
}
