import { FilterMensagem } from './../../../shared/models/filter-mensagem.model';
import { MensagemService } from './service/mensagem.service';
import { Communication } from './../../../shared/models/communication.model';
import { Mensagem } from './../../../shared/models/mensagem.model';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { LazyLoadEvent } from 'primeng/api';
import { ɵAnimationRendererFactory } from '@angular/platform-browser/animations';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-mensagem-adm',
  templateUrl: './mensagem-adm.component.html',
  styleUrls: ['./mensagem-adm.component.css']
})
export class MensagemAdmComponent implements OnInit {

  public colunas: any[];
  public itens: Communication[] = [];
  public selectedMensagem: Mensagem[];
  public mensagemSelecionada: Mensagem;
  public loading: boolean;
  public mensagens: Observable<Communication[]>;
  public totalRecords: number;
  public params = new HttpParams();
  public pt: any;
  public rangeDataFilter: Date[];
  public filter: FilterMensagem = new FilterMensagem();

  private readonly HORA_INICIO = 'T00:00:00';
  private readonly HORA_FIM = 'T23:59:59';

  public labelFiltro: string;
  public labelFiltrar: string;
  public labelLimpar: string;
  public labelDe: string;
  public labelTitulo: string;
  public labelData: string;

  constructor(private router: Router,
              private cdref: ChangeDetectorRef,
              private translocoService: TranslocoService,
              private mensagemService: MensagemService) { }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.loading = false;
    this.buildColumns();
    this.configCalendarPt();
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('filtro').subscribe(value => {
      this.labelFiltro = value;
    });
    this.translocoService.selectTranslate('filtrar').subscribe(value => {
      this.labelFiltrar = value;
    });
    this.translocoService.selectTranslate('limpar').subscribe(value => {
      this.labelLimpar = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-mensagens-de').subscribe(value => {
      this.labelDe = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-mensagens-titulo').subscribe(value => {
      this.labelTitulo = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-mensagens-data').subscribe(value => {
      this.labelData = value;
    });
  }

  private buildColumns(): void {
    this.colunas = [
      { field: 'from', header: 'De' },
      { field: 'title', header: 'Título'},
      { field: 'creationDate', header: 'Data' }
    ];
  }

  // tslint:disable-next-line: typedef
  public loadMensagens(valueEmited: LazyLoadEvent) {
    this.loading = true;
    // tslint:disable-next-line: no-string-literal
    this.filter.page = (valueEmited['first'] / 10).toString();
    this.filter.orderBy = valueEmited['sortField'] === 'de' ? 'de' : valueEmited['sortField'];
    this.filter.direction = valueEmited['sortOrder'] === 1 ? 'DESC' : 'ASC';
    this.getAllMensagem(this.filter);
  }

  public filtrar(): void {
    this.loading = true;
    this.filter.de = this.filter.de != null ? this.filter.de.toUpperCase() : null;
    this.filter.titulo = this.filter.titulo != null ? this.filter.titulo.toUpperCase() : null;
    if (this.rangeDataFilter !== null && this.rangeDataFilter !== undefined) {
      this.filter.dataInicio = this.toDateTime(new Date(this.rangeDataFilter[0]), this.HORA_INICIO);
      if (this.rangeDataFilter[1] !== null) {
        this.filter.dataFim = this.toDateTime(new Date(this.rangeDataFilter[1]), this.HORA_FIM);
      } else {
        this.filter.dataFim = null;
      }
    }
    this.getAllMensagem(this.filter);
  }

  public clear(): void {
    this.loading = true;
    this.filter.de = null;
    this.filter.titulo = null;
    this.rangeDataFilter = null;
    this.filter.dataInicio = null;
    this.filter.dataFim = null;
    this.getAllMensagem(this.filter);
  }

  public toDateTime(data: Date, hora: string): any {
    let returnDate = '';

    let year = data.getFullYear();
    let month = data.getMonth() + 1;
    let day = data.getDate();

    returnDate += year;
    returnDate += '-';

    if (month < 10) {
      returnDate += `0${month}-`;
    } else {
        returnDate += `${month}-`;
    }

    if (day < 10) {
      returnDate += `0${day}`;
    } else {
        returnDate += `${day}`;
    }

    returnDate += hora;
    return returnDate;
  }


  public getAllMensagem(filter: FilterMensagem): void {
    this.mensagemService.findAllMensagemFilterPage(this.filter).subscribe(res => {
      // tslint:disable-next-line: no-string-literal
      this.itens =  res['content'];
      // tslint:disable-next-line: no-string-literal
      this.totalRecords = res['totalElements'];
      this.loading = false;
    });
  }

  private configCalendarPt(): void {
    this.pt = {
      firstDayOfWeek: 1,
      dayNames: ['domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'],
      monthNamesShort: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };
  }



}
