import { MensagemFormModule } from './mensagem-form/mensagem-form.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MensagemAdmRoutingModule } from './mensagem-adm-routing.module';
import { MensagemAdmComponent } from './mensagem-adm.component';
import { TableModule } from 'primeng/table';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faFileExcel, faCheckCircle, faPlusSquare, faTrashAlt, faEdit, faEye, faFilter, faTimes} from '@fortawesome/free-solid-svg-icons';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { SharedModule } from './../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PanelModule } from 'primeng/panel';
import { CalendarModule } from 'primeng/calendar';
import { TranslocoModule } from '@ngneat/transloco';

@NgModule({
  declarations: [MensagemAdmComponent],
  imports: [
    CommonModule,
    MensagemAdmRoutingModule,
    TableModule,
    FontAwesomeModule,
    ButtonModule,
    InputTextModule,
    SharedModule,
    MensagemFormModule,
    FormsModule,
    ReactiveFormsModule,
    PanelModule,
    CalendarModule,
    TranslocoModule
  ]
})
export class MensagemAdmModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faFileExcel, faCheckCircle, faPlusSquare, faTrashAlt, faEdit, faEye, faFilter, faTimes);
  }
}
