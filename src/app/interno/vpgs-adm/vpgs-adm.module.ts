import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenubarModule } from 'primeng/menubar';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

import { VpgsAdmRoutingModule } from './vpgs-adm-routing.module';
import { VpgsAdmComponent } from './vpgs-adm.component';
//import { HashLocationStrategy, LocationStrategy } from '@angular/common';

@NgModule({
  declarations: [VpgsAdmComponent],
  imports: [
    CommonModule,
    VpgsAdmRoutingModule,
    MenubarModule,
    FontAwesomeModule
  ],
  //providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy}]
})
export class VpgsAdmModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faSignOutAlt);
  }
 }
