import { AprovacaoModule } from './aprovacao/aprovacao.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VpgsAdmComponent } from './vpgs-adm.component';

const routes: Routes = [
  {
    path: '',
    component: VpgsAdmComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'solicitacoes',
        loadChildren: () => import('./solicitacoes/solicitacoes.module').then(m => m.SolicitacoesModule)
      },
      {
        path: 'permissoes',
        loadChildren: () => import('./permissoes/permissoes.module').then(m => m.PermissoesModule)
      },
      {
        path: 'usuarios',
        loadChildren: () => import('./usuarios/usuarios.module').then(m => m.UsuariosModule)
      },
      {
        path: 'perguntas',
        loadChildren: () => import('./perguntas/perguntas.module').then(m => m.PerguntasModule)
      },
      {
        path: 'mensagens',
        loadChildren: () => import('./mensagem-adm/mensagem-adm.module').then(m => m.MensagemAdmModule)
      },
      {
        path: 'aprovacao',
        loadChildren: () => import('./aprovacao/aprovacao.module').then(m => m.AprovacaoModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VpgsAdmRoutingModule { }
