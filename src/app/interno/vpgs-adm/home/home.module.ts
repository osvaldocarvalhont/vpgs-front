import { HomeComponent } from './../home/home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faUser, faShieldAlt, faThumbsUp, faChartBar, faQuestion, faEnvelope, faUsers, faCheck } from '@fortawesome/free-solid-svg-icons';

import { HomeRoutingModule } from './home-routing.module';
import { TranslocoModule } from '@ngneat/transloco';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FontAwesomeModule,
    TranslocoModule
  ]
})
export class HomeModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faUser, faShieldAlt, faThumbsUp, faChartBar, faQuestion, faEnvelope, faUsers, faCheck );
  }
}
