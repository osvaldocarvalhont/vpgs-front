import { MenuItem } from 'primeng/api';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public items: MenuItem[];

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.router.navigate(['home'], { relativeTo: this.route });
    this.buildMenu();
  }

  public buildMenu(): void {
    this.items = [
      {
        label: 'Home', routerLink: 'interno/administracao'
      },
      {
        label: 'Contas',
        items: [
            {label: 'Usuários', routerLink: '/usuarios'},
            {label: 'Permissões', routerLink: '/permissoes'},
            {label: 'Solicitações', routerLink: '/interno/administracao/solicitacoes'},
            {label: 'Acessos', routerLink: '/acessos'}
        ]
      },
      {
          label: 'Comunicação',
          items: [
              {label: 'Mensagens', routerLink: '/mensagens'},
              {label: 'Aprovação de Email', routerLink: '/aprovacao'},
              {label: 'Perguntas Frequentes', routerLink: '/perguntas'}
          ]
      }];
    }

}
