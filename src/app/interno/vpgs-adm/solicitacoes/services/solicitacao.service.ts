import { FilterAcessos } from './../../../../shared/models/filter-acessos.models';
import { FilterSolicitacoes } from './../../../../shared/models/filter-solicitacoes.models';
import { Request } from './../../../../shared/models/request.model';
import { environment } from './../../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SolicitacaoService {

  private readonly ENDPOINT_REQUEST = 'requests';
  private readonly ENDPOINT_REQUEST_USER = '/user';
  private readonly ENDPOINT_REQUEST_REJECT = '/rejected';
  private readonly ENDPOINT_REQUEST_ACCEPT = '/accepted';

  constructor(private http: HttpClient) { }

  public findAllRequest(filter: FilterSolicitacoes): Observable<Request[]> {
    return this.http.get<Request[]>(`${environment.BASE_URL.concat(this.ENDPOINT_REQUEST)}`, { params: this.buildParam(filter) });
  }

  public findRequestById(id: number): Observable<Request> {
    return this.http.get<Request>(`${environment.BASE_URL.concat(this.ENDPOINT_REQUEST)}/${id}`);
  }

  public findRequestByUser(filter: FilterAcessos): Observable<Request[]> {
    return this.http.get<Request[]>(`${environment.BASE_URL.concat(this.ENDPOINT_REQUEST)
      .concat(this.ENDPOINT_REQUEST_USER)}`, { params: this.buildParam(filter) });
  }

  public save(request: Request): Observable<Request> {
    return this.http.post<Request>(`${environment.BASE_URL.concat(this.ENDPOINT_REQUEST)}`, request);
  }

  public updateRequestAsRejected(id: number, motivoRejeicao: string): Observable<Request> {
    return this.http.put<Request>(`${environment.BASE_URL
      .concat(this.ENDPOINT_REQUEST).concat(this.ENDPOINT_REQUEST_REJECT)}/${id}`, motivoRejeicao);
  }

  public updateRequestAsAccepted(id: number, request: Request): Observable<Request> {
    return this.http.put<Request>(`${environment.BASE_URL
      .concat(this.ENDPOINT_REQUEST).concat(this.ENDPOINT_REQUEST_ACCEPT)}/${id}`, request);
  }

  public buildParam(filter: any): HttpParams {
    let parameters = new HttpParams();
    // tslint:disable-next-line: forin
    for (const key in filter) {
      const value = filter[key];
      if (value !== null && value !== undefined) {
        parameters = parameters.append(key, value.toString());
      }
    }
    return parameters;
  }

}
