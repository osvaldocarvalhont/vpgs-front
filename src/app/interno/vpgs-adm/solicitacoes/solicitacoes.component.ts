import { FilterSolicitacoes } from './../../../shared/models/filter-solicitacoes.models';
import { Request } from './../../../shared/models/request.model';
import { SolicitacaoService } from './services/solicitacao.service';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { LazyLoadEvent, MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-solicitacoes',
  templateUrl: './solicitacoes.component.html',
  styleUrls: ['./solicitacoes.component.css'],
  providers: [MessageService, SolicitacaoService]
})
export class SolicitacoesComponent implements OnInit {

  public solicitacoes: Request[];
  public cols: any[];
  public solicitacaoSelected: Request;
  public displayDiologReject = false;
  public assunto = 'Solicitação rejeitada no Vale Procurement Global Services';
  public rejectionNote: string;
  public idRequestReject: number;
  public loading: boolean;
  public totalRecords: number;
  public filter: FilterSolicitacoes = new FilterSolicitacoes();
  public labelAceitar: string;
  public labelRejeitar: string;
  public labelRejeicaoMudanca: string;
  public labelEnviar: string;
  public toastSuccess: string;
  public toastError: string;
  public labeInfoRegistrosTabela: string;

  constructor(private messageService: MessageService,
              private solicitacaoService: SolicitacaoService,
              private translocoService: TranslocoService,
              private cdref: ChangeDetectorRef,
              private router: Router) { }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.buildColumns();
    this.getAllRequest(this.filter);
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('aceitar').subscribe(value => {
      this.labelAceitar = value;
    });
    this.translocoService.selectTranslate('rejeitar').subscribe(value => {
      this.labelRejeitar = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-solicitacao').subscribe(value => {
      this.labelRejeicaoMudanca = value;
    });
    this.translocoService.selectTranslate('enviar').subscribe(value => {
      this.labelEnviar = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-solicitacao-toast-success').subscribe(value => {
      this.toastSuccess = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-solicitacao-toast-error').subscribe(value => {
      this.toastError = value;
    });
    this.translocoService.selectTranslate('info-total-registros-tabela').subscribe(value => {
      this.labeInfoRegistrosTabela = value;
    });
  }

  private buildColumns(): void {
    this.cols = [
      { field: 'email', header: 'Email' },
      { field: 'firstName', header: 'Nome' },
      { field: 'lastName', header: 'Último Nome' },
      { field: 'serviceProvider', header: 'Tipo' },
      { field: 'createdOn', header: 'Data do Pedido' }
    ];
  }

  public getAllRequest(filter: FilterSolicitacoes): void {
    this.solicitacaoService.findAllRequest(filter).subscribe(res => {
      // tslint:disable-next-line: no-string-literal
      this.solicitacoes =  res['content'];
      this.totalRecords = res['totalElements'];
      this.loading = false;
    });
  }

  public accept(solicitacaoSelected: Request): void {
    this.setRequestAccepted(solicitacaoSelected.id, solicitacaoSelected);
    this.getRequestById(solicitacaoSelected.id);

  }

  private getRequestById(id: number): void {
    this.solicitacaoService.findRequestById(id).subscribe(data => {
      const idUser = data.userId;
      const idRequest = data.id;
      this.router.navigate(['/interno/administracao/usuarios/editRequest', idUser, idRequest]);
    });
  }

  public reject(solicitacaoSelected: Request): void {
    this.displayDiologReject = true;
    this.idRequestReject = solicitacaoSelected.id;
  }

  public setRequestRejected(): void {
    this.displayDiologReject = false;
    this.solicitacaoService.updateRequestAsRejected(this.idRequestReject, this.rejectionNote).subscribe(
      data => {
        this.showModal('success', 'Sucesso', this.toastSuccess);
      },
      error => {
        this.showModal('error', 'Erro', this.toastError);
        console.log(error);
      });
  }

  public setRequestAccepted(id: number, requestAccepted: Request): void {
    this.solicitacaoService.updateRequestAsAccepted(id, requestAccepted).subscribe(data => {});
    console.log(this.solicitacaoSelected);
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

  public loadData(event: LazyLoadEvent): void {
    this.loading = true;
    // tslint:disable-next-line: no-string-literal
    this.filter.page = (event['first'] / 10).toString();
    this.filter.orderBy = 'createdOn';
    this.filter.direction = event['sortOrder'] === 1 ? 'DESC' : 'ASC';
    this.getAllRequest(this.filter);
  }

}
