import { SolicitacoesComponent } from './../solicitacoes/solicitacoes.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faTimes, faCheck } from '@fortawesome/free-solid-svg-icons';

import { SolicitacoesRoutingModule } from './solicitacoes-routing.module';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { InputTextModule } from 'primeng/inputtext';
import { DialogModule } from 'primeng/dialog';
import { FormsModule } from '@angular/forms';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TranslocoModule } from '@ngneat/transloco';

@NgModule({
  declarations: [SolicitacoesComponent],
  imports: [
    CommonModule,
    SolicitacoesRoutingModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    FontAwesomeModule,
    ConfirmDialogModule,
    ToastModule,
    ButtonModule,
    DialogModule,
    FormsModule,
    InputTextareaModule,
    TranslocoModule
  ]
})
export class SolicitacoesModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTimes, faCheck);
  }
}
