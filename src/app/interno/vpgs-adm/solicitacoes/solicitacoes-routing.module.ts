import { SolicitacoesComponent } from './../solicitacoes/solicitacoes.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosComponent } from '../usuarios/usuarios.component';

const routes: Routes = [
  {
    path: '',
    component: SolicitacoesComponent
  },
  {
    path: 'edit',
    component: UsuariosComponent,
    children: [
      {
        path: 'editUser/:id',
        loadChildren: () => import('../usuarios/usuarios-form/usuarios-form.module').then(m => m.UsuariosFormModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitacoesRoutingModule { }
