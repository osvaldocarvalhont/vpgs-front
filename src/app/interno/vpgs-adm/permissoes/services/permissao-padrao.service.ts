import { FilterGeneral } from './../../../../shared/models/filter-general.model';
import { environment } from './../../../../../environments/environment';
import { PermissoesPadrao } from './../../../../shared/models/permissoes-padrao.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PermissaoPadraoService {

  private readonly ENDPOINT_STANDARD_PROFILE = 'standard-profiles';
  private readonly ENDPOINT_STANDARD_PROFILE_DELETE = '/delete';

  constructor(private http: HttpClient) { }

  public findAllStandardProfile(filter: FilterGeneral): Observable<PermissoesPadrao[]> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<PermissoesPadrao[]>(`${environment.BASE_URL.concat(this.ENDPOINT_STANDARD_PROFILE)}`, { params: this.buildParam(filter) });
  }

  public save(permissoesPadrao: PermissoesPadrao): Observable<PermissoesPadrao> {
    return this.http.post<PermissoesPadrao>(`${environment.BASE_URL.concat(this.ENDPOINT_STANDARD_PROFILE)}`, permissoesPadrao);
  }

  public update(permissoesPadrao: PermissoesPadrao): Observable<PermissoesPadrao> {
    return this.http.put<PermissoesPadrao>(`${environment.BASE_URL.concat(this.ENDPOINT_STANDARD_PROFILE)}`, permissoesPadrao);
  }

  public delete(list: number[]): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_STANDARD_PROFILE).concat(this.ENDPOINT_STANDARD_PROFILE_DELETE)}`, list);
  }

  public findPermissaoPadraoById(id: number): Observable<PermissoesPadrao> {
    return this.http.get<PermissoesPadrao>(`${environment.BASE_URL.concat(this.ENDPOINT_STANDARD_PROFILE)}/${id}`);
  }

  public buildParam(filter: FilterGeneral): HttpParams {
    let parameters = new HttpParams();
    // tslint:disable-next-line: forin
    for (const key in filter) {
      const value = filter[key];
      if (value !== null && value !== undefined) {
        parameters = parameters.append(key, value.toString());
      }
    }
    return parameters;
  }

}
