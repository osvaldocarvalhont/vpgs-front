import { UserPermission } from './../../../../shared/models/user-permission.model';
import { FilterPermissoes } from './../../../../shared/models/filter-permissoes.model';
import { environment } from './../../../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PermissaoService {

  private readonly ENDPOINT_USER_PERMISSION = 'user-permissions';
  private readonly ENDPOINT_USER_PERMISSION_EXPORT = '/export';

  constructor(private http: HttpClient) { }

  public findAllPermissionByUserPage(filter: FilterPermissoes): Observable<UserPermission[]> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<UserPermission[]>(`${environment.BASE_URL.concat(this.ENDPOINT_USER_PERMISSION)}`, { params: this.buildParam(filter) });
  }

  public getXlsxAllPermissoesFilter(filter: FilterPermissoes): any {
    return this.http.get<any>(`${environment.BASE_URL.concat(this.ENDPOINT_USER_PERMISSION)
      .concat(this.ENDPOINT_USER_PERMISSION_EXPORT)}`, this.buildOptions(filter));
  }

  public buildParam(filter: FilterPermissoes): HttpParams {
    let parameters = new HttpParams();
    // tslint:disable-next-line: forin
    for (const key in filter) {
      const value = filter[key];
      if (value !== null && value !== undefined) {
        parameters = parameters.append(key, value.toString());
      }
    }
    return parameters;
  }

  public buildOptions(filter: FilterPermissoes): any {
    let headers = new HttpHeaders({'content-type' : 'application/json',
    'Access-Control-Allow-Origin' : '*'});

    var options = {
      params: this.buildParam(filter),
      headers: headers,
      responseType: 'blob'};

    return options;
  }

}
