import { CountryService } from 'src/app/interno/vpgs-adm/usuarios/services/country.service';
import { AuthMode } from './../../../../shared/enums/auth-mode.enum';
import { MainField } from './../../../../shared/enums/main-field.enum';
import { PermissaoPadraoService } from './../services/permissao-padrao.service';
import { FiltroFiltros } from './../../../../shared/models/filtro-filtros.model';
import { FiltroPermissoesPadrao } from './../../../../shared/models/filtro-permissoes-padrao.model';
import { Component, OnInit } from '@angular/core';
import { SelectItem, MessageService } from 'primeng/api';
import { Location } from '@angular/common';
import { FiltroPermissoes } from 'src/app/shared/models/filtro-permissoes.model';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissoesPadrao } from 'src/app/shared/models/permissoes-padrao.model';
import { UserPermission } from 'src/app/shared/models/user-permission.model';
import { FiltroFiltrosPadrao } from 'src/app/shared/models/filtro-filtros-padrao.model';
import { BaseCoreComponent } from 'src/app/shared/base-core/base-core/base-core.component';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-permissoes-padrao',
  templateUrl: './permissoes-padrao.component.html',
  styleUrls: ['./permissoes-padrao.component.css']
})
export class PermissoesPadraoComponent extends BaseCoreComponent implements OnInit {

  public permissionList: FiltroPermissoes[] = [];
  public filterList: FiltroFiltrosPadrao[] = [];
  public totalRecords: number;
  public totalRecordsFilter: number;
  public permissoesPadrao: PermissoesPadrao = new PermissoesPadrao();
  public edit = false;
  public id: number;
  public labelNome: string;
  public labelHabilitado: string;
  public labelVoltar: string;
  public labelSalvar: string;

  constructor(
              private location: Location,
              private permissaoPadraoService: PermissaoPadraoService,
              private translocoService: TranslocoService,
              private router: Router,
              private route: ActivatedRoute,
              private countryService: CountryService,
              private messageService: MessageService) {
    super();
  }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.id = this.route.snapshot.params.id;
    if (this.id) {
      this.edit = true;
      this.getPermissaoPadraoById(this.id);
    }
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('nome').subscribe(value => {
      this.labelNome = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-permissao-habilitado').subscribe(value => {
      this.labelHabilitado = value;
    });
    this.translocoService.selectTranslate('voltar').subscribe(value => {
      this.labelVoltar = value;
    });
    this.translocoService.selectTranslate('salvar').subscribe(value => {
      this.labelSalvar = value;
    });
  }

  public getPermissaoPadraoById(id: number): void {
    this.permissaoPadraoService.findPermissaoPadraoById(id).subscribe(res => {
      this.permissoesPadrao = res;

      let listFilter: FiltroFiltrosPadrao = new FiltroFiltrosPadrao();
      for (let p of this.permissoesPadrao.filterPermission) {
        listFilter.id = p.id;
        listFilter.standardProfile = p.standardProfile;
        listFilter.mainField = p.mainField;
        listFilter.comparisonType = p.comparisonType;
        listFilter.mainValue = p.mainValue;
        listFilter.mainFieldValue = this.setMainField(p.mainField);
        listFilter.comparisonTypeValue = this.setComparisonType(p.comparisonType);
        if (p.mainField === 'AUTHMODE') {
          listFilter.mainValueValue = this.setAuthMode(p.mainValue);
        } else if (p.mainField === 'COUNTRY') {
          listFilter.mainValueValue = this.setCountry(p.mainValue);
        } else {
          listFilter.mainValueValue = p.mainValue;
        }
        listFilter.isRemoved = p.isRemoved ? p.isRemoved : false;
        this.filterList.push(Object.assign({}, listFilter));
      }

      let listPerm: FiltroPermissoes = new FiltroPermissoes();
      for (let p of this.permissoesPadrao.permission) {
        listPerm.id = p.id;
        listPerm.aplicacaoKey = p.serviceProviderId;
        listPerm.aplicacao = p.serviceProvider;
        listPerm.grupoKey = p.groupId;
        listPerm.grupo = p.group;
        listPerm.permissaoKey = p.permissionId;
        listPerm.permissao = p.permission;
        listPerm.isRemoved = p.isRemoved ? p.isRemoved : false;
        this.permissionList.push(Object.assign({}, listPerm));
      }
      this.totalRecords = this.permissionList.length;
      this.totalRecordsFilter = this.filterList.length;
    });
  }

  public back(): void {
    this.location.back();
    // this.router.navigate(['interno/administracao/permissoes']);
    // this.router.navigate(['/interno/administracao/permissoes/permissaoPadrao']);
  }

  public save(): void {
    this.getFilterList();
    this.getPermissionList();
    // console.log(this.permissoesPadrao);

    if (!this.edit) {
      this.permissaoPadraoService.update(this.permissoesPadrao).subscribe(
        data => {
          this.showModal('success', 'Sucesso', 'Permissão padrão salva com sucesso!');
          alert('Sucesso\n\nPermissão padrão salva com sucesso!');
          this.router.navigate(['interno/administracao/permissoes'])
         //this.load();
        },
        error => {
          const msg = error.error.message === null ? 'Erro ao salvar a permissão padrão!' : error.error.message;
          this.showModal('error', 'Erro', msg);
        });
    } else {
      this.permissaoPadraoService.update(this.permissoesPadrao).subscribe(
        data => {
          this.showModal('success', 'Sucesso', 'Permissão padrão atualizada com sucesso!');
        },
        error => {
          this.showModal('error', 'Erro', 'Erro ao atualizar a permissão padrão!');
        });
    }
  }

  private getFilterList(): void {
    if (this.filterList.length > 0) {
      this.permissoesPadrao.filterPermission = [];
      for (const item of this.filterList) {
        const filtro: FiltroFiltrosPadrao = new FiltroFiltrosPadrao();
        filtro.id = item.id;
        filtro.standardProfile = item.standardProfile;
        filtro.mainField = item.mainField;
        filtro.comparisonType = item.comparisonType;
        filtro.mainValue = item.mainValue;
        filtro.isRemoved = item.isRemoved;

        this.permissoesPadrao.filterPermission.push(Object.assign({}, filtro));
      }
    }
  }

  private getPermissionList(): void {
    if (this.permissionList.length > 0) {
      this.permissoesPadrao.permission = [];
      for (const item of this.permissionList) {
        const permissao: UserPermission = new UserPermission();
        permissao.id = item.id ? item.id : null;
        permissao.serviceProviderId = item.aplicacaoKey;
        permissao.serviceProvider = item.aplicacao;
        permissao.groupId = item.grupoKey;
        permissao.group = item.grupo;
        permissao.permissionId = item.permissaoKey;
        permissao.permission = item.permissao;
        permissao.isRemoved = item.isRemoved;

        this.permissoesPadrao.permission.push(Object.assign({}, permissao));
      }
    }
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

  public loadPermission(valueEmited: FiltroPermissoes[]): void {
    this.permissionList = valueEmited;
  }

  public loadFilter(valueEmited: FiltroFiltrosPadrao[]): void {
    this.filterList = valueEmited;
  }

}
