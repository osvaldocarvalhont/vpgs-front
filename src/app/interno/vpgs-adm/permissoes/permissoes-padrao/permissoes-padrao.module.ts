import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckboxModule } from 'primeng/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { ToastModule } from 'primeng/toast';

import { PermissoesPadraoRoutingModule } from './permissoes-padrao-routing.module';
import { PermissoesPadraoComponent } from './permissoes-padrao.component';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faPlus, faTrashAlt, faTimes, faCheck, faExpandAlt, faCompressAlt, faMinus, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslocoModule } from '@ngneat/transloco';

@NgModule({
  declarations: [PermissoesPadraoComponent],
  imports: [
    CommonModule,
    PermissoesPadraoRoutingModule,
    CheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    TableModule,
    FontAwesomeModule,
    DialogModule,
    DropdownModule,
    InputTextModule,
    MultiSelectModule,
    ToastModule,
    SharedModule,
    TranslocoModule
  ]
})
export class PermissoesPadraoModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faPlus, faTrashAlt, faTimes, faCheck, faExpandAlt, faCompressAlt, faMinus, faChevronLeft);
  }
}
