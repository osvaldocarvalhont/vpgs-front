import { PermissoesComponent } from './../permissoes/permissoes.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: PermissoesComponent,
    children: [
      {
        path: 'permissaoPadrao',
        loadChildren: () => import('./permissoes-padrao/permissoes-padrao.module').then(m => m.PermissoesPadraoModule)
      },
      {
        path: 'edit/:id',
        loadChildren: () => import('./permissoes-padrao/permissoes-padrao.module').then(m => m.PermissoesPadraoModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermissoesRoutingModule { }
