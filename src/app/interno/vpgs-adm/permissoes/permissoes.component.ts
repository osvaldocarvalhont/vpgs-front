import { AuthTokenService } from './../../../shared/authToken/auth-token.service';
import { PermissaoPadraoService } from './services/permissao-padrao.service';
import { FilterGeneral } from './../../../shared/models/filter-general.model';
import { UserPermission } from 'src/app/shared/models/user-permission.model';
import { PermissaoService } from './services/permissao.service';
import { LazyLoadEvent } from 'primeng/api';
import { FilterPermissoes } from './../../../shared/models/filter-permissoes.model';
import { PermissoesPadrao } from './../../../shared/models/permissoes-padrao.model';
import { Permissoes } from './../../../shared/models/permissoes.model';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { TranslocoService } from '@ngneat/transloco';
@Component({
  selector: 'app-permissoes',
  templateUrl: './permissoes.component.html',
  styleUrls: ['./permissoes.component.css'],
  providers: [ConfirmationService, MessageService, PermissaoService, PermissaoPadraoService]
})
export class PermissoesComponent implements OnInit {

  public colunasPermisao: any[];
  public colunasPermissoesPadrao: any[];
  public itensPermissoesPadrao: PermissoesPadrao[];
  public selectedPermissoes: Permissoes[];
  public selectedPermissoesPadrao: PermissoesPadrao[];
  public index: number = 0;

  public edit = false;
  public add = false;
  public permissao = true;
  public permissaoPadraoSelecionada: PermissoesPadrao;

  public filter: FilterPermissoes = new FilterPermissoes();
  public loading: boolean;
  public totalRecords: number;
  public itensPermissao: UserPermission[];

  public filterPermissaoPadrao: FilterGeneral = new FilterGeneral();
  public loadingPermissaoPadrao: boolean;
  public totalRecordsPermissaoPadrao: number;
  public permissionList: number[] = [];
  public labelPermissoes: string;
  public labelPermissao: string;
  public labelPermissoesPadrao: string;
  public labelUsuario: string;
  public labelAplicacao: string;
  public labelGrupo: string;
  public labelFiltrar: string;
  public labelLimpar: string;
  public labelExportar: string;
  public labelPermNaoEncontrada: string;
  public labelAdicionar: string;
  public labelEditar: string;
  public labelRemover: string;
  public labelNome: string;
  public labelHabilitado: string;
  public labeInfoRegistrosTabela: string;
  public isAdm: boolean;

  constructor(private confirmationService: ConfirmationService,
              private permissaoService: PermissaoService,
              private permissaoPadraoService: PermissaoPadraoService,
              private translocoService: TranslocoService,
              private messageService: MessageService,
              private cdref: ChangeDetectorRef,
              private router: Router,
              private tokenAuth : AuthTokenService) { }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.buildColumns();

    var token = this.tokenAuth.decodePayloadJWT();

    //console.info(this.tokenAuth.decodePayloadJWT());

    //var json: string   = localStorage.getItem('currentUser');

    //var user: User  = JSON.parse(json);
    this.isAdm      = token.isAdm;
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('vpgs-adm-menu-contas-permissoes').subscribe(value => {
      this.labelPermissoes = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-permissao-permPadrao').subscribe(value => {
      this.labelPermissoesPadrao = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-permissao-usuario').subscribe(value => {
      this.labelUsuario = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-permissao-permissao').subscribe(value => {
      this.labelPermissao = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-permissao-aplicacao').subscribe(value => {
      this.labelAplicacao = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-permissao-grupo').subscribe(value => {
      this.labelGrupo = value;
    });
    this.translocoService.selectTranslate('filtrar').subscribe(value => {
      this.labelFiltrar = value;
    });
    this.translocoService.selectTranslate('limpar').subscribe(value => {
      this.labelLimpar = value;
    });
    this.translocoService.selectTranslate('exportar').subscribe(value => {
      this.labelExportar = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-permissao-perm-nao-encontrada').subscribe(value => {
      this.labelPermNaoEncontrada = value;
    });
    this.translocoService.selectTranslate('adicionar').subscribe(value => {
      this.labelAdicionar = value;
    });
    this.translocoService.selectTranslate('editar').subscribe(value => {
      this.labelEditar = value;
    });
    this.translocoService.selectTranslate('remover').subscribe(value => {
      this.labelRemover = value;
    });
    this.translocoService.selectTranslate('nome').subscribe(value => {
      this.labelNome = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-permissao-habilitado').subscribe(value => {
      this.labelHabilitado = value;
    });
    this.translocoService.selectTranslate('info-total-registros-tabela').subscribe(value => {
      this.labeInfoRegistrosTabela = value;
    });
  }

  private buildColumns(): void {
    this.colunasPermisao = [
      { field: 'emailUser', header: this.labelUsuario },
      { field: 'serviceProvider', header: this.labelAplicacao},
      { field: 'group', header: this.labelGrupo },
      { field: 'permission', header: this.labelPermissao }
    ];

    this.colunasPermissoesPadrao = [
      { field: 'isEnabled', header: 'Habilitado' },
      { field: 'name', header: 'Nome'},

    ];
  }

  public exportExcel(): void {
    this.loading = true;
    this.setFilter();
    this.getPermissoesExcel(this.filter);
  }

  public getPermissoesExcel(filter: FilterPermissoes): void {
    this.permissaoService.getXlsxAllPermissoesFilter(this.filter).subscribe(res => {
      this.saveAsExcelFile(res, 'Permissões.xlsx');
      this.loading = false;
    }, error => {
      console.error(error);
    });
  }

  public saveAsExcelFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        let EXCEL_EXTENSION = '.xlsx';

        let data: Blob = new Blob([buffer], {
            type: 'application/octet-stream'
        });
        FileSaver.saveAs(data, fileName);
    });
  }

  public permPadraoSelecionada(permissao: PermissoesPadrao): void {
    this.permissaoPadraoSelecionada = permissao;
  }

  public editPermissaoPadrao(): void{
    if (this.permissaoPadraoSelecionada != null) {
      this.edit = true;
      this.permissao = false;
      // let id = this.permissaoPadraoSelecionada.id;
      //console.log(this.permissaoPadraoSelecionada);
      this.router.navigate(['/interno/administracao/permissoes/edit', this.permissaoPadraoSelecionada.id]);
    }
  }

  public addPermissaoPadrao(): void{
      this.add = true;
      this.permissao = false;
      this.router.navigate(['/interno/administracao/permissoes/permissaoPadrao']);
  }

  public removePermissaoPadrao(): void{
    if (this.selectedPermissoesPadrao != null) {
      this.buildPermissionListSelected();
      this.showConfirmationDiolog();
    }
  }

  private buildPermissionListSelected(): void {
    for (const perm of this.selectedPermissoesPadrao) {
        this.permissionList.push(perm.id);
    }
  }


  private showConfirmationDiolog(): void {
    let msgConfirmation = 'Gostaria mesmo de remover a seleção?';
    let msgModal = (this.selectedPermissoesPadrao.length === 1) ? 'Permissão removida com sucesso!' : ' Permissões removidas com sucesso!';

    this.confirmationService.confirm({
      message: msgConfirmation,
      acceptLabel: 'Sim',
      rejectLabel: 'Não',
      accept: () => {
        this.permissaoPadraoService.delete(this.permissionList).subscribe(data =>
          {
          this.showModal('success', 'Sucesso', msgModal);
          alert('Sucesso\n\n' +msgModal);
          //this.load();
          this.router.navigate(['/interno/administracao/permissoes']);
          },
        );
      },
      reject: () => {
        this.selectedPermissoesPadrao = [];
        this.permissionList = [];
      }
    });
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

  public filtrar(): void {
    this.loading = true;
    this.setFilter();
    this.getAllPermission(this.filter);
  }

  private setFilter() {
    this.filter.email = this.filter.email != null ? this.filter.email.toUpperCase() : null;
    this.filter.serviceProvider = this.filter.serviceProvider != null ? this.filter.serviceProvider.toUpperCase() : null;
    this.filter.group = this.filter.group != null ? this.filter.group.toUpperCase() : null;
    this.filter.permission = this.filter.permission != null ? this.filter.permission.toUpperCase() : null;
  }

  public clear(): void {
    this.filter.email = null;
    this.filter.serviceProvider = null;
    this.filter.group = null;
    this.filter.permission = null;
    this.getAllPermission(this.filter);
  }

  public loadData(event: LazyLoadEvent): void {
    this.loading = true;
    // tslint:disable-next-line: no-string-literal
    this.filter.page = (event['first'] / 10).toString();
    if(event['sortField'] === 'emailUser'){
      this.filter.orderBy = 'user.email'
    } else if (event['sortField'] === 'group'){
      this.filter.orderBy = 'groupCode.name'
    } else if (event['sortField'] === 'permission'){
      this.filter.orderBy = 'permissionCode.name'
    } else {
      this.filter.orderBy = 'serviceProvider.name';
    }
    this.filter.direction = event['sortOrder'] === 1 ? 'ASC' : 'DESC';
    this.getAllPermission(this.filter);
  }

  public getAllPermission(filter: FilterPermissoes): void {
    this.permissaoService.findAllPermissionByUserPage(this.filter).subscribe(res => {
      // tslint:disable-next-line: no-string-literal
      this.itensPermissao =  res['content'];
      // tslint:disable-next-line: no-string-literal
      this.totalRecords = res['totalElements'];
      this.loading = false;
    });
  }

  public loadDataPermissaoPadrao(event: LazyLoadEvent): void {
    this.loadingPermissaoPadrao = true;
    // tslint:disable-next-line: no-string-literal
    this.filterPermissaoPadrao.page = (event['first'] / 10).toString();
    this.filterPermissaoPadrao.orderBy = event['sortField'];
    this.filterPermissaoPadrao.direction = event['sortOrder'] === 1 ? 'ASC' : 'DESC';
    this.getAllPermissionPadrao(this.filterPermissaoPadrao);
  }

  public getAllPermissionPadrao(filterPermissaoPadrao: FilterGeneral): void {
    this.permissaoPadraoService.findAllStandardProfile(filterPermissaoPadrao).subscribe(res => {
      // tslint:disable-next-line: no-string-literal
      this.itensPermissoesPadrao =  res['content'];
      // tslint:disable-next-line: no-string-literal
      this.totalRecordsPermissaoPadrao = res['totalElements'];
      this.loadingPermissaoPadrao = false;
    });
  }

  public load(): void {
    //location.reload();
  }

}
