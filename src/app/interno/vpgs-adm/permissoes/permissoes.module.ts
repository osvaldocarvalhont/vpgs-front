import { TabViewModule } from 'primeng/tabview';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { PermissoesComponent } from './../permissoes/permissoes.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { faFileExcel, faCheckCircle, faPlusSquare, faTrashAlt, faEdit, faFilter} from '@fortawesome/free-solid-svg-icons';

import { PermissoesRoutingModule } from './permissoes-routing.module';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { InputTextModule } from 'primeng/inputtext';
import { PanelModule } from 'primeng/panel';
import { FormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';


@NgModule({
  declarations: [PermissoesComponent],
  imports: [
    CommonModule,
    PermissoesRoutingModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    FontAwesomeModule,
    TabViewModule,
    ConfirmDialogModule,
    ToastModule,
    PanelModule,
    FormsModule,
    TranslocoModule
  ]
})
export class PermissoesModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faFileExcel, faCheckCircle, faPlusSquare, faTrashAlt, faEdit, faFilter);
  }
}
