import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Mensagem } from 'src/app/shared/models/mensagem.model';

const ELEMENT_DATA: Mensagem[] = [
  {
    id: 1,
    fromId: 'dcapalazzi@stefanini.com',
    assunto: 'teste',
    mensagen: 'teste teste teste teste',
    data: '03/03/2020',
    arquivada: true,
    importante: true,
    lida: false,
    status: 'Aguardando Aprovação',
  },
  {
    id: 2,
    fromId: 'dcapalazzi@stefanini.com',
    assunto: 'teste1',
    mensagen: 'teste1 teste1 teste1 teste1',
    data: '03/03/2020',
    arquivada: true,
    importante: true,
    lida: false,
    status: 'Aguardando Aprovação',
  },
  {
    id: 3,
    fromId: 'dcapalazzi@stefanini.com',
    assunto: 'teste2',
    mensagen: 'teste2 teste2 teste2 teste2',
    data: '03/03/2020',
    arquivada: true,
    importante: true,
    lida: false,
    status: 'Aguardando Aprovação',
  }
];

@Component({
  selector: 'app-aprovacao',
  templateUrl: './aprovacao.component.html',
  styleUrls: ['./aprovacao.component.css']
})
export class AprovacaoComponent implements OnInit {

  public colunas: any[];
  public itens: Mensagem[];
  public selectedMensagem: Mensagem[];
  public mensagemSelecionada: Mensagem;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.buildColumns();
    this.itens = ELEMENT_DATA;
  }

  private buildColumns(): void {
    this.colunas = [
      { field: 'fromId', header: 'De' },
      { field: 'assunto', header: 'Título'},
      { field: 'data', header: 'Data' },
      { field: 'status', header: 'Status' }
    ];
  }

}
