import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AprovacaoRoutingModule } from './aprovacao-routing.module';
import { AprovacaoComponent } from './aprovacao.component';
import { SharedModule } from './../../../shared/shared.module';


@NgModule({
  declarations: [AprovacaoComponent],
  imports: [
    CommonModule,
    AprovacaoRoutingModule,
    SharedModule
  ]
})
export class AprovacaoModule { }
