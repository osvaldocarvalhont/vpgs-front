import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PerguntasCategoriaFormComponent } from './perguntas-categoria-form.component';

const routes: Routes = [
  {
    path: '',
    component: PerguntasCategoriaFormComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerguntasCategoriaFormRoutingModule { }
