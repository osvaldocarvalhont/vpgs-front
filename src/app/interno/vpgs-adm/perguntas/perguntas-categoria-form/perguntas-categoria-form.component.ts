import { BaseCoreComponent } from 'src/app/shared/base-core/base-core/base-core.component';
import { FaqCategoryService } from './../services/faq-category.service';
import { FaqCategoryQuestion } from 'src/app/shared/models/faq-category-question.model';
import { FiltroFiltros } from './../../../../shared/models/filtro-filtros.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { FiltroPermissoesPadrao } from 'src/app/shared/models/filtro-permissoes-padrao.model';
import { FiltroPermissoes } from 'src/app/shared/models/filtro-permissoes.model';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FaqCategory } from 'src/app/shared/models/faq-category.model';
import { LocaleService } from '../services/locale.service';
import { KeyValue } from 'src/app/shared/models/key-value.model';
import { UserPermission } from 'src/app/shared/models/user-permission.model';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-perguntas-categoria-form',
  templateUrl: './perguntas-categoria-form.component.html',
  styleUrls: ['./perguntas-categoria-form.component.css']
})
export class PerguntasCategoriaFormComponent extends BaseCoreComponent implements OnInit {

  public optionsBoolean: SelectItem[];
  public categoryId: number;
  public title: string;
  public edit: boolean = false;
  public faqCategory: FaqCategory = new FaqCategory();
  public permissionList: FiltroPermissoes[] = [];
  public totalRecords: number;
  public optionsLocale: SelectItem[];
  public labelSalvar: string;
  public labelVoltar: string;
  public labelIdioma: string;
  public labelManterAoFalhar: string;
  public labelUsuarioLogado: string;
  public labelNome: string;
  public labelSucesso: string;
  public labelErro: string;
  public labelCategoriaSalvaSucesso: string;
  public labelCategoriaSalvaErro: string;
  public labelCategoriaAtualizadaSucesso: string;
  public labelCategoriaAtualizadaErro: string;
  public labelCamposObrigatorios: string;

  constructor(private messageService: MessageService,
              public ref: DynamicDialogRef,
              public config: DynamicDialogConfig,
              public faqCategoryService: FaqCategoryService,
              private translocoService: TranslocoService,
              private localeService: LocaleService,
              private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.buildOptions();
    this.categoryId = this.config.data.categoryId;
    this.title = this.categoryId != null ? 'Editar' : 'Adicionar'
    if (this.categoryId != null) {
      this.edit = true;
      this.getCategoryById(this.categoryId);
    } else {
      this.edit = false;
    }
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('salvar').subscribe(value => {
      this.labelSalvar = value;
    });
    this.translocoService.selectTranslate('voltar').subscribe(value => {
      this.labelVoltar = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-idioma').subscribe(value => {
      this.labelIdioma = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-manter-ao-falhar').subscribe(value => {
      this.labelManterAoFalhar = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-usuario-logado').subscribe(value => {
      this.labelUsuarioLogado = value;
    });
    this.translocoService.selectTranslate('nome').subscribe(value => {
      this.labelNome = value;
    });
    this.translocoService.selectTranslate('sucesso').subscribe(value => {
      this.labelSucesso = value;
    });
    this.translocoService.selectTranslate('erro').subscribe(value => {
      this.labelErro = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-categoria-salva-sucesso').subscribe(value => {
      this.labelCategoriaSalvaSucesso = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-categoria-salva-erro').subscribe(value => {
      this.labelCategoriaSalvaErro = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-categoria-atualizada-sucesso').subscribe(value => {
      this.labelCategoriaAtualizadaSucesso = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-categoria-atualizada-erro').subscribe(value => {
      this.labelCategoriaAtualizadaErro = value;
    });
    this.translocoService.selectTranslate('campos-obrigatorios').subscribe(value => {
      this.labelCamposObrigatorios = value;
    });
  }

  private buildOptions(): void {
    this.buildLocales();
    this.optionsBoolean = [
      { label: 'Selecione', value: null },
      { label: 'Sim', value: '1' },
      { label: 'Não', value: '0' }
    ];
  }

  public buildLocales(): void {
    this.localeService.findAllLocales().subscribe(res => {
      let kv: KeyValue[] = [];
      kv.push({ value: 'Selecione', id: null })
      for (let item of res) {
        kv.push({value: this.setLocale(item.value), id: item.id});
      }
      this.optionsLocale = KeyValue.convertToSelectItem(kv);
    });
  }

  public getCategoryById(id: number): void {
    this.faqCategoryService.findFaqCategoryById(id).subscribe(res => {
      this.faqCategory = res;
      let listPerm: FiltroPermissoes = new FiltroPermissoes();
      for (let p of this.faqCategory.permission) {
        listPerm.id = p.id;
        listPerm.aplicacaoKey = p.serviceProviderId;
        listPerm.aplicacao = p.serviceProvider;
        listPerm.grupoKey = p.groupId;
        listPerm.grupo = p.group;
        listPerm.permissaoKey = p.permissionId;
        listPerm.permissao = p.permission;
        listPerm.isRemoved = p.isRemoved ? p.isRemoved : false;
        this.permissionList.push(Object.assign({}, listPerm));
      }
      this.totalRecords = this.permissionList.length;
    });
  }

  public close(): void {
    this.ref.close();
  }

  public save(): void {
    this.getPermissionList();
    if ((this.faqCategory.localeId != null && this.faqCategory.localeId != undefined) &&
    (this.faqCategory.name != null && this.faqCategory.name != undefined)) {
      if (!this.edit) {
        this.faqCategoryService.save(this.faqCategory).subscribe(
          data => {
            this.showModal('success', this.labelSucesso, this.labelCategoriaSalvaSucesso);
            this.close();
            this.router.navigate(['interno/administracao/perguntas']);
          },
          error => {
            const msg = error.error.message === null ? this.labelCategoriaSalvaErro : error.error.message;
            this.showModal('error', this.labelErro, msg);
          });
      } else if (this.edit ) {
          this.faqCategoryService.save(this.faqCategory).subscribe(
            data => {
              this.showModal('success', this.labelSucesso, this.labelCategoriaAtualizadaSucesso);
              this.close();
              this.router.navigate(['interno/administracao/perguntas']);
            },
            error => {
              this.showModal('error', this.labelErro, this.labelCategoriaAtualizadaErro);
            });
        }
      } else {
      this.showModal('error', this.labelErro, this.labelCamposObrigatorios);
    }
  }

  private getPermissionList(): void {
    if (this.permissionList.length > 0) {
      this.faqCategory.permission = [];
      for (const item of this.permissionList) {
        const permissao: UserPermission = new UserPermission();
        permissao.id = item.id ? item.id : null;
        permissao.serviceProviderId = item.aplicacaoKey;
        permissao.serviceProvider = item.aplicacao;
        permissao.groupId = item.grupoKey;
        permissao.group = item.grupo;
        permissao.permissionId = item.permissaoKey;
        permissao.permission = item.permissao;
        permissao.isRemoved = item.isRemoved;

        this.faqCategory.permission.push(Object.assign({}, permissao));
      }
    }
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

  public loadPermission(valueEmited: FiltroPermissoes[]): void {
    this.permissionList = valueEmited;
  }

}
