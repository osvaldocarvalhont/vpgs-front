import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerguntasCategoriaFormRoutingModule } from './perguntas-categoria-form-routing.module';
import { PerguntasCategoriaFormComponent } from './perguntas-categoria-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faCheck, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslocoModule } from '@ngneat/transloco';


@NgModule({
  declarations: [PerguntasCategoriaFormComponent],
  imports: [
    CommonModule,
    PerguntasCategoriaFormRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    DropdownModule,
    InputTextModule,
    FontAwesomeModule,
    ToastModule,
    TableModule,
    ConfirmDialogModule,
    DialogModule,
    SharedModule,
    TranslocoModule
  ]
})
export class PerguntasCategoriaFormModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faCheck, faChevronLeft);
  }
}
