import { FaqService } from './../services/faq.service';
import { Faq } from './../../../../shared/models/faq.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FaqCategoryQuestion } from 'src/app/shared/models/faq-category-question.model';
import { KeyValue } from 'src/app/shared/models/key-value.model';
import { FaqCategoryService } from '../services/faq-category.service';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-perguntas-gestao-form',
  templateUrl: './perguntas-gestao-form.component.html',
  styleUrls: ['./perguntas-gestao-form.component.css']
})
export class PerguntasGestaoFormComponent implements OnInit {

  public optionsCategoria: SelectItem[];
  public categoria: string;
  public questao: string;
  public resposta: string;
  public faqId: number;
  public title: string;
  public faq: Faq = new Faq();
  public optionsFaqCategory: SelectItem[];
  public selectedFaqCategory: SelectItem;
  public edit: boolean = false;
  selectedClass: KeyValue;
  public labelCategoria: string;
  public labelQuestao: string;
  public labelResposta: string;
  public labelSalvar: string;
  public labelVoltar: string;
  public labelSucesso: string;
  public labelErro: string;
  public labelPerguntaSalvaSucesso: string;
  public labelPerguntaAtualizadaSucesso: string;
  public labelPerguntaAtualizadaErro: string;
  public labelPerguntaSalvaErro: string;
  public labelCamposObrigatorios: string;

  constructor(private messageService: MessageService,
              public ref: DynamicDialogRef,
              public config: DynamicDialogConfig,
              private faqCategoryService: FaqCategoryService,
              private translocoService: TranslocoService,
              private faqService: FaqService,
              private router: Router) { }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.buildFaqCategories();
    this.faqId = this.config.data.faqId;
    this.title = this.faqId != null ? 'Editar' : 'Adicionar'
    if (this.faqId != null) {
      this.edit = true;
      this.getFaqById(this.faqId);
    } else {
      this.edit = false;
    }
  }

  private getLabelTanslate() {

    this.translocoService.selectTranslate('vpgs-adm-perguntas-categoria').subscribe(value => {
      this.labelCategoria = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-questao').subscribe(value => {
      this.labelQuestao = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-resposta').subscribe(value => {
      this.labelResposta = value;
    });
    this.translocoService.selectTranslate('salvar').subscribe(value => {
      this.labelSalvar = value;
    });
    this.translocoService.selectTranslate('voltar').subscribe(value => {
      this.labelVoltar = value;
    });
    this.translocoService.selectTranslate('sucesso').subscribe(value => {
      this.labelSucesso = value;
    });
    this.translocoService.selectTranslate('erro').subscribe(value => {
      this.labelErro = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-salva-sucesso').subscribe(value => {
      this.labelPerguntaSalvaSucesso = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-atualizada-sucesso').subscribe(value => {
      this.labelPerguntaAtualizadaSucesso = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-atualizada-erro').subscribe(value => {
      this.labelPerguntaAtualizadaErro = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-salva-erro').subscribe(value => {
      this.labelPerguntaSalvaErro = value;
    });
    this.translocoService.selectTranslate('campos-obrigatorios').subscribe(value => {
      this.labelCamposObrigatorios = value;
    });
  }

  public buildFaqCategories(): void {
    this.faqCategoryService.findAllFaqCategories().subscribe(res => {
      this.optionsFaqCategory = KeyValue.convertToSelectItem(res);
    });
  }

  public close(): void {
    this.ref.close();
  }

  public getFaqById(id: number): void {
    this.faqService.findFaqById(id).subscribe(res => {
      this.faq = res;
    });
  }

  public save(): void {
    if ((this.faq.category != null && this.faq.category != undefined) &&
        (this.faq.question != null && this.faq.question != undefined) &&
        (this.faq.answer != null && this.faq.answer != undefined)) {
          if (!this.edit) {
            console.log(this.faq);
            this.faqService.save(this.faq).subscribe(
              data => {
                this.showModal('success', this.labelSucesso, this.labelPerguntaSalvaSucesso);
                this.close();
                this.router.navigate(['interno/administracao/perguntas']);
              },
              error => {
                const msg = error.error.message === null ? this.labelPerguntaSalvaErro : error.error.message;
                this.showModal('error', this.labelErro, msg);
              });
          } else if (this.edit ) {
            this.faqService.save(this.faq).subscribe(
              data => {
                this.showModal('success', this.labelSucesso, this.labelPerguntaAtualizadaSucesso);
                this.close();
                this.router.navigate(['interno/administracao/perguntas']);
              },
              error => {
                this.showModal('error', this.labelErro, this.labelPerguntaAtualizadaErro);
              });
          }
        } else {
        this.showModal('error', this.labelErro, this.labelCamposObrigatorios);
      }
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }
}
