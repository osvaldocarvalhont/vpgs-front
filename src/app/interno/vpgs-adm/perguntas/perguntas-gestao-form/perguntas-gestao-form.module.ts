import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { EditorModule } from 'primeng/editor';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faCheck, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { ToastModule } from 'primeng/toast';

import { PerguntasGestaoFormRoutingModule } from './perguntas-gestao-form-routing.module';
import { PerguntasGestaoFormComponent } from './perguntas-gestao-form.component';
import { TranslocoModule } from '@ngneat/transloco';


@NgModule({
  declarations: [PerguntasGestaoFormComponent],
  imports: [
    CommonModule,
    PerguntasGestaoFormRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    DropdownModule,
    InputTextModule,
    EditorModule,
    FontAwesomeModule,
    ToastModule,
    TranslocoModule
  ]
})
export class PerguntasGestaoFormModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faCheck, faChevronLeft);
  }
}
