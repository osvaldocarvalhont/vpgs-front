import { PerguntasCategoriaFormComponent } from './perguntas-categoria-form/perguntas-categoria-form.component';
import { FaqCategory } from './../../../shared/models/faq-category.model';
import { LocaleService } from './services/locale.service';
import { FilterCategory } from './../../../shared/models/filter-category.model';
import { Faq } from './../../../shared/models/faq.model';
import { FaqService } from './services/faq.service';
import { FaqCategoryQuestion } from './../../../shared/models/faq-category-question.model';
import { FaqCategoryService } from './services/faq-category.service';
import { FilterFaqCategoryQuestion } from './../../../shared/models/filter-faq-category-question.model';
import { Categoria } from './../../../shared/models/categoria.model';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, LazyLoadEvent, MessageService, SelectItem } from 'primeng/api';
import { KeyValue } from 'src/app/shared/models/key-value.model';
import { PerguntasGestaoFormComponent } from './perguntas-gestao-form/perguntas-gestao-form.component';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { BaseCoreComponent } from 'src/app/shared/base-core/base-core/base-core.component';
import { ConstantPool } from '@angular/compiler';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-perguntas',
  templateUrl: './perguntas.component.html',
  styleUrls: ['./perguntas.component.css'],
  providers: [ConfirmationService, MessageService, DialogService]
})
export class PerguntasComponent extends BaseCoreComponent implements OnInit {

  public index: number = 0;
  public colunas: any[];
  public itens: Faq[];
  public selectedFaq: Faq[];

  public colunasCategoria: any[];
  public itensCategoria: FaqCategory[];
  public selectedCategory: FaqCategory[];

  public filter: FilterFaqCategoryQuestion = new FilterFaqCategoryQuestion();
  public optionsFaqCategory: SelectItem[];
  public optionsLocale: SelectItem[];
  public optionsBoolean: SelectItem[];
  public selectedFaqCategory: SelectItem;
  public loading: boolean;
  public loadingCategory: boolean;
  public totalRecords: number;
  public totalRecordsCategory: number;
  public selectedFaqCategoryQuestion: FaqCategoryQuestion[];
  public itensFaqCategoryQuestion: FaqCategoryQuestion[];
  public faqList: number[] = [];
  public categoryList: number[] = [];

  public filterCategory: FilterCategory = new FilterCategory();
  public selectedLocale: SelectItem;
  public selectedIsPersistent: SelectItem;
  public selectedIsLogged: SelectItem;
  public labelFiltro: string;
  public labelFiltrar: string;
  public labelLimpar: string;
  public labelGestao: string;
  public labelCategoria: string;
  public labelCategorias: string;
  public labelQuestao: string;
  public labelAdicionar: string;
  public labelEditar: string;
  public labelRemover: string;
  public labelExportar: string;
  public labelIdioma: string;
  public labelNome: string;
  public labelSemRegistros: string;
  public labelConfirmacao: string;
  public labelPerguntasFrequentes: string;
  public labelConfRemocao: string;
  public labelConfRemocaoConcluida: string;
  public labelSim: string;
  public labelNao: string;
  public labelSucesso: string;
  public labelAtencao: string;
  public labelNaoHaResgistroSelecionado: string;
  public labeInfoRegistrosTabela: string;

  constructor(private confirmationService: ConfirmationService,
              private messageService: MessageService,
              private faqCategoryService: FaqCategoryService,
              private faqService: FaqService,
              private translocoService: TranslocoService,
              private localeService: LocaleService,
              private cdref: ChangeDetectorRef,
              public dialogService: DialogService,
              private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.buildColumnsGestao();
    this.buildColumnsCategoria();
    this.buildOptions();
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('filtro').subscribe(value => {
      this.labelFiltro = value;
    });
    this.translocoService.selectTranslate('filtrar').subscribe(value => {
      this.labelFiltrar = value;
    });
    this.translocoService.selectTranslate('limpar').subscribe(value => {
      this.labelLimpar = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-gestao').subscribe(value => {
      this.labelGestao = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-categoria').subscribe(value => {
      this.labelCategoria = value;
    });
    this.translocoService.selectTranslate('adicionar').subscribe(value => {
      this.labelAdicionar = value;
    });
    this.translocoService.selectTranslate('editar').subscribe(value => {
      this.labelEditar = value;
    });
    this.translocoService.selectTranslate('remover').subscribe(value => {
      this.labelRemover = value;
    });
    this.translocoService.selectTranslate('exportar').subscribe(value => {
      this.labelExportar = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-questao').subscribe(value => {
      this.labelQuestao = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-categorias').subscribe(value => {
      this.labelCategorias = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-idioma').subscribe(value => {
      this.labelIdioma = value;
    });
    this.translocoService.selectTranslate('nome').subscribe(value => {
      this.labelNome = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-sem-registros').subscribe(value => {
      this.labelSemRegistros = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-confirmacao').subscribe(value => {
      this.labelConfirmacao = value;
    });
    this.translocoService.selectTranslate('menuPerguntasFreq').subscribe(value => {
      this.labelPerguntasFrequentes = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-confirmacao-remocacao').subscribe(value => {
      this.labelConfRemocao = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-perguntas-confirmacao-remocacao-concluida').subscribe(value => {
      this.labelConfRemocaoConcluida = value;
    });
    this.translocoService.selectTranslate('sim').subscribe(value => {
      this.labelSim = value;
    });
    this.translocoService.selectTranslate('nao').subscribe(value => {
      this.labelNao = value;
    });
    this.translocoService.selectTranslate('sucesso').subscribe(value => {
      this.labelSucesso = value;
    });
    this.translocoService.selectTranslate('atencao').subscribe(value => {
      this.labelAtencao = value;
    });
    this.translocoService.selectTranslate('nao-ha-registro-selecionado').subscribe(value => {
      this.labelNaoHaResgistroSelecionado = value;
    });
    this.translocoService.selectTranslate('info-total-registros-tabela').subscribe(value => {
      this.labeInfoRegistrosTabela = value;
    });
  }

  private buildColumnsGestao(): void {
    this.colunas = [
      { field: 'categoryName', header: this.labelCategoria },
      { field: 'question', header: this.labelQuestao}
    ];
  }

  private buildColumnsCategoria(): void {
    this.colunasCategoria = [
      { field: 'locale', header: this.labelIdioma },
      { field: 'name', header: this.labelNome}
    ];
  }

  private buildOptions(): void {
    this.buildFaqCategories();
    this.buildBooleanField();
    this.buildLocales();
  }

  public buildBooleanField(): void {
    this.optionsBoolean = [
      { label: 'Selecione', value: null },
      { label: 'False', value: '0' },
      { label: 'True', value: '1' }
    ];
  }

  public getValueFaqCategory(): void {
    this.filter.category = this.selectedFaqCategory.value;
  }

  public buildFaqCategories(): void {
    this.faqCategoryService.findAllFaqCategories().subscribe(res => {
      this.optionsFaqCategory = KeyValue.convertToSelectItem(res);
    });
  }

  public buildLocales(): void {
    this.localeService.findAllLocales().subscribe(res => {
      this.optionsLocale = KeyValue.convertToSelectItem(res);
    });
  }

  public filtrar(): void {
    this.setFilter();
    this.getAllFaqCategoryQuestion(this.filter);
  }

  private setFilter() {
    this.filter.question = this.filter.question != null ? this.filter.question.toUpperCase() : null;
  }

  public clear(): void {
    this.filter.category = null;
    this.selectedFaqCategory = null
    this.filter.question = null;
    this.getAllFaqCategoryQuestion(this.filter);
  }

  public getAllFaqCategoryQuestion(filter: FilterFaqCategoryQuestion): void {
    this.faqService.findAllFaqCategoryQuestion(this.filter).subscribe(res => {
      this.itensFaqCategoryQuestion = res['content'];
      this.totalRecords = res['totalElements'];
      this.loading = false;
    });
  }

  public filtrarCategory(): void {
    this.setFilterCategory();
    console.log(this.filterCategory);
    this.getAllFaqCategory(this.filterCategory);
  }

  private setFilterCategory() {
    this.filterCategory.locale = this.selectedLocale ? this.selectedLocale.value : null;
    this.filterCategory.isPersistent = this.selectedIsPersistent ? this.selectedIsPersistent.value : null;
    this.filterCategory.isLogged = this.selectedIsLogged ? this.selectedIsLogged.value : null;
    this.filterCategory.name = this.filterCategory.name != null ? this.filterCategory.name.toUpperCase() : null;
  }

  public clearCategory(): void {
    this.filterCategory.locale = null;
    this.filterCategory.isPersistent = null;
    this.filterCategory.isLogged = null;
    this.filterCategory.name = null;
    this.selectedLocale = null;
    this.selectedIsPersistent = null;
    this.selectedIsLogged = null;
    this.getAllFaqCategory(this.filterCategory);
  }

  public getAllFaqCategory(filter: FilterCategory): void {
    this.faqCategoryService.findAllFaqCategoriesFilterPage(filter).subscribe(res => {

      let category: FaqCategory[] = res['content'];
      for (let index in res['content']) {
        category[index].locale = this.setLocale(category[index].locale)
      }
      this.itensCategoria  = category;
      this.totalRecordsCategory = res['totalElements'];
      this.loadingCategory = false;
    });
  }

  public addPergunta(): void{
    this.showDialogFaqForm();
  }

  public editPergunta(): void{
    this.showDialogFaqForm(this.selectedFaqCategoryQuestion[0].id);
  }

  public removePergunta(): void{
    this.buildFaqListSelected();
    this.showConfirmationDiolog('faq');
  }

  private buildFaqListSelected(): void {
    if (this.selectedFaqCategoryQuestion != null) {
      for (const f of this.selectedFaqCategoryQuestion) {
        this.faqList.push(f.id);
      }
    }
  }

  private buildCategoryListSelected(): void {
    if (this.selectedCategory != null) {
      for (const c of this.selectedCategory) {
        this.categoryList.push(c.id);
      }
    }
  }

  public showDialogFaqForm(id?: number): DynamicDialogRef {
    const ref = this.dialogService.open(PerguntasGestaoFormComponent, {
        data: {faqId: id},
        header: this.labelPerguntasFrequentes,
        width: '80%',
        height: '100%',
        contentStyle: {'overflow': 'auto'},
        baseZIndex: 10000,
    });

    return ref;

    ref.onClose.subscribe(() => {
    });
  }

  public addCategory(): void{
    this.showDialogCategoryForm();
  }

  public editCategory(): void{
    this.showDialogCategoryForm(this.selectedCategory[0].id);
  }

  public removeCategory(): void{
    this.buildCategoryListSelected;
    this.showConfirmationDiolog('category');
  }

  public showDialogCategoryForm(id?: number): DynamicDialogRef {
    const ref = this.dialogService.open(PerguntasCategoriaFormComponent, {
        data: {categoryId: id},
        header: this.labelPerguntasFrequentes,
        width: '80%',
        height: '100%',
        contentStyle: {'overflow': 'auto'},
        baseZIndex: 10000,
    });

    return ref;

    ref.onClose.subscribe(() => {
    });
  }

  private showConfirmationDiolog(type: string): void {
    let msgConfirmation = this.labelConfRemocao;
    let msgModal = this.labelConfRemocaoConcluida;

    this.confirmationService.confirm({
      message: msgConfirmation,
      acceptLabel: this.labelSim,
      rejectLabel: this.labelNao,
      accept: () => {
        debugger
        if(type === 'faq') {
          this.faqService.remove(this.faqList).subscribe(data =>
            {
            this.showModal('success', this.labelSucesso, msgModal);
            this.load();
            },
          );
        } else {
          this.faqCategoryService.remove(this.categoryList).subscribe(data =>
            {
            this.showModal('success', this.labelSucesso, msgModal);
            this.load();
            },
          );
        }
      }
    });
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

  public exportExcel(): void {
    this.loading = true;
    this.setFilter();
    this.getAllFaqCategoryQuestionExcel(this.filter);
  }

  public getAllFaqCategoryQuestionExcel(filter: FilterFaqCategoryQuestion): void {
    this.faqService.getXlsxAllFaqCategoryQuestion(this.filter).subscribe(res => {
      this.saveAsExcelFile(res, 'Perguntas.xlsx');
      this.loading = false;
    }, error => {
      console.error(error);
    });
  }

  public saveAsExcelFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        let EXCEL_EXTENSION = '.xlsx';

        let data: Blob = new Blob([buffer], {
            type: 'application/octet-stream'
        });
        FileSaver.saveAs(data, fileName);
    });
  }

  public loadData(event: LazyLoadEvent): void {
    this.loading = true;
    // tslint:disable-next-line: no-string-literal
    this.filter.page = (event['first'] / 10).toString();
    if(event['sortField'] === 'categoryName'){
      this.filter.orderBy = 'category';
    }
    this.filter.direction = event['sortOrder'] === 1 ? 'ASC' : 'DESC';
    this.getAllFaqCategoryQuestion(this.filter);
  }

  public loadDataCategory(event: LazyLoadEvent): void {
    this.loadingCategory = true;
    // tslint:disable-next-line: no-string-literal
    this.filterCategory.page = (event['first'] / 10).toString();
    this.filterCategory.direction = event['sortOrder'] === 1 ? 'ASC' : 'DESC';
    this.getAllFaqCategory(this.filterCategory);
  }

  public load(): void {
    //location.reload();
  }

  public movePositionDownFaq(): void {
    this.buildFaqListSelected();
    if (this.faqList.length > 0) {
      // console.log(this.categoryList);
      this.faqService.movePositionDownFaq(this.faqList).subscribe(data =>
        {
          this.load();
        },
      );
    } else {
      this.showModal('warn', this.labelAtencao, this.labelNaoHaResgistroSelecionado);
    }
  }

  public movePositionUpFaq(): void {
    this.buildFaqListSelected();
    if (this.faqList.length > 0) {
      // console.log(this.categoryList);
      this.faqService.movePositionUpFaq(this.faqList).subscribe(data =>
        {
          this.load();
        },
      );
    } else {
      this.showModal('warn', this.labelAtencao, this.labelNaoHaResgistroSelecionado);
    }
  }

  public movePositionDownFaqCategory(): void {
    this.buildCategoryListSelected();
    if (this.categoryList.length > 0) {
      // console.log(this.categoryList);
      this.faqCategoryService.movePositionDownFaqCategory(this.categoryList).subscribe(data =>
        {
          this.load();
        },
      );
    } else {
      this.showModal('warn', this.labelAtencao, this.labelNaoHaResgistroSelecionado);
    }
  }

  public movePositionUpFaqCategory(): void {
    this.buildCategoryListSelected();
    if (this.categoryList.length > 0) {
      // console.log(this.categoryList);
      this.faqCategoryService.movePositionUpFaqCategory(this.categoryList).subscribe(data =>
        {
          this.load();
        },
      );
    } else {
      this.showModal('warn', this.labelAtencao, this.labelNaoHaResgistroSelecionado);
    }
  }



}
