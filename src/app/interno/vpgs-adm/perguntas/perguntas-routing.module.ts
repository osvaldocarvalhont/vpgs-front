import { PerguntasComponent } from './perguntas.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import '@angular/compiler';

const routes: Routes = [
  {
    path: '',
    component: PerguntasComponent,
    children: [
      {
        path: 'add',
        loadChildren: () => import('./perguntas-gestao-form/perguntas-gestao-form.module').then(m => m.PerguntasGestaoFormModule)
      },
      {
        path: 'addCategoria',
        loadChildren: () => import('./perguntas-categoria-form/perguntas-categoria-form.module').then(m => m.PerguntasCategoriaFormModule)
      },
      {
        path: 'edit/:id',
        loadChildren: () => import('./perguntas-gestao-form/perguntas-gestao-form.module').then(m => m.PerguntasGestaoFormModule)
      },
      {
        path: 'editCategoria/:id',
        loadChildren: () => import('./perguntas-categoria-form/perguntas-categoria-form.module').then(m => m.PerguntasCategoriaFormModule)
      }
    ]
    // children: [
    //   {
    //     path: 'addPergunta',
    //     loadChildren: () => import('./perguntas-gestao-form/perguntas-gestao-form.module').then(m => m.PerguntasGestaoFormModule)
    //   }
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerguntasRoutingModule { }
