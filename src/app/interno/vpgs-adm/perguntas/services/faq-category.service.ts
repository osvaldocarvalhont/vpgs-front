import { FilterPerguntasFrequentes } from './../../../../shared/models/filter-perguntas-frequentes.model';
import { FaqCategory } from './../../../../shared/models/faq-category.model';
import { FilterCategory } from './../../../../shared/models/filter-category.model';
import { KeyValue } from './../../../../shared/models/key-value.model';
import { environment } from './../../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FaqCategoryService {

  private readonly ENDPOINT_FAQ_CATEGORY = 'faq-categories';
  private readonly ENDPOINT_FAQ_CATEGORY_FILTER = '/filter-search';
  private readonly ENDPOINT_FAQ_CATEGORY_REMOVE = '/remove';
  private readonly ENDPOINT_FAQ_CATEGORY_UPDATE_DOWN = '/down';
  private readonly ENDPOINT_FAQ_CATEGORY_UPDATE_UP = '/up';
  private readonly ENDPOINT_FAQ_CATEGORY_FAQS = '/faqs';

  constructor(private http: HttpClient) { }

  public findAllFaqCategories(): Observable<KeyValue[]> {
    return this.http.get<KeyValue[]>(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ_CATEGORY)}`);
  }

  public findAllFaqCategoriesFilterPage(filter: FilterCategory): Observable<FaqCategory[]> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<FaqCategory[]>(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ_CATEGORY).concat(this.ENDPOINT_FAQ_CATEGORY_FILTER)}`, { params: this.buildParam(filter) });
  }

  public save(faqCategory: FaqCategory): Observable<FaqCategory> {
    return this.http.post<FaqCategory>(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ_CATEGORY)}`, faqCategory);
  }

  public findFaqCategoryById(id: number): Observable<FaqCategory> {
    return this.http.get<FaqCategory>(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ_CATEGORY)}/${id}`);
  }

  public remove(list: number[]): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ_CATEGORY).concat(this.ENDPOINT_FAQ_CATEGORY_REMOVE)}`, list);
  }

  public movePositionDownFaqCategory(list: number[]): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ_CATEGORY).concat(this.ENDPOINT_FAQ_CATEGORY_UPDATE_DOWN)}`, list);
  }

  public movePositionUpFaqCategory(list: number[]): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ_CATEGORY).concat(this.ENDPOINT_FAQ_CATEGORY_UPDATE_UP)}`, list);
  }

  public findAll(filter: FilterPerguntasFrequentes): Observable<FaqCategory[]> {
    return this.http.get<FaqCategory[]>(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ_CATEGORY).concat(this.ENDPOINT_FAQ_CATEGORY_FAQS)}`, { params: this.buildParamPerguntas(filter) });
  }

  public buildParam(filter: FilterCategory): HttpParams {
    let parameters = new HttpParams();
    // tslint:disable-next-line: forin
    for (const key in filter) {
      const value = filter[key];
      if (value !== null && value !== undefined) {
        parameters = parameters.append(key, value.toString());
      }
    }
    return parameters;
  }

  public buildParamPerguntas(filter: FilterPerguntasFrequentes): HttpParams {
    let parameters = new HttpParams();
    // tslint:disable-next-line: forin
    for (const key in filter) {
      const value = filter[key];
      if (value !== null && value !== undefined) {
        parameters = parameters.append(key, value.toString());
      }
    }
    return parameters;
  }

}
