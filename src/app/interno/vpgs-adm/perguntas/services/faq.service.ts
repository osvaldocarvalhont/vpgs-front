import { environment } from './../../../../../environments/environment';
import { FaqCategoryQuestion } from './../../../../shared/models/faq-category-question.model';
import { FilterFaqCategoryQuestion } from './../../../../shared/models/filter-faq-category-question.model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Faq } from 'src/app/shared/models/faq.model';

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  private readonly ENDPOINT_FAQ = 'faqs';
  private readonly ENDPOINT_FAQ_FILTER = '/filter';
  private readonly ENDPOINT_FAQ_EXPORT = '/export';
  private readonly ENDPOINT_FAQ_REMOVE = '/remove';
  private readonly ENDPOINT_FAQ_UPDATE_DOWN = '/down';
  private readonly ENDPOINT_FAQ_UPDATE_UP = '/up';


  constructor(private http: HttpClient) { }


  public findAllFaqCategoryQuestion(filter: FilterFaqCategoryQuestion): Observable<FaqCategoryQuestion[]> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<FaqCategoryQuestion[]>(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ).concat(this.ENDPOINT_FAQ_FILTER)}`, { params: this.buildParam(filter) });
  }

  public getXlsxAllFaqCategoryQuestion(filter: FilterFaqCategoryQuestion): any {
    return this.http.get<any>(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ)
      .concat(this.ENDPOINT_FAQ_EXPORT)}`, this.buildOptions(filter));
  }

  public save(faq: Faq): Observable<Faq> {
    return this.http.post<Faq>(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ)}`, faq);
  }

  public findFaqById(id: number): Observable<Faq> {
    return this.http.get<Faq>(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ)}/${id}`);
  }

  public movePositionDownFaq(list: number[]): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ).concat(this.ENDPOINT_FAQ_UPDATE_DOWN)}`, list);
  }

  public movePositionUpFaq(list: number[]): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ).concat(this.ENDPOINT_FAQ_UPDATE_UP)}`, list);
  }

  public remove(list: number[]): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_FAQ).concat(this.ENDPOINT_FAQ_REMOVE)}`, list);
  }

  public buildParam(filter: FilterFaqCategoryQuestion): HttpParams {
    let parameters = new HttpParams();
    // tslint:disable-next-line: forin
    for (const key in filter) {
      const value = filter[key];
      if (value !== null && value !== undefined) {
        parameters = parameters.append(key, value.toString());
      }
    }
    return parameters;
  }

  public buildOptions(filter: FilterFaqCategoryQuestion): any {
    let headers = new HttpHeaders({'content-type' : 'application/json',
    'Access-Control-Allow-Origin' : '*'});

    var options = {
      params: this.buildParam(filter),
      headers: headers,
      responseType: 'blob'};

    return options;
  }

}
