import { KeyValue } from './../../../../shared/models/key-value.model';
import { environment } from './../../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocaleService {

  private readonly ENDPOINT_LOCALE = 'locales';

  constructor(private http: HttpClient) { }

  public findAllLocales(): Observable<KeyValue[]> {
    return this.http.get<KeyValue[]>(`${environment.BASE_URL.concat(this.ENDPOINT_LOCALE)}`);
  }

}
