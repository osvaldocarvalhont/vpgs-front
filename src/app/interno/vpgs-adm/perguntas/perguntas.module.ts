import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabViewModule } from 'primeng/tabview';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faFileExcel, faCheckCircle, faPlusSquare, faTrashAlt, faEdit, faFilter, faChevronLeft} from '@fortawesome/free-solid-svg-icons';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { InputTextModule } from 'primeng/inputtext';

import { PerguntasRoutingModule } from './perguntas-routing.module';
import { PerguntasComponent } from './perguntas.component';
import { PanelModule } from 'primeng/panel';
import { FormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';
import { TranslocoModule } from '@ngneat/transloco';


@NgModule({
  declarations: [PerguntasComponent],
  imports: [
    CommonModule,
    PerguntasRoutingModule,
    TabViewModule,
    FontAwesomeModule,
    ButtonModule,
    TableModule,
    DropdownModule,
    InputTextModule,
    ConfirmDialogModule,
    ToastModule,
    PanelModule,
    FormsModule,
    DialogModule,
    TranslocoModule
  ]
})
export class PerguntasModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faFileExcel, faCheckCircle, faPlusSquare, faTrashAlt, faEdit, faFilter, faChevronLeft);
  }
}
