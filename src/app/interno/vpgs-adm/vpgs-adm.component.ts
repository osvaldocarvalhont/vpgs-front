import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { TranslocoService } from '@ngneat/transloco';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-vpgs-adm',
  templateUrl: './vpgs-adm.component.html',
  styleUrls: ['./vpgs-adm.component.css']
})
export class VpgsAdmComponent implements OnInit {

  public items: MenuItem[];
  public titulo: string;
  public labelContas: string;
  public labelUsuarios: string;
  public labelPermissoes: string;
  public labelSolicitacoes: string;
  public labelAcessos: string;
  public labelMensagens: string;
  public labelPerguntasFrequentes: string;
  public labelComunicacao: string;
  public labelGerenciarContas: string;
  public labelEditarUsuario: string;
  public labelGerenciarSolicitações: string;
  public labelPainelAplicacoes: string;

  public usuario: string;


  constructor(private router: Router, private translocoService: TranslocoService) { }

  ngOnInit(): void {
    this.getLabelTanslate();
    setTimeout(()=>{
    this.buildMenu();
    this.getRouter();
  },200);

    var json: string = localStorage.getItem('currentUser');
    var user: User = JSON.parse(json);

    this.usuario = user.firstName;
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('vpgs-adm-menu-contas').subscribe(value => {
      this.labelContas = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-contas-usuarios').subscribe(value => {
      this.labelUsuarios = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-contas-permissoes').subscribe(value => {
      this.labelPermissoes = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-contas-solicitacoes').subscribe(value => {
      this.labelSolicitacoes = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-contas-acessos').subscribe(value => {
      this.labelAcessos = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-comunicacao-mensagens').subscribe(value => {
      this.labelMensagens = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-comunicacao-frequentes').subscribe(value => {
      this.labelPerguntasFrequentes = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-comunicacao').subscribe(value => {
      this.labelComunicacao = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-contas-usuarios-tile').subscribe(value => {
      this.labelGerenciarContas = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-contas-usuarios-edit-tile').subscribe(value => {
      this.labelEditarUsuario = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-contas-solicitacoes-title').subscribe(value => {
      this.labelGerenciarSolicitações = value;
    });
    this.translocoService.selectTranslate('vpgs-adm-menu-home-title').subscribe(value => {
      this.labelPainelAplicacoes = value;
    });
  }

  private getRouter(): void {
    this.router.events.subscribe(value => {
      this.getPageTitle(this.router.url.toString());
    });
  }

  public getPageTitle(url: string): void {
    if (url.match('/interno/administracao/solicitacoes')) {
      this.titulo = this.labelGerenciarSolicitações;
    } else if (url.match('/interno/administracao/usuarios/usuariosForm')) {
      this.titulo = this.labelGerenciarContas;
    } else if (url.match('/interno/administracao/usuarios/editUser')) {
      this.titulo = this.labelEditarUsuario;
    } else if (url.match('/interno/administracao/usuarios/editRequest')) {
      this.titulo = this.labelEditarUsuario;
    } else if (url.match('/interno/administracao/permissoes')) {
      this.titulo = this.labelPermissoes;
    } else if (url.match('/interno/administracao/usuarios')) {
      this.titulo = this.labelGerenciarContas;
    } else if (url.match('/interno/administracao/acessos')) {
      this.titulo = this.labelAcessos;
    } else if (url.match('/interno/administracao/perguntas')) {
      this.titulo = this.labelPerguntasFrequentes;
    } else if (url.match('/interno/administracao/mensagens')) {
      this.titulo = this.labelMensagens;
    } else if (url.match('/interno/administracao/aprovacao')) {
      this.titulo = 'Aprovação de E-mail';
    } else {
      this.titulo = this.labelPainelAplicacoes;
    }


  }

  public buildMenu(): void {
    this.items = [{
      label: 'Home', routerLink: '/interno/administracao'
    },
    {
      label: this.labelContas,
      items: [
          {label: this.labelUsuarios, routerLink: '/interno/administracao/usuarios'},
          {label: this.labelPermissoes, routerLink: '/interno/administracao/permissoes'},
          {label: this.labelSolicitacoes, routerLink: '/interno/administracao/solicitacoes'},
          {label: this.labelAcessos, routerLink: ''}
      ]
    },
    {
        label: this.labelComunicacao,
        items: [
            {label: this.labelMensagens, routerLink: '/interno/administracao/mensagens'},
            // {label: 'Aprovação de E-mail', routerLink: '/interno/administracao/aprovacao'},
            {label: this.labelPerguntasFrequentes, routerLink: '/interno/administracao/perguntas'}
        ]
    }];
  }

  public logout(): void {
    this.router.navigate(['/interno']);
  }

}
