import { FilterAcessos } from './../../../shared/models/filter-acessos.models';
import { RequestStatus } from './../../../shared/enums/request-status.enum';
import { SolicitacaoService } from './../../vpgs-adm/solicitacoes/services/solicitacao.service';
import { Request } from './../../../shared/models/request.model';
import { FormBuilder } from '@angular/forms';
import {LazyLoadEvent, MessageService, SelectItem} from 'primeng/api';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-acesso',
  templateUrl: './acesso.component.html',
  styleUrls: ['./acesso.component.css'],
  providers: [MessageService, SolicitacaoService]
})
export class AcessoComponent implements OnInit {

  index = 0;
  public aplicacoes: SelectItem[];
  public itens: Request[];
  public reqItens: Request[] = [];
  public request: Request = new Request();
  public requestInfo: Request = new Request();
  public cols: any[];
  public selectedRequest: Request;
  public displayDiologInfoRequest = false;
  public loading: boolean;
  public totalRecords: number;
  public filter: FilterAcessos = new FilterAcessos();
  public getValueNovoAcesso: string;
  public getValueSolicitacoes: string;
  public getValueInfoSolicitacao: string;
  public getValueFechar: string;
  public labelEnviar: string;
  public idioma: string;
  public labeInfoRegistrosTabela: string;

  constructor(private fb: FormBuilder,
              private solicitacaoService: SolicitacaoService,
              private cdref: ChangeDetectorRef,
              private translocoService: TranslocoService,
              private messageService: MessageService) { }

  ngOnInit(): void {
    this.idioma = this.translocoService.getActiveLang();
    this.setConfigInitial();
    this.getRequestByUser(this.filter);
    this.buildOptionsAplicacoes();
    this.buildColumns();
    this.getLabelTanslate();
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('vpgs-app-acesso-novoAcessso').subscribe(value => {
      this.getValueNovoAcesso = value;
    });
    this.translocoService.selectTranslate('vpgs-app-acesso-solicitacoes').subscribe(value => {
      this.getValueSolicitacoes = value;
    });
    this.translocoService.selectTranslate('vpgs-app-acesso-solicitacoes-infoSituacao').subscribe(value => {
      this.getValueInfoSolicitacao = value;
    });
    this.translocoService.selectTranslate('fechar').subscribe(value => {
      this.getValueFechar = value;
    });
    this.translocoService.selectTranslate('enviar').subscribe(value => {
      this.labelEnviar = value;
    });
    this.translocoService.selectTranslate('info-total-registros-tabela').subscribe(value => {
      this.labeInfoRegistrosTabela = value;
    });
  }

  private buildOptionsAplicacoes(): void {
    this.aplicacoes = [
      { label: 'Portal Midas', value: 1 },
      { label: 'Service Center', value: 3 },
      { label: 'Consulta Unificada', value: 4 }
    ];
  }

  private buildColumns(): void {
    this.cols = [
      { field: 'createdOn', header: 'Data de Abertura' },
      { field: 'serviceProvider', header: 'Tipo' },
      { field: 'status', header: 'Situação' }
    ];
  }

  private setConfigInitial(): void {
    this.request.serviceProviderId = 1;
    this.clearInput();
  }

  public submit(): void  {
    this.solicitacaoService.save(this.request).subscribe(
      data => {
        this.setConfigInitial();
        this.showModal('success', 'Sucesso', 'Solicitação enviada com sucesso!');
      },
      error => {
        this.showModal('error', 'Erro', 'Erro ao enviar a solicitação!');
        console.log(error);
      });

  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

  public clearInput(): void {
    this.request.descriptiOn = null;
  }

  public getRequestByUser(filter: FilterAcessos): void {
    this.solicitacaoService.findRequestByUser(filter).subscribe(data => {
      this.reqItens = data['content'];
      // tslint:disable-next-line: forin
      for (var index in data['content']) {
        this.reqItens[index].status = this.setStatus(data['content'][index].status, this.reqItens[index]);
      }
      this.itens = this.reqItens;
      this.totalRecords = data['totalElements'];
      this.loading = false;
    });
  }

  public loadData(event: LazyLoadEvent): void {
    this.loading = true;
    // tslint:disable-next-line: no-string-literal
    this.filter.page = (event['first'] / 10).toString();
    this.filter.orderBy = 'createdOn';
    this.filter.direction = event['sortOrder'] === 1 ? 'DESC' : 'ASC';
    this.getRequestByUser(this.filter);
  }

  public onRowSelect(event): void {
    this.displayDiologInfoRequest = true;
    this.solicitacaoService.findRequestById(event.data.id).subscribe(data => {
      this.requestInfo = data;
      this.requestInfo.status = this.setStatus(data.status, this.requestInfo);
    });
  }

  public setStatus(status: string, request: Request): string {
    switch (status) {
      case 'OPEN':
        request.status = RequestStatus.OPEN;
        break;
      case 'CLOSED':
        request.status = RequestStatus.CLOSED;
        break;
      case 'INACTIVATED':
        request.status = RequestStatus.INACTIVATED;
        break;
      case 'ACCEPTED':
        request.status = RequestStatus.ACCEPTED;
        break;
      case 'REJECTED':
        request.status = RequestStatus.REJECTED;
        break;
      case 'WAITING_CONFIRMATION':
        request.status = RequestStatus.WAITING_CONFIRMATION;
        break;
    }
    return request.status;
  }

}
