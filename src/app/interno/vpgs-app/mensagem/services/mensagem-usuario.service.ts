import { CommunicationUser } from './../../../../shared/models/communication-user.model';
import { environment } from './../../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MensagemUsuarioService {

  private readonly ENDPOINT_MENSAGEM_USUARIO = 'communications-user';
  private readonly ENDPOINT_MENSAGEM_USUARIO_UNREAD = '/unread';
  private readonly ENDPOINT_MENSAGEM_USUARIO_ARCHIVE = '/archive';
  private readonly ENDPOINT_MENSAGEM_USUARIO_IMPORTANT = '/important';
  private readonly ENDPOINT_MENSAGEM_USUARIO_UNIMPORTANT = '/unimportant';

  constructor(private http: HttpClient) { }

  public getUnreadMessagesByUser(): Observable<number> {
    return this.http.get<number>(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM_USUARIO).concat(this.ENDPOINT_MENSAGEM_USUARIO_UNREAD)}`);
  }

  public archiveMessage(list: number[]): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM_USUARIO).concat(this.ENDPOINT_MENSAGEM_USUARIO_ARCHIVE)}`, list);
  }

  public setImportant(id: number): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM_USUARIO).concat(this.ENDPOINT_MENSAGEM_USUARIO_IMPORTANT)}`, id);
  }

  public setUnimportant(id: number): Observable<any> {
    return this.http.post(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM_USUARIO).concat(this.ENDPOINT_MENSAGEM_USUARIO_UNIMPORTANT)}`, id);
  }

  public findCommunicationUserById(id: number): Observable<CommunicationUser> {
    return this.http.get<CommunicationUser>(`${environment.BASE_URL.concat(this.ENDPOINT_MENSAGEM_USUARIO)}/${id}`);
  }

}
