import { MensagemComponent } from './../mensagem/mensagem.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faFolder, faTh, faFilter, faStar, faStarHalfAlt, faEye } from '@fortawesome/free-solid-svg-icons';

import { MensagemRoutingModule } from './mensagem-routing.module';
import { PanelModule } from 'primeng/panel';
import { TranslocoModule } from '@ngneat/transloco';


@NgModule({
  declarations: [MensagemComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    CommonModule,
    MensagemRoutingModule,
    ScrollPanelModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    InputTextareaModule,
    TableModule,
    DropdownModule,
    CalendarModule,
    InputTextModule,
    FontAwesomeModule,
    PanelModule,
    TranslocoModule
  ]
})
export class MensagemModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faFolder, faTh, faFilter, faStar, faStarHalfAlt, faEye);
  }
 }
