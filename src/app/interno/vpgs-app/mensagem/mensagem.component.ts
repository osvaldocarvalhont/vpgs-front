import { MensagemUsuarioService } from './services/mensagem-usuario.service';
import { CommunicationUser } from './../../../shared/models/communication-user.model';
import { FilterMensagemUser } from './../../../shared/models/filter-mensagem-user.model';
import { LazyLoadEvent, SelectItem } from 'primeng/api';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MensagemService } from '../../vpgs-adm/mensagem-adm/service/mensagem.service';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-mensagem',
  templateUrl: './mensagem.component.html',
  styleUrls: ['./mensagem.component.css']
})
export class MensagemComponent implements OnInit {

  public optionsMensagem: SelectItem[];
  public optionSelected: string;
  public cols: any[];
  public pt: any;

  public filter: FilterMensagemUser = new FilterMensagemUser();
  public rangeDataFilter: Date[];
  public loading: boolean;
  public colunas: any[];
  public itens: CommunicationUser[] = [];
  public totalRecords: number;
  public selectedMensagem: CommunicationUser[];
  private readonly HORA_INICIO = 'T00:00:00';
  private readonly HORA_FIM = 'T23:59:59';
  public msgList: number[] = [];
  public labelFiltrar: string;
  public labelFiltro: string;
  public labelLimpar: string;
  public labelVisualizar: string;
  public labelArquivar: string;
  public labelTitulo: string;
  public labelData: string;
  public idioma: string;
  public labeInfoRegistrosTabela: string;

  constructor(private router: Router,
              private cdref: ChangeDetectorRef,
              private mensagemUsuarioService: MensagemUsuarioService,
              private translocoService: TranslocoService,
              private mensagemService: MensagemService) { }

  ngOnInit(): void {
    this.idioma = this.translocoService.getActiveLang();
    this.buildColumns();
    this.buildOptionsTipoMsg();
    this.configCalendarPt();
    this.getLabelTanslate();
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('filtro').subscribe(value => {
      this.labelFiltro = value;
    });
    this.translocoService.selectTranslate('filtrar').subscribe(value => {
      this.labelFiltrar = value;
    });
    this.translocoService.selectTranslate('limpar').subscribe(value => {
      this.labelLimpar = value;
    });
    this.translocoService.selectTranslate('visualizar').subscribe(value => {
      this.labelVisualizar = value;
    });
    this.translocoService.selectTranslate('arquivar').subscribe(value => {
      this.labelArquivar = value;
    });
    this.translocoService.selectTranslate('vpgs-app-mensagem-titulo').subscribe(value => {
      this.labelTitulo = value;
    });
    this.translocoService.selectTranslate('vpgs-app-mensagem-data').subscribe(value => {
      this.labelData = value;
    });
    this.translocoService.selectTranslate('info-total-registros-tabela').subscribe(value => {
      this.labeInfoRegistrosTabela = value;
    });
  }

  private buildOptionsTipoMsg() {
    if(this.idioma == 'pt-BR') {
      this.optionsMensagemPt();
    } else if (this.idioma == 'en') {
      this.optionsMensagemEn();
    } else {
      this.optionsMensagemEs();
    }
  }

  private optionsMensagemPt() {
    this.optionsMensagem = [
      { label: 'Caixa de Entrada', value: 'caixaEntrada' },
      { label: 'Importantes', value: 'important' },
      { label: 'Arquivados', value: 'archived' }
    ];
  }

  private optionsMensagemEs() {
    this.optionsMensagem = [
      { label: 'Caixa de Entrada', value: 'caixaEntrada' },
      { label: 'Importantes', value: 'important' },
      { label: 'Archivados', value: 'archived' }
    ];
  }
  private optionsMensagemEn() {
    this.optionsMensagem = [
      { label: 'Inbox', value: 'caixaEntrada' },
      { label: 'Importants', value: 'important' },
      { label: 'Archived', value: 'archived' }
    ];
  }

  private buildColumns(): void {
    this.colunas = [
      { field: 'important', header: 'Importante' },
      { field: 'title', header: 'Assunto'},
      { field: 'creationDate', header: 'Data'},

    ];
  }

  private configCalendarPt(): void {
    this.pt = {
      firstDayOfWeek: 1,
      dayNames: ['domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado'],
      dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthNames: ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'],
      monthNamesShort: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };
  }

  public getValueoptionsMensagem(): void {
    if (this.optionSelected === 'important') {
      this.filter.importante = true;
      this.filter.arquivada = null;
    } else if (this.optionSelected === 'archived') {
      this.filter.arquivada = true;
      this.filter.importante = null;
    } else {
      this.filter.arquivada = false;
      this.filter.importante = null;
    }
    this.getAllCommunicationByUser(this.filter);
  }

  public filtrar(): void {
    this.loading = true;
    this.filter.titulo = this.filter.titulo != null ? this.filter.titulo.toUpperCase() : null;
    if (this.rangeDataFilter !== null && this.rangeDataFilter !== undefined) {
      this.filter.dataInicio = this.toDateTime(new Date(this.rangeDataFilter[0]), this.HORA_INICIO);
      if (this.rangeDataFilter[1] !== null) {
        this.filter.dataFim = this.toDateTime(new Date(this.rangeDataFilter[1]), this.HORA_FIM);
      } else {
        this.filter.dataFim = null;
      }
    }
    this.getAllCommunicationByUser(this.filter);
  }

  public arquivar(): void {
    this.buildMsgListSelected();
    this.mensagemUsuarioService.archiveMessage(this.msgList).subscribe(data => {
        this.load();
      }
    );
  }

  public show(): void {
    if (this.selectedMensagem != null) {
      this.router.navigate(['interno/mensagemDetalhe/', this.selectedMensagem[0].id]);
    }

  }

  private buildMsgListSelected(): void {
    if (this.selectedMensagem != null) {
      for (const msg of this.selectedMensagem) {
        this.msgList.push(msg.id);
      }
    }
  }

  public getAllCommunicationByUser(filter: FilterMensagemUser): void {
    this.mensagemService.findAllCommunicationByUserFilter(this.filter).subscribe(res => {
      // tslint:disable-next-line: no-string-literal
      this.itens =  res['content'];
      // tslint:disable-next-line: no-string-literal
      this.totalRecords = res['totalElements'];
      this.loading = false;
    });
  }

  public clear(): void {
    this.loading = true;
    this.filter.titulo = null;
    this.rangeDataFilter = null;
    this.filter.dataInicio = null;
    this.filter.dataFim = null;
    this.getAllCommunicationByUser(this.filter);
  }

  public setImportant(isImportant: boolean, rowData: CommunicationUser): void {
    if (isImportant === true) {
      this.mensagemUsuarioService.setImportant(rowData.id).subscribe(data => {
        this.load();
      });
    } else {
      this.mensagemUsuarioService.setUnimportant(rowData.id).subscribe(data => {
        this.load();
      });
    }
  }

  public loadData(valueEmited: LazyLoadEvent) {
    this.loading = true;
    // tslint:disable-next-line: no-string-literal
    this.filter.arquivada = false;
    this.filter.page = (valueEmited['first'] / 10).toString();
    if (valueEmited['sortField'] === 'creationDate' || valueEmited['sortField'] === null || valueEmited['sortField'] === undefined) {
      this.filter.orderBy = valueEmited['sortField'] = 'c.creationDate';
    } else {
      this.filter.orderBy = valueEmited['sortField'] = 'c.title';
    }
    this.filter.direction = valueEmited['sortOrder'] === 1 ? 'DESC' : 'ASC';
    this.getAllCommunicationByUser(this.filter);
  }

  public load(): void {
    //location.reload();
  }

  public toDateTime(data: Date, hora: string): any {
    let returnDate = '';

    let year = data.getFullYear();
    let month = data.getMonth() + 1;
    let day = data.getDate();

    returnDate += year;
    returnDate += '-';

    if (month < 10) {
      returnDate += `0${month}-`;
    } else {
        returnDate += `${month}-`;
    }

    if (day < 10) {
      returnDate += `0${day}`;
    } else {
        returnDate += `${day}`;
    }

    returnDate += hora;
    return returnDate;
  }

}
