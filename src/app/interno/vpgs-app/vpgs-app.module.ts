import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabViewModule } from 'primeng/tabview';
// import {ButtonModule} from 'primeng-lts/button';

import { ScrollPanelModule } from 'primeng/scrollpanel';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faTimes, faHome, faFile, faEnvelope, faUser, faQuestion, faSearch, faFolder, faTh,
          faStar, faDownload, faChevronRight, faSignOutAlt, faUsers, faShieldAlt, faThumbsUp, faChartBar  } from '@fortawesome/free-solid-svg-icons';
import { VpgsAppRoutingModule } from './vpgs-app-routing.module';
import { VpgsAppComponent } from './vpgs-app.component';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { TranslocoModule, TRANSLOCO_SCOPE } from '@ngneat/transloco';

@NgModule({
  declarations: [VpgsAppComponent],
  imports: [
    CommonModule,
    VpgsAppRoutingModule,
    ScrollPanelModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    TabViewModule,
    ButtonModule,
    ConfirmDialogModule,
    TranslocoModule  ]
})
export class VpgsAppModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTimes, faHome, faFile, faEnvelope, faUser, faQuestion, faSearch, faFolder, faTh,
      faStar, faDownload, faChevronRight , faSignOutAlt, faUsers, faShieldAlt, faThumbsUp, faChartBar);
  }
}
function routes(routes: any): any[] | import("@angular/core").Type<any> | import("@angular/core").ModuleWithProviders<{}> {
  throw new Error('Function not implemented.');
}

