import { MeusDadosComponent } from './../meus-dados/meus-dados.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PanelModule } from 'primeng/panel';
import { PasswordModule } from 'primeng/password';

import { MeusDadosRoutingModule } from './meus-dados-routing.module';
import { TranslocoModule } from '@ngneat/transloco';

@NgModule({
  declarations: [
    MeusDadosComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    CommonModule,
    MeusDadosRoutingModule,
    ScrollPanelModule,
    TabViewModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    TableModule,
    PanelModule,
    PasswordModule,
    InputTextModule,
    TranslocoModule
  ]
})
export class MeusDadosModule { }
