import { FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/models/user.model';
import { TrocarSenha } from 'src/app/shared/models/trocar-senha.model';
import { MessageService } from 'primeng/api';
import { UsuarioService } from '../../vpgs-adm/usuarios/services/usuario.service';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { TranslocoService } from '@ngneat/transloco';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-meus-dados',
  templateUrl: './meus-dados.component.html',
  styleUrls: ['./meus-dados.component.css'],
  providers: [MessageService, DialogService]
})
export class MeusDadosComponent implements OnInit {

  public index: number = 0;

  public user: User = new User();
  public trocarSenha: TrocarSenha = new TrocarSenha();
  public labelMeusDados: string;
  public labelSenha: string;
  public labelEnviar: string;
  public error = '';
  public showError: boolean = false;

  constructor(private fb: FormBuilder,
              private router: Router,
              private usuarioService: UsuarioService,
              private translocoService: TranslocoService,
              private messageService: MessageService) { }

  ngOnInit(): void {
    this.getLabelTanslate();

    var json: string = localStorage.getItem('currentUser');

    this.user = JSON.parse(json);

    this.showError = false;
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('menuMeusDados').subscribe(value => {
      this.labelMeusDados = value;
    });
    this.translocoService.selectTranslate('senha').subscribe(value => {
      this.labelSenha = value;
    });
    this.translocoService.selectTranslate('enviar').subscribe(value => {
      this.labelEnviar = value;
    });
  }

  public update(): void  {
    //TO DO: PEGAR USUARIO LOGADO
    //this.user.id = 32217;
    this.user.firstName = this.user.firstName != null ? this.user.firstName.toUpperCase() : null;
    this.user.lastName = this.user.lastName != null ? this.user.lastName.toUpperCase() : null;
    //console.log(this.user);
    if ((this.user.firstName === null || this.user.firstName === undefined) &&
          (this.user.lastName === null || this.user.lastName === undefined)) {
        this.showModal('error', 'Erro', 'Campos obrigatórios não informados!');
      } else {
          this.usuarioService.updateName(this.user).pipe(take(1)).subscribe(
            data => {
              this.showModal('success', 'Sucesso', 'Nome alterado com sucesso!');
            },
            error => {
              this.showError = true;
              this.error = error.error.error + ' -> ' + error.error.message;
              this.showModal('error', 'Erro', 'Erro ao alterar nome!');
            });
        }
    this.clearMeusDados();
    this.router.navigate(['interno']);
  }

  public changePassword(): void  {
    this.usuarioService.confirmarNovaSenha(this.trocarSenha).pipe(take(1)).subscribe(
      data => {
        this.showModal('success', 'Sucesso', 'Senha alterado com sucesso!');
        this.error = null
      },
      error => {
        //console.log(error.error.error);
        this.showError = true;
        this.error = error.error.error + ' -> ' + error.error.message;
        this.showModal('error', error.error.error, error.error.message);
      });
  }

  public clearMeusDados(): void  {
    this.user.firstName = null;
    this.user.lastName = null;
  }

  public showModal(severityValue: string, summaryValue: string, detailValue: string): void {
    alert(summaryValue + "\n\n" + detailValue)
    this.messageService.add({severity: severityValue, summary: summaryValue, detail: detailValue});
  }

}
