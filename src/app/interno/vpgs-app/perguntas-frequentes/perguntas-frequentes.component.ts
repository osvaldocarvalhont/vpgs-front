import { FilterPerguntasFrequentes } from './../../../shared/models/filter-perguntas-frequentes.model';
import { PerguntasFrequentes } from './../../../shared/models/perguntas-frequentes.model';
import { FaqCategoryService } from './../../vpgs-adm/perguntas/services/faq-category.service';
import { Component, OnInit } from '@angular/core';
import { FaqCategory } from 'src/app/shared/models/faq-category.model';
import {
  debounceTime,
  distinctUntilChanged,
  take
} from "rxjs/operators";

@Component({
  selector: 'app-perguntas-frequentes',
  templateUrl: './perguntas-frequentes.component.html',
  styleUrls: ['./perguntas-frequentes.component.css']
})
export class PerguntasFrequentesComponent implements OnInit {

  public perguntasFrequentes: FaqCategory[] = [];
  public collapsed: boolean = true;
  public filter: FilterPerguntasFrequentes = new FilterPerguntasFrequentes();

  constructor(private faqCategoryService: FaqCategoryService) { }

  ngOnInit(): void {
    this.filter.isLogged = '0';
    this.getAllFaqCategoriesNotLogged();
  }

  public getAllFaqCategoriesNotLogged(): void {
    this.faqCategoryService.findAll(this.filter).pipe(take(1)).subscribe(res => {
          this.perguntasFrequentes =  res;
    });
  }

  public load(valueEmited: string) {
    this.filter.textSearch = valueEmited != null ? valueEmited.toUpperCase() : null;
    this.getAllFaqCategoriesNotLogged();
  }


}
