import { PerguntasFrequentesComponent } from './../perguntas-frequentes/perguntas-frequentes.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: PerguntasFrequentesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerguntasFrequentesRoutingModule { }
