import { SharedModule } from './../../../shared/shared.module';
import { PerguntasFrequentesComponent } from './../perguntas-frequentes/perguntas-frequentes.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PanelModule } from 'primeng/panel';
import { EditorModule } from 'primeng/editor';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faTimes, faSearch } from '@fortawesome/free-solid-svg-icons';


import { PerguntasFrequentesRoutingModule } from './perguntas-frequentes-routing.module';


@NgModule({
  declarations: [PerguntasFrequentesComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    CommonModule,
    PerguntasFrequentesRoutingModule,
    ScrollPanelModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    PanelModule,
    InputTextModule,
    EditorModule,
    FontAwesomeModule,
    SharedModule
  ]
})
export class PerguntasFrequentesModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTimes, faSearch);
  }
}
