import { Cookie } from 'ng2-cookies';
import { AuthMode } from 'src/app/shared/enums/auth-mode.enum';
import { User } from 'src/app/shared/models/user.model';
import { MensagemUsuarioService } from './mensagem/services/mensagem-usuario.service';
import { Component, OnInit } from '@angular/core';
import { TranslocoService, TRANSLOCO_SCOPE } from '@ngneat/transloco';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { AuthTokenService } from 'src/app/shared/authToken/auth-token.service';

@Component({
  selector: 'app-vpgs-app',
  templateUrl: './vpgs-app.component.html',
  styleUrls: ['./vpgs-app.component.css']
})
export class VpgsAppComponent implements OnInit {

  public numberMsg: number;
  public initial = true;
  public idioma: string;
  public isVale: boolean = false;

  constructor(private router: Router, private mensagemUsuarioService: MensagemUsuarioService,  private translocoService: TranslocoService, private http: HttpClient, private tokenAuth : AuthTokenService) { }

  ngOnInit(): void {
    this.getUnreadMessagesByUser();
    this.idioma = this.translocoService.getActiveLang();

    var json  = localStorage.getItem('currentUser');
    var token = this.tokenAuth.decodePayloadJWT();
    console.info(token)

    console.info(JSON.parse(json).authMode)

    this.isVale         = JSON.parse(json).authMode === 'SAML';
  }

  public getUnreadMessagesByUser(): void {
    this.mensagemUsuarioService.getUnreadMessagesByUser().subscribe(res => {
      this.numberMsg =  res;
    });
  }

  public change(): void {
    this.initial = false;
  }

  public logout(): void {

    //this.router.navigate(['?action=logout']);

    var json: string   = localStorage.getItem('currentUser');
    var user: User  = JSON.parse(json);

    Cookie.deleteAll('/', 'localhost');
    Cookie.deleteAll('/', 'vpgs.valeglobal.net');
    Cookie.delete('access_token');
    Cookie.delete('refresh_token');
    Cookie.delete('access_token_exp');
    Cookie.delete('access-code');

    localStorage.removeItem("token");
    localStorage.clear();

    if (user.authMode === 'SAML') {
      this.http.get<any>(environment.BASE_URL + "iam/logout-iam",{}).subscribe(url => window.location.href = url.url);
    } else {
      this.http.get<any>(environment.BASE_URL + "iam/logout-vpgs",{}).subscribe(url => window.location.href = url.url);
    }
  }
}
