import { AuthTokenService } from 'src/app/shared/authToken/auth-token.service';
import { Component, OnInit } from '@angular/core';
import { AuthMode } from 'src/app/shared/enums/auth-mode.enum';
import { User } from 'src/app/shared/models/user.model';
import { AplicacaoService } from './services/aplicacao.service'

@Component({
  selector: 'app-aplicacao',
  templateUrl: './aplicacao.component.html',
  styleUrls: ['./aplicacao.component.css']
})
export class AplicacaoComponent implements OnInit {

  public userLogado: string;
  public unificada: boolean;
  public dashboard: boolean;
  public midas: boolean;
  public serviceCenter: boolean;
  public admin: boolean = false;

  constructor(private aplicacaoService : AplicacaoService, private tokenAuth : AuthTokenService) { }

  ngOnInit(): void {
    var json: string   = localStorage.getItem('currentUser');
    var token = this.tokenAuth.decodePayloadJWT();

    console.info(this.tokenAuth.decodePayloadJWT());

    //this.unificada     = token.unificada;
    //this.dashboard     = token.dashboard;
    //this.midas         = token.midas;
    //this.serviceCenter = token.serviceCenter;
    this.unificada     = true;
    this.dashboard     = true;
    this.midas         = true;
    this.serviceCenter = true;
    this.admin         = token.isAdm || token.isVisualizador;

    var user: User  = JSON.parse(json);
    console.info(user);
    this.userLogado = user.firstName;
  }

  public click(nome : string) : void {

    this.aplicacaoService.getUrl(nome).subscribe(url => {
      window.location.href = url.url;
    });
  }

}
