import { AplicacaoComponent } from './../aplicacao/aplicacao.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AplicacaoRoutingModule } from './aplicacao-routing.module';
import { TranslocoModule } from '@ngneat/transloco';


@NgModule({
  declarations: [
    AplicacaoComponent
  ],
  imports: [
    CommonModule,
    AplicacaoRoutingModule,
    FormsModule,
    TranslocoModule
  ]
})
export class AplicacaoModule { }
