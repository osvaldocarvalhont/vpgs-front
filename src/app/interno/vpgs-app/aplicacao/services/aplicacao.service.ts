import {​​​​​​​ environment }​​​​​​​ from '../../../../../environments/environment';
import {​​​​​​​ HttpClient, HttpHeaders, HttpParams }​​​​​​​ from '@angular/common/http';
import {​​​​​​​Injectable}​​​​​​​ from '@angular/core';
import {​​​​​​​ Observable }​​​​​​​ from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AplicacaoService {

    private readonly ENDPOINT_MODULE = 'module/url/';

    constructor(private http: HttpClient) {​​​​​​​ }​​​​​​​


    getUrl(modulo: string) : Observable<any> {
        return this.http.get<any>(`${environment.BASE_URL.concat(this.ENDPOINT_MODULE).concat(modulo)}`, {});
    }
}