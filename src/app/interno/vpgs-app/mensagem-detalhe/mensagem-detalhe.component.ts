import { MensagemUsuarioService } from './../mensagem/services/mensagem-usuario.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommunicationUser } from 'src/app/shared/models/communication-user.model';
import { Location } from '@angular/common';
import { NgxPrinterService } from 'ngx-printer';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-mensagem-detalhe',
  templateUrl: './mensagem-detalhe.component.html',
  styleUrls: ['./mensagem-detalhe.component.css']
})
export class MensagemDetalheComponent implements OnInit {

  public id: number;
  public listId: number[] = [];
  public labelImprimir: string
  public labelImportante: string
  public labelArquivar: string

  public communicationUser: CommunicationUser = new CommunicationUser();

  constructor(private mensagemUsuarioService: MensagemUsuarioService,
              private location: Location,
              private router: Router,
              private printerService: NgxPrinterService,
              private translocoService: TranslocoService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getLabelTanslate();
    this.id = this.route.snapshot.params['id'];
    if (this.id) {
      this.getCommunicationUserById(this.id);
    }
  }

  private getLabelTanslate() {
    this.translocoService.selectTranslate('vpgs-app-mensagem-imprimir').subscribe(value => {
      this.labelImprimir = value;
    });
    this.translocoService.selectTranslate('vpgs-app-mensagem-importante').subscribe(value => {
      this.labelImportante = value;
    });
    this.translocoService.selectTranslate('arquivar').subscribe(value => {
      this.labelArquivar = value;
    });
  }

  public getCommunicationUserById(id: number): void {
    this.mensagemUsuarioService.findCommunicationUserById(id).subscribe(res => {
      this.communicationUser = res;
    });
  }

  public setImportant(isImportant: boolean, id: number): void {
    if (isImportant === true) {
      this.mensagemUsuarioService.setImportant(id).subscribe(data => {
        this.back();
      });
    } else {
      this.mensagemUsuarioService.setUnimportant(id).subscribe(data => {
        this.back();
      });
    }
  }

  public arquivar(id: number): void {
    this.listId.push(id);
    this.mensagemUsuarioService.archiveMessage(this.listId).subscribe(data => {
        this.back();
      }
    );
  }

  public print(): void {
    this.printerService.printDiv('toPrint');
  }

  public back(): void{
    this.location.back();
  }

}
