import { MensagemDetalheComponent } from './../mensagem-detalhe/mensagem-detalhe.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { EditorModule } from 'primeng/editor';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faFolder, faStar, faDownload, faPrint } from '@fortawesome/free-solid-svg-icons';
import { NgxPrinterModule } from 'ngx-printer';

import { MensagemDetalheRoutingModule } from './mensagem-detalhe-routing.module';
import { TranslocoModule } from '@ngneat/transloco';

@NgModule({
  declarations: [MensagemDetalheComponent],
  imports: [
    CommonModule,
    MensagemDetalheRoutingModule,
    ScrollPanelModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    EditorModule,
    FontAwesomeModule,
    NgxPrinterModule,
    TranslocoModule
  ]
})
export class MensagemDetalheModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faFolder, faStar, faDownload, faPrint);
  }
}
