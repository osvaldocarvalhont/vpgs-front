import { VpgsAppComponent } from './vpgs-app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: VpgsAppComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./aplicacao/aplicacao.module').then(m => m.AplicacaoModule)
      },
      {
        path: 'aplicacao',
        loadChildren: () => import('./aplicacao/aplicacao.module').then(m => m.AplicacaoModule)
      },
      {
        path: 'acesso',
        loadChildren: () => import('./acesso/acesso.module').then(m => m.AcessoModule)
      },
      {
        path: 'meusDados',
        loadChildren: () => import('./meus-dados/meus-dados.module').then(m => m.MeusDadosModule)
      },
      {
        path: 'mensagem',
        loadChildren: () => import('./mensagem/mensagem.module').then(m => m.MensagemModule)
      },
      {
        path: 'mensagemDetalhe/:id',
        loadChildren: () => import('./mensagem-detalhe/mensagem-detalhe.module').then(m => m.MensagemDetalheModule)
      },
      {
        path: 'perguntas',
        loadChildren: () => import('./perguntas-frequentes/perguntas-frequentes.module').then(m => m.PerguntasFrequentesModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VpgsAppRoutingModule { }
