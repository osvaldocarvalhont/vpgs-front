module.exports = {
  rootTranslationsPath: 'src/assets/i18n/',
  langs: ['pt-BR', 'en', 'es'],
  keysManager: {}
};